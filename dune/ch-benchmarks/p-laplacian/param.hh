#pragma once

#include <random>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/common/diagonalmatrix.hh>
#include "dune/ch-benchmarks/utility.hh"


template<typename RT>
struct Param {
  template<typename E, typename X>
  RT f(const E &e, const X &x) const {
    const auto xg = e.geometry().global(x);
    return 1 + std::cos(2 * M_PI * xg[0]) * std::sin(2 * M_PI * xg[1]);
  }

  template<typename IS, typename X>
  RT g(const IS &is, const X &x) const {
    return 0.;
  }

  template<typename IS, typename X>
  bool isDirichlet(const IS& is, const X& x) const {
    return true;
  }

  RT p;
  RT eps;
};
