#pragma once

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include <dune/common/fvector.hh>
#include <dune/pdelab/common/quadraturerules.hh>

template<typename P, typename FEM>
class PLaplacian
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::NumericalJacobianVolume<PLaplacian<P, FEM>>
{
  using FE = typename FEM::Traits::FiniteElement;
  using Basis = typename FE::Traits::LocalBasisType;
  using RF = typename Basis::Traits::RangeFieldType;
  Dune::PDELab::LocalBasisCache<Basis> cache = {};

  P param;
public:
  static constexpr bool doPatternVolume = true;
  static constexpr bool doAlphaVolume = true;

  explicit PLaplacian(P param_) : param(std::move(param_)) {}

  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const {
    const auto& basis = lfsu.finiteElement().localBasis();

    constexpr int dim = EG::Entity::dimension;
    const auto& cell = eg.entity();
    auto geo = eg.geometry();

    const auto jit = geo.jacobianInverseTransposed({});
    const auto detjac = geo.integrationElement({});

    std::vector<Dune::FieldVector<RF, dim>> grad_phi(lfsu.size());

    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 2)){
      const auto& phi = cache.evaluateFunction(qp.position(), basis);
      const auto& js = cache.evaluateJacobian(qp.position(), basis);

      for (std::size_t i = 0; i < lfsu.size(); ++i)
        jit.mv(js[i][0], grad_phi[i]);

      Dune::FieldVector<RF, dim> grad_u{};
      for (int i = 0; i < lfsu.size(); ++i)
        grad_u += x(lfsu, i) * grad_phi[i];

      const auto factor = qp.weight() * detjac;
      const auto f = param.f(cell, qp.position());
      for (int i = 0; i < lfsu.size(); ++i)
        r.accumulate(lfsu, i, (std::pow(grad_u.two_norm2() + param.eps, (param.p - 2) * 0.5) * grad_u * grad_phi[i]
                               - f * phi[i]) * factor);
    }
  }


  template<typename EG, typename LFSU, typename X, typename LFSV,
           typename Jacobian>
  void jacobian_volume( const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, Jacobian& mat) const {
    const auto& basis = lfsu.finiteElement().localBasis();

    constexpr int dim = EG::Entity::dimension;
    const auto& cell = eg.entity();
    auto geo = eg.geometry();

    const auto jit = geo.jacobianInverseTransposed({});
    const auto detjac = geo.integrationElement({});

    std::vector<Dune::FieldVector<RF, dim>> grad_phi(lfsu.size());

    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 2)){
      const auto& js = cache.evaluateJacobian(qp.position(), basis);

      for (std::size_t i = 0; i < lfsu.size(); ++i)
        jit.mv(js[i][0], grad_phi[i]);

      Dune::FieldVector<RF, dim> grad_u{};
      for (int i = 0; i < lfsu.size(); ++i)
        grad_u += x(lfsu, i) * grad_phi[i];

      const auto p_4 = std::pow(grad_u.two_norm2() + param.eps, (param.p - 4) * 0.5);
      const auto p_2 = std::pow(grad_u.two_norm2() + param.eps, (param.p - 2) * 0.5);

      const auto factor = qp.weight() * detjac;
      const auto f = param.f(cell, qp.position());
      for (int i = 0; i < lfsu.size(); ++i)
        for (int j = 0; j < lfsu.size(); ++j)
        mat.accumulate(lfsu, j, lfsu, i,
                       (((param.p - 2) * p_4 * grad_u * grad_phi[j] * grad_u)
                        + p_2 * grad_phi[j])  * grad_phi[i] * factor);
    }

  }
};