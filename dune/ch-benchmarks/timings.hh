#pragma once

#include <chrono>
#include <cmath>
#include <string>
#include <iostream>

#include <likwid.h>
#include "mpihelper.hh"

template <typename F>
auto likwid_timed_fixed(const std::string& region, F&& f, const int it = 1){
  const auto start = std::chrono::system_clock::now();
  LIKWID_MARKER_START(region.c_str());
  for (int i = 0; i < it; ++i)
    f();
  LIKWID_MARKER_STOP(region.c_str());
  return std::chrono::system_clock::now() - start;
}

template <typename F, typename Pre = std::function<void()>, typename Post = std::function<void()>>
void likwid_timed(const std::string& region, const MPIHelper& mpiHelper, F&& f, Pre&& pre = []{}, Post&& post = []{}){
  using namespace std::chrono_literals;
  int it = 1;

  std::stringstream dump{};
  auto* coutbuf = std::cout.rdbuf();
  std::cout.rdbuf(dump.rdbuf());
  pre();
  auto once = likwid_timed_fixed(region, std::forward<F>(f), it);
  post();
  std::cout.rdbuf(coutbuf);

  if (once > 0.5s){
    std::cout << dump.str();
  }
  else{
    auto dur = once;
    while (dur < 0.5s) {
      it = static_cast<int>(std::ceil((0.55s / dur) * it));
      const auto start = std::chrono::system_clock::now();
      for (int i = 0; i < it; ++i)
        f();
      dur = std::chrono::system_clock::now() - start;
    }
    it = mpiHelper.max(it);

    LIKWID_MARKER_RESET(region.c_str());
    pre();
    likwid_timed_fixed(region, std::forward<F>(f), it);
    post();
  }
  std::cout << region << std::endl;
  std::cout << "IT " << it << std::endl;
}

template<typename F>
int guess_iterations(F&& f){
  int it = 1;

  double runtime = 0.;
  while(runtime < 0.75) {
    const auto start = std::chrono::steady_clock::now();
    for (int i = 0; i < it; ++i) {
      f();
    }
    const auto end = std::chrono::steady_clock::now();
    runtime += std::chrono::duration<double>(end - start).count();
    if(runtime < 1.) it += static_cast<int>(std::ceil(1.1 * it));
  }

  return it;
}
