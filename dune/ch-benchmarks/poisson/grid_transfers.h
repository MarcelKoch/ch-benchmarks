#ifndef BENCHMARKS_POISSON_GRID_TRANSFERS_H
#define BENCHMARKS_POISSON_GRID_TRANSFERS_H


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"


template<int k, int dim>
class RestrictionDG0LocalOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern
{
public:
  enum { doPatternVolume = true };

public:
  enum { isLinear = true };

public:
  enum { doAlphaVolume = true };


public:
  template<typename X, typename R, typename LFSU, typename LFSV, typename EG>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const {
    auto *x_base = Dune::PDELab::accessBaseContainer(x).data();
    auto *x0 = x_base + 0;
    auto *r_base = Dune::PDELab::accessBaseContainer(r).data();
    auto *r0 = r_base + 0;

    if constexpr (dim == 2)
      for (int ey = 0; ey < k; ++ey)
        for (int ex = 0; ex < k; ++ex)
          r0[0] += x0[ex + ey * k] / double(k * k);
    else if constexpr (dim == 3)
      for (int ez = 0; ez < k; ++ez)
        for (int ey = 0; ey < k; ++ey)
          for (int ex = 0; ex < k; ++ex)
            r0[0] += x0[ex + ey * k + ez * k * k] / double(k * k * k);
  }
};

#endif //BENCHMARKS_POISSON_GRID_TRANSFERS_H
