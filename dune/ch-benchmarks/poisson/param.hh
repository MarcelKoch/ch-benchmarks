#ifndef BENCHMARKS_POISSON_PARAM_HH
#define BENCHMARKS_POISSON_PARAM_HH

#include <random>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/common/diagonalmatrix.hh>
#include "dune/ch-benchmarks/utility.hh"


template<typename GV, typename RT>
struct Param : public Dune::PDELab::ConvectionDiffusionModelProblem<GV, RT> {
  using Traits = typename Dune::PDELab::ConvectionDiffusionModelProblem<GV, RT>::Traits;

  RT f(const typename Traits::ElementType &e, const typename Traits::DomainType &x) const {
    return 1.;
  }
};

template<typename Grid, typename RF>
struct ParamRandom : public Dune::PDELab::ConvectionDiffusionModelProblem<typename Grid::LeafGridView, RF> {
  using GV = typename Grid::LeafGridView;
  using Traits = typename Dune::PDELab::ConvectionDiffusionModelProblem<GV, RF>::Traits;
  static constexpr int dim = Grid::dimension;

  explicit ParamRandom(const unsigned N)
      : grid_ptr(Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<RF, dim>(0.),
                                                                   Dune::FieldVector<RF, dim>(1.),
                                                                   filled_array<dim>(N))),
        gv(grid_ptr->leafGridView()),
        indexSet(gv.indexSet()),
        rv(gv.size(0))
  {
    std::mt19937 gen(0);
    std::uniform_real_distribution<RF> dist{0.5, 2.};

    std::generate(begin(rv), end(rv), [&]() { return dist(gen); });
  }

  ParamRandom(const ParamRandom&) = default;
  ParamRandom(ParamRandom&&) = default;

  //! tensor diffusion coefficient
  auto A(const typename Traits::ElementType &e, const typename Traits::DomainType &x) const {
    const auto idx = indexSet.index(e);
    Dune::DiagonalMatrix<RF, GV::dimension> I;
    for (std::size_t i = 0; i < Traits::dimDomain; i++)
      I[i] = rv[idx];
    return I;
  }

  RF f(const typename Traits::ElementType &e, const typename Traits::DomainType &x) const {
    return 1.;
  }

  auto const& data() const { return rv; }

private:
  std::shared_ptr<Grid> grid_ptr{};
  const GV& gv;
  using IndexSet = typename GV::IndexSet;
  const IndexSet& indexSet;

  std::vector<RF> rv{};
};


#endif //BENCHMARKS_POISSON_PARAM_HH
