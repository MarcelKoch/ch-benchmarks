#pragma once

#include <mpi.h>
#include <memory>

namespace impl{
  template<typename T>
  auto mpi_type = MPI_DATATYPE_NULL;

  template <> auto mpi_type<int> = MPI_INT;
  template <> auto mpi_type<double> = MPI_DOUBLE;
}


class MPIHelper
{
public:
  MPIHelper(const MPIHelper&) = delete;
  MPIHelper& operator=(const MPIHelper) = delete;

  template<typename T>
  [[nodiscard]] T max(T t) const {
    MPI_Allreduce(&t, &t, 1, impl::mpi_type<T>, MPI_MAX, MPI_COMM_WORLD);
    return t;
  }

  template<typename T>
  [[nodiscard]] T min(T t) const {
    MPI_Allreduce(&t, &t, 1, impl::mpi_type<T>, MPI_MIN, MPI_COMM_WORLD);
    return t;
  }

  static MPIHelper& instance(int& argc, char**& argv)
  {
    // create singleton instance
    static MPIHelper singleton (argc, argv);
    return singleton;
  }

  [[nodiscard]] int rank () const { return rank_; }
  [[nodiscard]] int size () const { return size_; }

private:
  int rank_;
  int size_;
  bool initializedHere_;
  void prevent_warning(int){}

  MPIHelper(int& argc, char**& argv)
      : initializedHere_(false)
  {
    int wasInitialized = -1;
    MPI_Initialized( &wasInitialized );
    if(!wasInitialized)
    {
      rank_ = -1;
      size_ = -1;
      static int is_initialized = MPI_Init(&argc, &argv);
      prevent_warning(is_initialized);
      initializedHere_ = true;
    }

    MPI_Comm_rank(MPI_COMM_WORLD,&rank_);
    MPI_Comm_size(MPI_COMM_WORLD,&size_);
  }

  ~MPIHelper()
  {
    int wasFinalized = -1;
    MPI_Finalized( &wasFinalized );
    if(!wasFinalized && initializedHere_)
    {
      MPI_Finalize();
    }
  }
};