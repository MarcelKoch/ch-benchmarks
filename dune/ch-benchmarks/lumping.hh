#pragma once

#include <dune/istl/solver.hh>
#include <dune/pdelab/backend/solver.hh>
#include <dune/codegen/blockstructured/preconditioner/jacobi/diagonal_matrix_inverse.hh>

template <typename GO, typename V>
class LumpedSolver: public Dune::PDELab::SequentialNorm, public Dune::PDELab::LinearResultStorage{
public:
  template<bool matrix_free>
  LumpedSolver(const GO& go_, std::integral_constant<bool, matrix_free>)
      : go(go_), op(), r(go.testGridFunctionSpace(), 0.), z(go.testGridFunctionSpace(), 0.) {
    typename GO::Domain x(go.trialGridFunctionSpace(), 1.);
    typename GO::Range diag(go.testGridFunctionSpace(), 0.);

    if constexpr (matrix_free) {
      go.jacobian_apply(x, diag);
    } else {
      typename GO::Jacobian J(go);
      go.jacobian(x, J);

      using Dune::PDELab::Backend::native;
      native(J).mv(native(x), native(diag));
    }
    op = std::make_shared<DiagonalMatrixInverse<V>>(diag);
  }

  void apply(V& x){
    r = 0.;
    go.residual(x, r);

    z = 0.;
    Dune::InverseOperatorResult stat;
    op->apply(z, r, stat);
    res.converged  = stat.converged;
    res.iterations = stat.iterations;
    res.elapsed    = stat.elapsed;
    res.reduction  = stat.reduction;
    res.conv_rate  = stat.conv_rate;

    x -= z;
  }

private:
  const GO& go;
  std::shared_ptr<DiagonalMatrixInverse<V>> op;

  V r;
  V z;
};


