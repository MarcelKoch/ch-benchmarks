#pragma once

#include <array>

template<int N, typename T>
constexpr std::array<T, N> filled_array(const T& t){
  std::array<T, N> a{};
  std::fill(begin(a), end(a), t);
  return a;
}

template<typename T>
struct less{
  constexpr bool operator()(const T& a, const T& b) const{
    for (int i = 0; i < a.N(); ++i) {
      if (a[i] < b[i] - 1e-10) return true;
      if (a[i] > b[i] + 1e-10) return false;
    }
    return false;
  }
};
