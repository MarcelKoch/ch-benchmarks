#pragma once

#include <memory>
#include <dune/istl/solver.hh>
#include <dune/istl/solvers.hh>
#include <dune/pdelab/backend/solver.hh>
#include <dune/pdelab/backend/istl/seqistlsolverbackend.hh>
#include <dune/codegen/blockstructured/preconditioner/two_level/additive.hh>

template<typename GO, template<typename> typename Solver, typename PreFine, typename PreCoarse>
class MatrixFreeTwoLevel
    : public Dune::PDELab::SequentialNorm, public Dune::PDELab::LinearResultStorage
{
  using V = typename GO::Traits::Domain;
  using W = typename GO::Traits::Range;
public:
  /*! \brief make a linear solver object

    \param[in] maxiter_ maximum number of iterations to do
    \param[in] verbose_ print messages if true
  */
  explicit MatrixFreeTwoLevel(const GO& go, std::shared_ptr<PreFine> preFine_, std::shared_ptr<PreCoarse> preCoarse_,
                              unsigned maxiter=5000, int verbose=1)
      : opa_(go), preFine(preFine_), preCoarse(preCoarse_), pre(*preFine, *preCoarse), maxiter_(maxiter),
        verbose_(verbose)
  {}

  /*! \brief solve the given linear system

    \param[in] A the given matrix
    \param[out] z the solution vector to be computed
    \param[in] r right hand side
    \param[in] reduction to be achieved
  */
  void apply(V& z, W& r, typename Dune::template FieldTraits<typename W::ElementType >::real_type reduction)
  {
    Solver<V> solver(opa_, pre, reduction, maxiter_, verbose_);
    Dune::InverseOperatorResult stat;
    solver.apply(z, r, stat);
    res.converged  = stat.converged;
    res.iterations = stat.iterations;
    res.elapsed    = stat.elapsed;
    res.reduction  = stat.reduction;
    res.conv_rate  = stat.conv_rate;
  }

  //! Set position of jacobian, ust be called before apply() for nonlinear problems.
  void setLinearizationPoint(const V& u)
  {
    preFine->setLinearizationPoint(u);
    preCoarse->setLinearizationPoint(u);
    opa_.setLinearizationPoint(u);
  }

private:
  Dune::PDELab::OnTheFlyOperator<V,W,GO> opa_;

  std::shared_ptr<PreFine> preFine;
  std::shared_ptr<PreCoarse> preCoarse;
  AdditiveTwoLevel<V, W> pre;

  unsigned maxiter_;
  int verbose_;

};

template<typename GO, typename PreFine, typename PreCoarse>
using MatrixFreeTwoLevelBiCGStab = MatrixFreeTwoLevel<GO, Dune::BiCGSTABSolver, PreFine, PreCoarse>;
