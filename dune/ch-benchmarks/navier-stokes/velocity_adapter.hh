#pragma once

#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include <dune/pdelab/backend/interface.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>

template<typename GFS, typename RF>
class AdapterBase{
  using LFS = Dune::PDELab::LocalFunctionSpace<GFS>;
  using LFSC = Dune::PDELab::LFSIndexCache<LFS>;

  using V = Dune::PDELab::Backend::Vector<GFS, RF>;
  using View = typename V::template ConstLocalView<LFSC>;
public:
  explicit AdapterBase(const GFS& gfs_)
      : lfs(gfs_), lfsc(lfs), v(), view(), loc()
  {}

  AdapterBase(const AdapterBase<GFS, RF>&) = delete;

  void setContainer(V& v_){
    v = Dune::stackobject_to_shared_ptr(v_);
    view.attach(v);
  }

  template<typename EG>
  void bind(const EG& eg){
    lfs.bind(eg.entity());
    lfsc.update();

    loc.resize(lfs.size());

    view.bind(lfsc);
    view.read(loc);
    view.unbind();
  }

protected:
  LFS lfs;
  LFSC lfsc;

  std::shared_ptr<V> v;
  View view;

  Dune::PDELab::LocalVector<RF> loc;
};

template<typename GFS, typename RF>
class VelocityAdapter: public AdapterBase<GFS, RF>{
  using Base = AdapterBase<GFS, RF>;

  using Basis = typename GFS::ChildType::Traits::FiniteElement::Traits::LocalBasisType;
  static constexpr int dim = Basis::Traits::dimDomain;
public:
  explicit VelocityAdapter(const GFS& gfs_)
      : Base(gfs_), cache(), grad_phi()
  {}

  template<typename EG, typename X>
  Dune::FieldVector<RF, dim> operator()(const EG& eg, const X& x) const {
    const auto& basis = this->lfs.child(0).finiteElement().localBasis();
    const auto& phi = this->cache.evaluateFunction(x, basis);

    Dune::FieldVector<RF, dim> val(0.);
    for (int d = 0; d < dim; ++d) {
      for (std::size_t i = 0; i < this->lfs.child(d).size(); ++i) {
        val[d] += this->loc(this->lfs.child(d), i) * phi[i][0];
      }
    }
    return val;
  }

  template<typename EG, typename X>
  Dune::FieldMatrix<RF, dim, dim> gradient(const EG& eg, const X& x) const {
    auto geo = eg.geometry();

    const auto jit = geo.jacobianInverseTransposed(x);

    const auto& basis = this->lfs.child(0).finiteElement().localBasis();
    const auto& js = this->cache.evaluateJacobian(x, basis);

    grad_phi.resize(this->lfs.child(0).size());

    for (std::size_t i = 0; i < this->lfs.child(0).size(); ++i)
      jit.mv(js[i][0], grad_phi[i]);

    Dune::FieldMatrix<RF, dim, dim> grad(0.);
    for (int d = 0; d < dim; ++d) {
      for (std::size_t i = 0; i < this->lfs.child(d).size(); ++i) {
        grad[d] += this->loc(this->lfs.child(d), i) * grad_phi[i];
      }
    }
    return grad;
  }

  template<typename EG, typename X>
  auto divergence(const EG& eg, const X& x) const {
    auto geo = eg.geometry();

    const auto jit = geo.jacobianInverseTransposed(x);

    const auto& basis = this->lfs.child(0).finiteElement().localBasis();
    const auto& js = this->cache.evaluateJacobian(x, basis);

    RF div = 0.;
    for (int d = 0; d < dim; ++d) {
      for (std::size_t i = 0; i < this->lfs.child(d).size(); ++i) {
        div += this->loc(this->lfs.child(d), i) * js[i][0] * jit[d];
      }
    }
    return div;
  }

private:
  mutable Dune::PDELab::LocalBasisCache<Basis> cache;
  mutable std::vector<Dune::FieldVector<RF, dim>> grad_phi;
};

template<typename GFS, typename RF>
class PressureAdapter: public AdapterBase<GFS, RF>{
  using Base = AdapterBase<GFS, RF>;

  using Basis = typename GFS::Traits::FiniteElement::Traits::LocalBasisType;
  static constexpr int dim = Basis::Traits::dimDomain;
public:
  explicit PressureAdapter(const GFS& gfs_)
      : Base(gfs_), cache(), grad_phi()
  {}

  template<typename EG, typename X>
  Dune::FieldVector<RF, dim> gradient(const EG& eg, const X& x) const {
    auto geo = eg.geometry();

    const auto jit = geo.jacobianInverseTransposed(x);

    const auto& basis = this->lfs.finiteElement().localBasis();
    const auto& js = this->cache.evaluateJacobian(x, basis);

    grad_phi.resize(this->lfs.size());

    for (std::size_t i = 0; i < this->lfs.size(); ++i)
      jit.mv(js[i][0], grad_phi[i]);

    Dune::FieldVector<RF, dim> grad(0.);
    for (std::size_t i = 0; i < this->lfs.size(); ++i) {
      grad += this->loc(this->lfs, i) * grad_phi[i];
    }
    return grad;
  }

private:
  mutable Dune::PDELab::LocalBasisCache<Basis> cache;
  mutable std::vector<Dune::FieldVector<RF, dim>> grad_phi;
};
