#pragma once

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/typetree/childextraction.hh>
#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/localoperator/pattern.hh>


template<typename P, typename FEM, typename VA>
class PressureCorrection
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern
{
  using FE_U = typename FEM::Traits::FiniteElement;
  using Basis = typename FE_U::Traits::LocalBasisType;
  using RF = typename Basis::Traits::RangeFieldType;
  Dune::PDELab::LocalBasisCache<Basis> cache = {};

  P param;
  RF dt;

  VA& ustar_adapter;

public:
  static constexpr bool doPatternVolume = true;
  static constexpr bool doAlphaVolume = true;

  explicit PressureCorrection(P param_, RF dt_, VA& ustar_adapter_)
      : param(std::move(param_)), dt(dt_), ustar_adapter(ustar_adapter_){}

  template<typename EG, typename LFSU_HAT, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU_HAT& lfsu, const X& x, const LFSV& lfsv, R& r) const {
    ustar_adapter.bind(eg);

    const auto& basis = lfsu.finiteElement().localBasis();

    constexpr int dim = EG::Entity::dimension;
    auto geo = eg.geometry();

    std::vector<Dune::FieldVector<RF, dim>> grad_phi(lfsu.size());

    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 4)){
      const auto& js = cache.evaluateJacobian(qp.position(), basis);
      const auto& phi = cache.evaluateFunction(qp.position(), basis);

      const auto jit = geo.jacobianInverseTransposed(qp.position());
      const auto detjac = geo.integrationElement(qp.position());

      for (std::size_t i = 0; i < lfsu.size(); ++i)
        jit.mv(js[i][0], grad_phi[i]);

      Dune::FieldVector<RF, dim> grad_p(0.);
      for (std::size_t i = 0; i < lfsu.size(); ++i)
        grad_p += x(lfsu, i) * grad_phi[i];

      const auto div = ustar_adapter.divergence(eg, qp.position());

      const auto factor = qp.weight() * detjac;
      for (std::size_t i = 0; i < lfsu.size(); ++i)
        r.accumulate(lfsu, i, (grad_phi[i] * grad_p + phi[i] * div * param.rho / dt) * factor);
    }
  }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Jacobian>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, Jacobian& mat) const{
    const auto& basis = lfsu.finiteElement().localBasis();

    constexpr int dim = EG::Entity::dimension;
    auto geo = eg.geometry();

    std::vector<Dune::FieldVector<RF, dim>> grad_phi(lfsu.size());

    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 4)) {
      const auto& js = cache.evaluateJacobian(qp.position(), basis);

      const auto jit = geo.jacobianInverseTransposed(qp.position());
      const auto detjac = geo.integrationElement(qp.position());

      for (std::size_t i = 0; i < lfsu.size(); ++i)
        jit.mv(js[i][0], grad_phi[i]);

      const auto factor = qp.weight() * detjac;
      for (std::size_t i = 0; i < lfsu.size(); ++i)
        for (std::size_t j = 0; j < lfsu.size(); ++j)
          mat.accumulate(lfsu, i, lfsu, j, (grad_phi[i] * grad_phi[j]) * factor);
    }
  }
};
