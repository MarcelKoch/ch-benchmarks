#pragma once

#include <cmath>

template<typename RF>
struct Channel{
  template<typename X>
  bool isDirichlet_u(const X&x){
    return std::abs(x[1]) < 1e-10 or std::abs(x[1] - 1) < 1e-10;
  }

  template<typename X>
  bool isDirichlet_p(const X&x){
    return std::abs(x[0]) < 1e-10 or std::abs(x[0] - 1) < 1e-10;
  }

  template<typename X>
  RF p(const X&x){
    if (std::abs(x[0]) < 1e-10) return RF(8.);
    else return RF(0.);
  }

  template<typename X>
  Dune::FieldVector<RF, 2> u(const X& x){
    return {0., 0.};
  }

  RF rho = 1.;
  RF nu = 1.;
};

template<typename RF>
struct FlowPastCylinder{
  template<typename X>
  bool isDirichlet_u(const X&x){
    Dune::FieldVector<RF, 2> o{0.2, 0.2};
    if (std::abs(x[1]) < 1e-10 or std::abs(x[1] - 0.41) < 1e-10 or (x - o).two_norm() < 0.05 + 1e-8 or
        std::abs(x[0]) < 1e-10)
      return true;
    else
      return false;
  }

  template<typename X>
  bool isDirichlet_p(const X&x){
    return std::abs(x[0] - 2.2) < 1e-10;
  }

  template<typename X>
  RF p(const X&x){
    if (std::abs(x[0] - 2.2) < 1e-10) return RF(0.);
    else return RF(0.);
  }

  template<typename X>
  Dune::FieldVector<RF, 2> u(const X& x){
    if (std::abs(x[0]) < 1e-10) {
      const RF scaling = std::sin(M_PI * t / 8.);
      return {scaling * 1.5 * (4 * x[1] * (0.41 - x[1])) / (0.41 * 0.41), 0};
    }
    else
      return {0., 0.};
  }

  void setTime(const RF t_) { t = t_; }

  RF rho = 1.;
  RF nu = 0.001;
  RF t = 0.;
};

