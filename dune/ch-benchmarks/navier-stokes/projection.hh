#pragma once

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/typetree/childextraction.hh>
#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/localoperator/pattern.hh>


template<typename P, typename FEM, typename VA, typename PA>
class Projection
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern
{
  using FE_U = typename FEM::Traits::FiniteElement;
  using Basis = typename FE_U::Traits::LocalBasisType;
  using RF = typename Basis::Traits::RangeFieldType;
  Dune::PDELab::LocalBasisCache<Basis> cache = {};

  P param;
  RF dt;

  VA& ustar_adapter;
  PA& p_adapter;
public:
  static constexpr bool doPatternVolume = true;
  static constexpr bool doAlphaVolume = true;

  explicit Projection(P param_, RF dt_, VA& un_adapter_, PA& p_adapter_)
      : param(std::move(param_)), dt(dt_), ustar_adapter(un_adapter_), p_adapter(p_adapter_) {}

  template<typename EG, typename LFSU_HAT, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU_HAT& lfsu_hat, const X& x, const LFSV& lfsv, R& r) const {
    ustar_adapter.bind(eg);
    p_adapter.bind(eg);

    const auto& lfs = Dune::TypeTree::child(lfsu_hat, Dune::Indices::_0);

    const auto& basis = lfs.finiteElement().localBasis();

    constexpr int dim = EG::Entity::dimension;
    auto geo = eg.geometry();

    std::vector<Dune::FieldVector<RF, dim>> grad_phi(lfs.size(), Dune::FieldVector<RF,dim>(0.));

    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 6)){
      const auto& phi = cache.evaluateFunction(qp.position(), basis);

      const auto detjac = geo.integrationElement(qp.position());

      Dune::FieldVector<RF, dim> u(0.);
      for (int d = 0; d < dim; ++d)
        for (std::size_t i = 0; i < lfs.size(); ++i)
          u[d] += x(lfsu_hat.child(d), i) * phi[i];

      const auto u_star = ustar_adapter(eg, qp.position());
      const auto grad_p = p_adapter.gradient(eg, qp.position());

      const auto factor = qp.weight() * detjac;
      for (int d = 0; d < dim; ++d)
        for (std::size_t i = 0; i < lfs.size(); ++i) {
          r.accumulate(lfsu_hat.child(d), i, ((u[d] - u_star[d] + dt / param.rho * grad_p[d]) * phi[i])
                                             * factor);
        }
    }
  }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Jacobian>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, Jacobian& mat) const{
    const auto& lfs = Dune::TypeTree::child(lfsu, Dune::Indices::_0);

    const auto& basis = lfs.finiteElement().localBasis();

    constexpr int dim = EG::Entity::dimension;
    auto geo = eg.geometry();

    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 6)) {
      const auto &phi = cache.evaluateFunction(qp.position(), basis);

      const auto detjac = geo.integrationElement(qp.position());

      const auto factor = qp.weight() * detjac;
      for (int d = 0; d < dim; ++d)
        for (std::size_t i = 0; i < lfs.size(); ++i)
          for (std::size_t j = 0; j < lfs.size(); ++j)
            mat.accumulate(lfsu.child(d), i, lfsu.child(d), j, (phi[i] * phi[j]) * factor);
    }
  }
};
