#pragma  once

#include <dune/common/fmatrix.hh>

template<typename RF, int dim>
Dune::FieldMatrix<RF, dim, dim> identity(){
  Dune::FieldMatrix<RF, dim, dim> Id{};
  for (int i = 0; i < dim; ++i)
    Id[i][i] = 1.;
  return Id;
}

template<typename M>
constexpr M transpose(const M& A){
  M t{};
  for (int i = 0; i < A.rows; ++i)
    for (int j = 0; j < A.cols; ++j)
      t[j][i] = A[i][j];
  return t;
}

template<typename M>
constexpr typename M::field_type trace(const M& A){
  typename M::field_type tr{};
  for (int i = 0; i < A.rows; ++i)
    tr += A[i][i];
  return tr;
}


template<typename RF, int d>
RF scalar_product(const Dune::FieldMatrix<RF, d, d>& a, const Dune::FieldMatrix<RF, d, d>& b){
  RF r(0.);
  for (int i = 0; i < d; ++i)
      r += a[i] * b[i];
  return r;
}



template<typename RF, int dim>
class NeoHookMaterial{
public:
  NeoHookMaterial(RF mu_, RF lambda_): mu(mu_), lambda(lambda_) {}

  auto first_piola_stress_tensor(const Dune::FieldMatrix<RF, dim, dim>& grad_u) const {
    const auto F = Id + grad_u;
    const auto Fit = [&]{ auto A = F; A.invert(); return transpose(A);}();
    const auto J = F.determinant();

    return mu * F - (mu - lambda * std::log(J)) * Fit;
  }

  auto derivative(const Dune::FieldMatrix<RF, dim, dim>& grad_u, const Dune::FieldMatrix<RF, dim, dim>& grad_phi) const{
    const auto F = Id + grad_u;
    const auto Fit = [&]{ auto A = F; A.invert(); return transpose(A);}();
    const auto J = F.determinant();

    const auto sp = scalar_product(Fit, grad_phi);
    const auto T = Fit.rightmultiplyany(transpose(grad_phi).rightmultiplyany(Fit));

//    RF sp[dim] = {};
//    for (int i = 0; i < dim; ++i)
//      sp[i] = Fit[i] * grad_phi;
//    Dune::FieldMatrix<RF, dim, dim> T(0.); // T = Fit * grad_phi^T * Fit
//    for (int i = 0; i < dim; ++i)
//      for (int k = 0; k < dim; ++k)
//        T[i][k] = sp[i] * Fit[d][k];

    return mu * grad_phi - (lambda * sp * Fit - (mu - lambda * std::log(J)) * T);
  }

private:
  RF mu = {};
  RF lambda = {};
  inline static const Dune::FieldMatrix<RF, dim, dim> Id = identity<RF, dim>();
};


template<typename RF, int dim>
class StVenantKirchoffMaterial{
public:
  StVenantKirchoffMaterial(RF mu_, RF lambda_): mu(mu_), lambda(lambda_) {}

  auto first_piola_stress_tensor(const Dune::FieldMatrix<RF, dim, dim>& grad_u) const {
    const auto F = Id + grad_u;
    const auto E = 0.5 * (grad_u + transpose(grad_u) + transpose(grad_u).rightmultiplyany(grad_u));

    return lambda * trace(E) * F + 2 * mu * F.rightmultiplyany(E);
  }

private:
  RF mu = {};
  RF lambda = {};
  inline static const Dune::FieldMatrix<RF, dim, dim> Id = identity<RF, dim>();
};

template<typename Material, typename RF>
struct HyperelasticityParameter{
  explicit HyperelasticityParameter(Material material_, RF theta_)
      : material(std::move(material_)), theta(theta_) {}

  template<typename E, typename X>
  Dune::FieldVector<RF, 3> f(const E& e, const X& x) const {
    return {0., -0.5, 0.};
  }

  template<typename IS, typename X>
  Dune::FieldVector<RF, 3> j(const IS& is, const X& x) const {
    return {0.1, 0., 0.};
  }

  template<typename X>
  Dune::FieldVector<RF, 3> g(const X& x) const {
    const auto scaled_theta = x[0] * theta;
    Dune::FieldMatrix<RF, 3, 3> rot{{1., 0., 0.},
                                    {0., std::cos(scaled_theta), -std::sin(scaled_theta)},
                                    {0., std::sin(scaled_theta), std::cos(scaled_theta)}};
    Dune::FieldVector<RF, 3> origin{x[0], 0.5, 0.5};
    Dune::FieldVector<RF, 3> y{};
    rot.mv(x - origin, y);

    return y + (origin - x);
  }

  template<typename IS, typename X>
  bool isDirichlet(const IS& is, const X& x) const {
    const auto global = is.geometry().global(x);
    return (std::abs(global[0]) < 1e-10) or (std::abs(global[0] - 1) < 1e-10);
  }

  Material material = {};
  RF theta = M_PI / 3.;
};
