#pragma once

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/typetree/childextraction.hh>
#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>


template<typename P, typename FEM>
class Hyperelasticity
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::NumericalJacobianApplyVolume<Hyperelasticity<P, FEM>>,
      public Dune::PDELab::NumericalJacobianApplyBoundary<Hyperelasticity<P, FEM>>,
      public Dune::PDELab::NumericalJacobianVolume<Hyperelasticity<P, FEM>>,
      public Dune::PDELab::NumericalJacobianBoundary<Hyperelasticity<P, FEM>>
{
  using FE = typename FEM::Traits::FiniteElement;
  using Basis = typename FE::Traits::LocalBasisType;
  using RF = typename Basis::Traits::RangeFieldType;
  Dune::PDELab::LocalBasisCache<Basis> cache = {};

  P param;
public:
  static constexpr bool doPatternVolume = true;
  static constexpr bool doAlphaVolume = true;
  static constexpr bool doAlphaBoundary = true;

  explicit Hyperelasticity(P param_): param(std::move(param_)) {}

  template<typename EG, typename LFSU_HAT, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU_HAT& lfsu_hat, const X& x, const LFSV& lfsv, R& r) const {
    const auto& lfsu = Dune::TypeTree::child(lfsu_hat, Dune::Indices::_0);

    const auto& basis = lfsu.finiteElement().localBasis();

    constexpr int dim = EG::Entity::dimension;
    const auto& cell = eg.entity();
    auto geo = eg.geometry();

    const auto jit = geo.jacobianInverseTransposed({});
    const auto detjac = geo.integrationElement({});

    std::vector<Dune::FieldVector<RF, dim>> grad_phi(lfsu.size());

    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 2)){
      const auto& phi = cache.evaluateFunction(qp.position(), basis);
      const auto& js = cache.evaluateJacobian(qp.position(), basis);

      for (std::size_t i = 0; i < lfsu.size(); ++i)
        jit.mv(js[i][0], grad_phi[i]);

      Dune::FieldMatrix<RF, dim, dim> grad_u{};
      for (int d = 0; d < dim; ++d)
        for (int i = 0; i < lfsu.size(); ++i)
          grad_u[d] += x(lfsu_hat.child(d), i) * grad_phi[i];

      const auto factor = qp.weight() * detjac;

      const auto stress_tensor = param.material.first_piola_stress_tensor(grad_u);

      const auto body_force = param.f(cell, qp.position());
      for (int d = 0; d < dim; ++d)
        for (int i = 0; i < lfsu.size(); ++i)
          r.accumulate(lfsu_hat.child(d), i, (stress_tensor[d] * grad_phi[i] - body_force[d] * phi[i]) * factor);
    }
  }

  template<typename IG, typename LFSU_HAT, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig, const LFSU_HAT& lfsu_hat, const X& x, const LFSV& lfsv, R& r) const {
    const auto& lfsu = Dune::TypeTree::child(lfsu_hat, Dune::Indices::_0);

    const auto& basis = lfsu.finiteElement().localBasis();

    constexpr int dim = IG::Entity::dimension;
    const auto& cell = ig.inside();
    const auto geo = ig.geometry();
    const auto local_geo = ig.geometryInInside();

    auto ref_el = referenceElement(local_geo);
    auto local_face_center = ref_el.position(0, 0);

    if (param.isDirichlet(ig, local_face_center)) return;

    const auto detjac = geo.integrationElement({});

    for (const auto &qp: Dune::PDELab::quadratureRule(local_geo, 2)) {
      const auto &phi = cache.evaluateFunction(local_geo.global(qp.position()), basis);

      const auto factor = qp.weight() * detjac;

      const auto traction_force = param.j(ig, qp.position());
      for (int d = 0; d < dim; ++d)
        for (int i = 0; i < lfsu.size(); ++i)
          r.accumulate(lfsu_hat.child(d), i, -traction_force[d] * phi[i] * factor);
    }
  }

//  template<typename EG, typename LFSU_HAT, typename X, typename LFSV, typename J>
//  void jacobian_volume(const EG& eg, const LFSU_HAT& lfsu_hat, const X& x, const LFSV& lfsv_hat, J& jac) const {
//    const auto& lfsu = Dune::TypeTree::child(lfsu_hat, Dune::Indices::_0);
//    const auto& lfsv = Dune::TypeTree::child(lfsv_hat, Dune::Indices::_0);
//
//    const auto& basis = lfsu.finiteElement().localBasis();
//
//    constexpr int dim = EG::Entity::dimension;
//    const auto& cell = eg.entity();
//    auto geo = eg.geometry();
//
//    const auto jit = geo.jacobianInverseTransposed({});
//    const auto detjac = geo.integrationElement({});
//
//    std::vector<Dune::FieldVector<RF, dim>> grad_phi(lfsu.size());
//
//    for (const auto& qp: Dune::PDELab::quadratureRule(geo, 2)){
//      const auto& phi = cache.evaluateFunction(qp.position(), basis);
//      const auto& js = cache.evaluateJacobian(qp.position(), basis);
//
//      for (std::size_t i = 0; i < lfsu.size(); ++i)
//        jit.mv(js[i][0], grad_phi[i]);
//
//      Dune::FieldMatrix<RF, dim, dim> grad_u{};
//      for (int d = 0; d < dim; ++d)
//        for (int i = 0; i < lfsu.size(); ++i)
//          grad_u[d] += x(lfsu_hat.child(d), i) * grad_phi[i];
//
//      const auto factor = qp.weight() * detjac;
//      for (int j = 0; j < lfsu.size(); ++j) {
//        for (int jd = 0; jd < dim; ++jd) {
//          Dune::FieldMatrix<RF, dim, dim> grad_phi_j(0.);
//          grad_phi_j[jd] = grad_phi[j];
//          const auto derivative = param.material.derivative(grad_u, grad_phi_j);
//          for (int i = 0; i < lfsv.size(); ++i) {
//            for (int id = 0; id < dim; ++id){
//              jac.accumulate(lfsu_hat.child(jd), j, lfsv_hat.child(id), i,
//                             derivative[id] * grad_phi[i] * factor);
//            }
//          }
//        }
//      }
//    }
//  }
//
//  template<typename LFSV, typename IG, typename X, typename LFSU, typename J>
//  void jacobian_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, J& jac_s_s) const{}
};
