// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/grid/yaspgrid.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/solver/newton.hh>

#include <dune/ch-benchmarks/timings.hh>
#include <dune/ch-benchmarks/mpihelper.hh>
#include <dune/ch-benchmarks/p-laplacian/param.hh>
#include <dune/ch-benchmarks/p-laplacian/localoperator.hh>

#include <likwid.h>

constexpr int dim = 2;


int main(int argc, char** argv)
{
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;
  const auto& helper = MPIHelper::instance(argc, argv);

  const auto type = std::string(argv[1]);
  const auto N = static_cast<unsigned>(std::stoul(argv[2]));

  const std::set<std::string> valid_types = {"solver", "verify", "operator"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using RF = double;

  using Grid = Dune::YaspGrid<dim>;
  using GV = typename Grid::LeafGridView;

  auto grid_p = Dune::StructuredGridFactory<Grid>::createCubeGrid({0., 0.}, {1., 1.},
                                                                  {N, N});
  auto gv = grid_p->leafGridView();

  Param<RF> param{1.5, 1e-10};

  using FEM = Dune::PDELab::QkLocalFiniteElementMap<GV, RF, RF, 1>;
  FEM fem(gv);

  using CON = Dune::PDELab::ConformingDirichletConstraints;
  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, CON>;
  GFS gfs(gv, fem);
  gfs.name("Q1");

  using CC = GFS::template ConstraintsContainer<RF>::Type;
  CC cc{};
  Dune::PDELab::constraints(Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&](const auto& is, const auto& x){
                              return param.isDirichlet(is, x);
                            }),
                            gfs, cc);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  MB mb(dim * Dune::power(3, dim));

  using LOP = PLaplacian<decltype(param), FEM>;
  LOP lop(param);

  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RF, RF, RF, CC, CC>;
  GOP gop(gfs, cc, gfs, cc, lop, mb);

  std::cout << "DIM " << N << "x" << N << std::endl;
  std::cout << "gfs with " << gfs.size() << " dofs generated  "<< std::endl;
  std::cout << "cc with " << cc.size() << " dofs generated  "<< std::endl;

  using V = Dune::PDELab::Backend::Vector<GFS, RF>;
  if (type == "operator" or type == "get_iterations"){
    using Dune::PDELab::Backend::native;
    V x(gfs, 0.), y(gfs, 0.), z(gfs, 0.);
    std::generate(begin(*x.storage()), end(*x.storage()), std::rand);
    std::generate(begin(*z.storage()), end(*z.storage()), std::rand);

    using Jacobian = Dune::PDELab::Backend::Matrix<MB, V, V, RF>;
    Jacobian J(gop);

    likwid_timed("assemble", helper, [&]{ gop.jacobian(z, J); });
    likwid_timed("operator", helper, [&]{ native(J).mv(native(x), native(y)); });
  }
  else if(type == "solver" or type == "verify") {
    using LS = Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<GOP>;
    LS ls(5000, 1);

    using Solver = Dune::PDELab::NewtonMethod<GOP, LS>;
    Solver solver(gop, ls);

    V x(gfs, 0.);
    Dune::PDELab::interpolate(Dune::PDELab::makeGridFunctionFromCallable(gv, [&](const auto &e, const auto& x)
                              { return param.g(e, x); }),
                              gfs, x);
    Dune::PDELab::set_nonconstrained_dofs(cc, 0., x);

    int verbosity = 1;
    likwid_timed("operator", helper, [&]{
                   x = 0.;
                   LS ls(5000, verbosity);
                   Solver solver(gop, ls);
                   solver.setVerbosityLevel(verbosity);
                   solver.apply(x);
                   },
                 [&]{ verbosity = 1; }, [&]{ verbosity = 0; });

    if (type == "verify") {
      using DGF = Dune::PDELab::DiscreteGridFunction<GFS, V>;
      DGF dgf(gfs, x);
      Dune::VTKWriter<GV> vtkwriter(gv, Dune::VTK::conforming);
      typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;
      vtkwriter.addVertexData(std::make_shared<VTKF>(dgf, "fesol"));
      vtkwriter.write("handwritten-verify", Dune::VTK::appendedraw);
    }
  }
  LIKWID_MARKER_CLOSE;
}
