// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/solver/newton.hh>
#include <dune/ch-benchmarks/timings.hh>
#include <dune/ch-benchmarks/linearsolver.hh>
#include <dune/ch-benchmarks/nullstream.hh>
#include <dune/ch-benchmarks/mpihelper.hh>
#include <likwid.h>

#ifdef KERNEL_TIMINGS
#define ENABLE_HP_TIMERS 1
#endif

#include "p-laplacian_generated_ilvl-@ilvl@_block-@block@_driverblock.hh"


constexpr int k = @block@;
constexpr int dim = 2;


int main(int argc, char** argv)
{
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;
  const auto& helper = MPIHelper::instance(argc, argv);

  const auto type = std::string(argv[1]);
  const auto N = static_cast<unsigned>(std::stoul(argv[2]));

  const std::set<std::string> valid_types = {"solver", "verify", "operator"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using RF = double;

  using Grid = Dune::YaspGrid<dim>;
  using GV = typename Grid::LeafGridView;

  auto grid_p = Dune::StructuredGridFactory<Grid>::createCubeGrid({0., 0.}, {1., 1.},
                                                                  {N, N});
  auto gv = grid_p->leafGridView();

  std::cout << "DIM " << N * k << "x" << N * k << std::endl;

  using DB = DriverBlock<GV>;
  DB driver(gv, {});

  using GOP = DB::GO_r;
  using GFS = DB::BCG1_dirichlet_GFS ;
  using V = DB::V_R;
  auto go = driver.getGridOperator_r();
  auto gfs = driver.getGridFunctionsSpace_BCG1();

  if (type == "operator" or type == "get_iterations"){
    V x(*gfs, 0.), y(*gfs, 0.), z(*gfs, 0.);
    std::generate(begin(*x.storage()), end(*x.storage()), std::rand);
    std::generate(begin(*z.storage()), end(*z.storage()), std::rand);

    likwid_timed("operator", helper,
                 [&]{ go->jacobian_apply(x, z, y);},
                 [&]{ NullStream nullStream{}; driver.lop_r->dump_timers(nullStream, "KERNEL", true); });
    driver.lop_r->dump_timers(std::cout, "KERNEL", true);
  }
  else if(type == "solver" or type == "verify") {
    using PreFine = DB::Jacobi;
    using PreCoarse = DB::CGC;
    auto preFine = driver.pre_fine;
    auto preCoarse = driver.pre_coarse;

    using LS = MatrixFreeTwoLevelBiCGStab<GOP, PreFine, PreCoarse>;

    using Solver = Dune::PDELab::NewtonMethod<GOP, LS, true>;

    auto& x = *driver.getCoefficient_r();

    int verbosity = 1;
    likwid_timed("operator", helper, [&]{
                   x = 0.;
                   LS ls(*go, preFine, preCoarse, 5000, verbosity);
                   Solver solver(*go, ls);
                   solver.setVerbosityLevel(verbosity);
                   solver.apply(x);
                 },
                 [&]{ verbosity = 1; }, [&]{ verbosity = 0; });

    if (type == "verify") {
      using DGF = Dune::PDELab::DiscreteGridFunction<GFS, V>;
      DGF dgf(*gfs, x);
      Dune::SubsamplingVTKWriter<GV> vtkwriter(gv, Dune::refinementIntervals(k));
      typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;
      vtkwriter.addVertexData(std::make_shared<VTKF>(dgf, "fesol"));
      vtkwriter.write("generated-verify", Dune::VTK::appendedraw);
    }
  }
  LIKWID_MARKER_CLOSE;
}
