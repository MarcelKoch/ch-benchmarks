// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/stationary/linearproblem.hh>

#include <dune/ch-benchmarks/mpihelper.hh>
#include <dune/ch-benchmarks/timings.hh>
#include <dune/ch-benchmarks/lumping.hh>
#include <dune/ch-benchmarks/navier-stokes/param.hh>
#include <dune/ch-benchmarks/navier-stokes/velocity_adapter.hh>
#include <dune/ch-benchmarks/navier-stokes/intermediate.hh>
#include <dune/ch-benchmarks/navier-stokes/pressure_correction.hh>
#include <dune/ch-benchmarks/navier-stokes/projection.hh>

#include <likwid.h>

constexpr int dim = 2;


int main(int argc, char** argv)
{
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;
  const auto& helper = MPIHelper::instance(argc, argv);

  const auto type = std::string(argv[1]);
  const auto levels = static_cast<unsigned>(std::stoul(argv[2]));

  const auto T = 8.;
  const auto dt = 1. / 1600.;

  const std::set<std::string> valid_types = {"solver", "verify", "operator", "assemble", "get_iterations"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using RF = double;

  using Grid = Dune::UGGrid<dim>;
  using GV = typename Grid::LeafGridView;

  std::unique_ptr<Grid> grid_p = Dune::GmshReader<Grid>::read("flow_around_cylinder.msh");
  grid_p->globalRefine(levels);
  auto gv = grid_p->leafGridView();

  FlowPastCylinder<RF> param;

#ifdef STABLE
  constexpr int deg_u = 2;
#else
  constexpr int deg_u = 1;
#endif
  using FEM_U = Dune::PDELab::QkLocalFiniteElementMap<GV, RF, RF, deg_u>;
  FEM_U fem_u(gv);

  using GFS_Q2_UC = Dune::PDELab::GridFunctionSpace<GV, FEM_U>;
  GFS_Q2_UC gfs_q2_uc(gv, fem_u);
  using GFS_UC = Dune::PDELab::PowerGridFunctionSpace<GFS_Q2_UC, dim, GFS_Q2_UC::Traits::Backend>;
  GFS_UC gfs_uc(gfs_q2_uc);
  gfs_uc.update();

  using CON = Dune::PDELab::ConformingDirichletConstraints;
  using GFS_Q2 = Dune::PDELab::GridFunctionSpace<GV, FEM_U, CON>;
  GFS_Q2 gfs_q2(gv, fem_u);

  using VB = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>;
  using GFS_U = Dune::PDELab::PowerGridFunctionSpace<GFS_Q2, dim, VB, Dune::PDELab::EntityBlockedOrderingTag>;
  GFS_U gfs_u(gfs_q2);
  gfs_u.name("velocity");
  gfs_u.child(0).name("U");
  gfs_u.child(1).name("V");
  gfs_u.update();

  using CC_U = GFS_U::template ConstraintsContainer<RF>::Type;
  CC_U cc_u{};
  Dune::PDELab::constraints(Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&](const auto& x){
                              return param.isDirichlet_u(x);
                            }),
                            gfs_u, cc_u);

  using FEM_P = Dune::PDELab::QkLocalFiniteElementMap<GV, RF, RF, 1>;
  FEM_P fem_p(gv);

  using GFS_P = Dune::PDELab::GridFunctionSpace<GV, FEM_P, CON>;
  GFS_P gfs_p(gv, fem_p);
  gfs_p.name("pressure");
  gfs_p.update();

  using CC_P = GFS_P::template ConstraintsContainer<RF>::Type;
  CC_P cc_p{};
  Dune::PDELab::constraints(Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&](const auto& x){
                              return param.isDirichlet_p(x);
                            }),
                            gfs_p, cc_p);

  std::cout << "elements " << gv.size(0) << std::endl;
  std::cout << "gfs_u with " << gfs_u.size() << " dofs generated  " << std::endl;
  std::cout << "cc_u with " << cc_u.size() << " dofs generated  " << std::endl;
  std::cout << "gfs_p with " << gfs_p.size() << " dofs generated  " << std::endl;
  std::cout << "cc_p with " << cc_p.size() << " dofs generated  " << std::endl;

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;

  using IntermediateAdapter = VelocityAdapter<GFS_U, RF>;
  using ProjectionAdapter = VelocityAdapter<GFS_UC, RF>;
  IntermediateAdapter ustar_adapter(gfs_u);
  ProjectionAdapter un_adapter(gfs_uc);

  using PressureAdapter = PressureAdapter<GFS_P, RF>;
  PressureAdapter p_adapter(gfs_p);

  using IntermediateLOP = IntermediateVelocity<decltype(param), FEM_U, ProjectionAdapter>;
  IntermediateLOP intermediate_lop(param, dt, un_adapter);
  using IntermediateGOP = Dune::PDELab::GridOperator<GFS_U, GFS_U, IntermediateLOP, MB, RF, RF, RF, CC_U, CC_U>;
  IntermediateGOP intermediate_gop(gfs_u, cc_u, gfs_u, cc_u, intermediate_lop,
                                   MB(dim * Dune::power((2 * deg_u + 1), dim)));

  using PressureLOP = PressureCorrection<decltype(param), FEM_P, IntermediateAdapter>;
  PressureLOP pressure_lop(param, dt, ustar_adapter);
  using PressureGOP = Dune::PDELab::GridOperator<GFS_P, GFS_P, PressureLOP, MB, RF, RF, RF, CC_P, CC_P>;
  PressureGOP pressure_gop(gfs_p, cc_p, gfs_p, cc_p, pressure_lop, MB(Dune::power(3, dim)));

  using ProjectionLOP = Projection<decltype(param), FEM_U, IntermediateAdapter, PressureAdapter>;
  ProjectionLOP projection_lop(param, dt, ustar_adapter, p_adapter);
  using ProjectionGOP = Dune::PDELab::GridOperator<GFS_UC, GFS_UC, ProjectionLOP, MB, RF, RF, RF>;
  ProjectionGOP projection_gop(gfs_uc, gfs_uc, projection_lop,
                               MB(dim * Dune::power((2 * deg_u + 1), dim)));

  using V_U = Dune::PDELab::Backend::Vector<GFS_U, RF>;
  using V_UC = Dune::PDELab::Backend::Vector<GFS_UC, RF>;
  using V_P = Dune::PDELab::Backend::Vector<GFS_P, RF>;

  if (type == "operator" or type == "get_iterations"){
    typename IntermediateGOP::Jacobian J_u(intermediate_gop);
    V_U x_u(gfs_u, 1.), y_u(gfs_u, 0.);
    intermediate_gop.jacobian(x_u, J_u);

    typename PressureGOP ::Jacobian J_p(pressure_gop);
    V_P x_p(gfs_p, 1.), y_p(gfs_p, 0.);
    pressure_gop.jacobian(x_p, J_p);

    V_UC x_uc(gfs_uc, 1.), y_uc(gfs_uc, 0.);
    ustar_adapter.setContainer(x_u);
    p_adapter.setContainer(x_p);

    using Dune::PDELab::Backend::native;

    auto f_intermediate = [&]{ native(J_u).mv(native(x_u), native(y_u)); };
    auto f_pressure = [&]{ native(J_p).mv(native(x_p), native(y_p)); };
    auto f_projection = [&]{ projection_gop.residual(x_uc, y_uc); };

    likwid_timed("operator_intermediate", helper, f_intermediate);
    likwid_timed("operator_pressure", helper, f_pressure);
    likwid_timed("operator_projection", helper, f_projection);
  }
  else if(type == "solver" or type == "verify") {
    const auto steps = std::stoul(argv[3]);

    using IntermediateLS = Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<IntermediateGOP>;
    IntermediateLS intermediate_ls(5000, 0, true);
    using IntermediateSolver = Dune::PDELab::StationaryLinearProblemSolver<IntermediateGOP, IntermediateLS, V_U>;
    IntermediateSolver intermediate_solver(intermediate_gop, intermediate_ls, 1e-10, 1e-99, 0);
    intermediate_solver.setKeepMatrix(true);

    using PressureLS = Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<PressureGOP>;
    PressureLS pressure_ls(5000, 0, true);
    using PressureSolver = Dune::PDELab::StationaryLinearProblemSolver<PressureGOP, PressureLS, V_P>;
    PressureSolver pressure_solver(pressure_gop, pressure_ls, 1e-10, 1e-99, 0);
    pressure_solver.setKeepMatrix(true);

    using ProjectionLS = LumpedSolver<ProjectionGOP, V_UC>;
    ProjectionLS projection_solver(projection_gop, std::false_type{});

    V_U u_star(gfs_u, 0.), dir_values_u(gfs_u, 0.);
    V_UC u_n(gfs_uc, 0.);
    V_P p(gfs_p, 0.);

    auto dir_u = Dune::PDELab::makeInstationaryGridFunctionFromCallable(gv, [&](const auto& x) { return param.u(x);},
                                                                        param);
    auto dir_p = Dune::PDELab::makeGridFunctionFromCallable(gv, [&](const auto& x) { return param.p(x);});

    Dune::PDELab::interpolate(dir_u, gfs_u, dir_values_u);
    Dune::PDELab::interpolate(dir_p, gfs_p, p);

    Dune::PDELab::copy_constrained_dofs(cc_u, dir_values_u, u_star);
    Dune::PDELab::set_nonconstrained_dofs(cc_p, 0., p);

    Dune::VTKSequenceWriter<GV> vtkSequenceWriter(std::make_shared<Dune::VTKWriter<GV>>(gv),
                                                  "handwritten-verify");
    using DGF_U = Dune::PDELab::VectorDiscreteGridFunction<GFS_U, V_U>;
    DGF_U dgf_u(gfs_u, u_star);
    using DGF_P = Dune::PDELab::DiscreteGridFunction<GFS_P, V_P>;
    DGF_P dgf_p(gfs_p, p);
    vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF_U>>(dgf_u, "velocity"));
    vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF_P>>(dgf_p, "pressure"));
    if (type == "verify"){
      vtkSequenceWriter.write(0., Dune::VTK::appendedraw);
    }

    RF t = 0.;
    int dt_out = 10;
    int next_out = dt_out;

    std::vector<Dune::PDELab::LinearSolverResult<RF>> intermediate_results;
    std::vector<Dune::PDELab::LinearSolverResult<RF>>  pressure_results;

    {
      V_U dummy(gfs_u, 0.);
      const auto real_reduction = intermediate_solver.reduction();
      intermediate_solver.setReduction(100);
      un_adapter.setContainer(u_n);
      intermediate_solver.apply(dummy);
      intermediate_solver.setReduction(real_reduction);
    }
    {
      V_P dummy(gfs_p, 0.);
      const auto real_reduction = pressure_solver.reduction();
      pressure_solver.setReduction(100);
      ustar_adapter.setContainer(u_star);
      pressure_solver.apply(dummy);
      pressure_solver.setReduction(real_reduction);
    }

    using namespace std::chrono_literals;
    std::chrono::steady_clock::duration intermediate_duration{0};
    std::chrono::steady_clock::duration pressure_duration{0};

    int i = 0;
    for (i = 0; (i < steps and t < T - 1e-8) and
                (type == "verify" or
                 helper.min(std::chrono::duration<double>(std::min(intermediate_duration, pressure_duration)).count())
                 < 0.55);
         ++i) {
      t += dt;
      dir_u.setTime(t);

      Dune::PDELab::interpolate(dir_u, gfs_u, dir_values_u);

      Dune::PDELab::copy_constrained_dofs(cc_u, dir_values_u, u_star);
      un_adapter.setContainer(u_n);
      intermediate_duration += likwid_timed_fixed("solver_intermediate",
                                                  [&]{ intermediate_solver.apply(u_star, true); });
      intermediate_results.push_back(intermediate_solver.ls_result());

      ustar_adapter.setContainer(u_star);
      pressure_duration += likwid_timed_fixed("solver_pressure",
                                              [&]{ pressure_solver.apply(p, true); });
      pressure_results.push_back(pressure_solver.ls_result());

      p_adapter.setContainer(p);
      projection_solver.apply(u_n);

      if (type == "verify" and (i == next_out or (i == steps - 2 or t + dt >= T - 1e-8))){
        next_out += dt_out;
        vtkSequenceWriter.write(t, Dune::VTK::appendedraw);
      }
    }
    std::cout << "TIME_LOOP " << i << std::endl;

    for(auto&& [results, name] : {std::make_pair(intermediate_results, "solver_intermediate"),
                                  std::make_pair(pressure_results, "solver_pressure")}){
      unsigned int it = 0;
      for(auto&& res : results)
        it += res.iterations;
      std::cout << name << std::endl;
      std::cout << "SOLVER_IT " << it << std::endl;
    }
  }
  LIKWID_MARKER_CLOSE;
}
