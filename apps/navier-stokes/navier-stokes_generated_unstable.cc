// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <dune/grid/uggrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/codegen/matrixfree.hh>
#include <dune/ch-benchmarks/lumping.hh>
#include <dune/ch-benchmarks/timings.hh>
#include <dune/ch-benchmarks/mpihelper.hh>
#include <dune/ch-benchmarks/nullstream.hh>

#ifdef KERNEL_TIMINGS
#define ENABLE_HP_TIMERS 1
#endif

#include "unstable_intermediate_velocity_ilvl-@ilvl@_@block@_driverblock.hh"
#include "unstable_pressure_correction_ilvl-@ilvl@_@block@_driverblock.hh"
#include "unstable_projection_ilvl-@ilvl@_@block@_driverblock.hh"

#include <likwid.h>

constexpr int dim = 2;
constexpr int k = @block@;

int main(int argc, char** argv)
{
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;
  const auto& helper = MPIHelper::instance(argc, argv);

  const auto type = std::string(argv[1]);
  const auto levels = static_cast<unsigned>(std::stoul(argv[2]));

  const auto T = 8;
  const auto dt = 1. / 1600.;

  const std::set<std::string> valid_types = {"solver", "verify", "operator", "assemble", "get_iterations"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using RF = double;

  using Grid = Dune::UGGrid<dim>;
  using GV = typename Grid::LeafGridView;

  std::unique_ptr<Grid> grid_p = Dune::GmshReader<Grid>::read("flow_around_cylinder.msh");
  grid_p->globalRefine(levels);
  auto gv = grid_p->leafGridView();

  using IntermediateVelocityDB = IntermediateVelocityDriverBlock<GV>;
  using PressureCorrectionDB = PressureCorrectionDriverBlock<GV>;
  using ProjectionDB = ProjectionDriverBlock<GV>;

  IntermediateVelocityDB intermediateVelocity_db(gv, {});
  PressureCorrectionDB pressureCorrection_db(gv, {});
  ProjectionDB projection_db(gv, {});

  using V_U = IntermediateVelocityDB::V_F1;
  using V_UC = ProjectionDB::V_F3;
  using V_P = PressureCorrectionDB::V_F2;

  auto gfs_u = intermediateVelocity_db.getGridFunctionsSpace_BCG1_2();
  auto gfs_p = pressureCorrection_db.getGridFunctionsSpace_BCG1();
  auto gfs_uc = projection_db.getGridFunctionsSpace_BCG1_2();

  auto go_intermediate = intermediateVelocity_db.getGridOperator_F1();
  auto go_pressure = pressureCorrection_db.getGridOperator_F2();
  auto go_projection = projection_db.getGridOperator_F3();

  auto u_star = intermediateVelocity_db.getCoefficient_F1();
  auto u_n = projection_db.getCoefficient_F3();
  auto p = pressureCorrection_db.getCoefficient_F2();

  intermediateVelocity_db.setCoefficientBCG1_2_1(gfs_uc, u_n);
  pressureCorrection_db.setCoefficientBCG1_2_2(gfs_u, u_star);
  projection_db.setCoefficientBCG1_2_2(gfs_u, u_star);
  projection_db.setCoefficientBCG1_4(gfs_p, p);

  // set up coarse coefficients for coarse grid correction
  intermediateVelocity_db.setCoefficientCG1_2_1(projection_db.cg1_gfs_0_pow2gfs_,
                                                 projection_db.getCoefficient_F3Coarse());
  pressureCorrection_db.setCoefficientCG1_2_2(intermediateVelocity_db.cg1_dirichlet_gfs_0_pow2gfs_,
                                               intermediateVelocity_db.getCoefficient_F1Coarse());

  if (type == "operator" or type == "get_iterations"){
    V_U x_u(*gfs_u, 1.), y_u(*gfs_u, 0.);
    V_P x_p(*gfs_p, 1.), y_p(*gfs_p, 0.);
    V_UC x_uc(*gfs_uc, 1.), y_uc(*gfs_uc, 0.);

    NullStream nullStream{};

    likwid_timed("operator_intermediate", helper,
                 [&]{ go_intermediate->jacobian_apply(x_u, y_u); },
                 [&]{ intermediateVelocity_db.lop_F1->dump_timers(nullStream, "KERNEL", true); });
    intermediateVelocity_db.lop_F1->dump_timers(std::cout, "KERNEL", true);
    likwid_timed("operator_pressure", helper,
                 [&]{ go_pressure->jacobian_apply(x_p, y_p);},
                 [&]{ pressureCorrection_db.lop_F2->dump_timers(nullStream, "KERNEL", true); });
    pressureCorrection_db.lop_F2->dump_timers(std::cout, "KERNEL", true);
    likwid_timed("operator_projection", helper,
                 [&]{ go_projection->residual(x_uc, y_uc); },
                 [&]{ projection_db.lop_F3->dump_timers(nullStream, "KERNEL", true); });
    projection_db.lop_F3->dump_timers(std::cout, "KERNEL", true);
  }
  else if (type == "solver" or type == "verify"){
    const auto steps = std::stoul(argv[3]);

    using GFS_U = IntermediateVelocityDB::BCG1_dirichlet_GFS_POW2GFS;
    using GFS_P = PressureCorrectionDB::BCG1_dirichlet_GFS;

    using ProjectionLS = LumpedSolver<ProjectionDB::GO_F3, ProjectionDB::V_F3>;
    ProjectionLS projection_solver(*go_projection, std::true_type{});

    V_U dir_values_ustar(*gfs_u, 0.);
    IntermediateVelocityDB::V_F1COARSE dir_values_ustar_coarse(*intermediateVelocity_db.cg1_dirichlet_gfs_0_pow2gfs_,
                                                               0.);

    Dune::VTKSequenceWriter<GV> vtkSequenceWriter(
        std::make_shared<Dune::SubsamplingVTKWriter<GV>>(gv, Dune::refinementIntervals(k)),"generated-verify");
    using DGF_U = Dune::PDELab::VectorDiscreteGridFunction<GFS_U, V_U>;
    DGF_U dgf_u(gfs_u, u_star);
    using DGF_P = Dune::PDELab::DiscreteGridFunction<GFS_P, V_P>;
    DGF_P dgf_p(gfs_p, p);
    vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF_U>>(dgf_u, "velocity"));
    vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF_P>>(dgf_p, "pressure"));
    if (type == "verify"){
      vtkSequenceWriter.write(0., Dune::VTK::appendedraw);
    }

    RF t = 0.;
    int dt_out = 10;
    int next_out = dt_out;

    std::vector<Dune::InverseOperatorResult> intermediate_results;
    std::vector<Dune::InverseOperatorResult> pressure_results;

    using namespace std::chrono_literals;
    std::chrono::steady_clock::duration intermediate_duration{0};
    std::chrono::steady_clock::duration pressure_duration{0};

    int i;
    for (i = 0; (i < steps and t < T - 1e-8) and
                (type == "verify" or
                 helper.min(std::chrono::duration<double>(std::min(intermediate_duration, pressure_duration)).count())
                 < 0.55);
         ++i) {
      t += dt;
      intermediateVelocity_db.getLocalOperator_F1()->setTime(t);

      Dune::PDELab::interpolate(*intermediateVelocity_db.interpolate_expression, *gfs_u, dir_values_ustar);
      Dune::PDELab::interpolate(*intermediateVelocity_db.interpolate_expression,
                                *intermediateVelocity_db.cg1_dirichlet_gfs_0_pow2gfs_,
                                dir_values_ustar_coarse);

      Dune::PDELab::copy_constrained_dofs(*intermediateVelocity_db.bcg1_dirichlet_gfs_0_pow2gfs__cc,
                                          dir_values_ustar, *intermediateVelocity_db.getCoefficient_F1());
      Dune::PDELab::copy_constrained_dofs(*intermediateVelocity_db.cg1_dirichlet_gfs_0_pow2gfs__cc,
                                          dir_values_ustar_coarse, *intermediateVelocity_db.getCoefficient_F1Coarse());

      intermediate_duration += likwid_timed_fixed("solver_intermediate", [&]{
        auto stats = solveMatrixFree(*go_intermediate, *u_star, *intermediateVelocity_db.pre, 0);
        intermediate_results.push_back(stats);
      });

      pressure_duration += likwid_timed_fixed("solver_pressure",  [&]{
        auto stats = solveMatrixFree(*go_pressure, *p, *pressureCorrection_db.pre, 0);
        pressure_results.push_back(stats);
      });

      projection_solver.apply(*u_n);

      if (type == "verify" and (i == next_out or (i == steps - 2 or t + dt >= T - 1e-8))){
        next_out += dt_out;
        vtkSequenceWriter.write(t, Dune::VTK::appendedraw);
      }
    }
    std::cout << "TIME_LOOP " << i << std::endl;

    for(auto&& [results, name] : {std::make_pair(intermediate_results, "intermediate_solver"),
                                  std::make_pair(pressure_results, "pressure_solver")}){
      int it = 0;
      for(auto&& res : results)
        it += res.iterations;
      std::cout << name << std::endl;
      std::cout << "SOLVER_IT " << it << std::endl;
    }
  }
  LIKWID_MARKER_CLOSE;
}
