SetFactory("OpenCASCADE");
Rectangle(1) = {0, 0, 0, 2.2, 0.41, 0};
Disk(2) = {0.2, 0.2, 0, 0.05, 0.05};
BooleanDifference(3) = { Surface{1}; Delete; }{ Surface{2}; Delete; };
Recombine Surface {3};

lc = 0.125;
Field[1] = Box;
Field[1].VIn = lc;
Field[1].VOut = 1;
Field[1].XMax = 0.75;
Field[1].XMin = 0.125;
Field[1].YMax = 0.275;
Field[1].YMin = 0.125;

Field[2] = Box;
Field[2].VIn = 2 * lc;
Field[2].VOut = 1;
Field[2].XMax = 0.75;
Field[2].XMin = 0.;
Field[2].YMax = 0.41;
Field[2].YMin = 0.;

Field[3] = Distance;
Field[3].EdgesList = {3};

Field[4] = Threshold;
Field[4].DistMax = 0.45;
Field[4].DistMin = 1.45;
Field[4].IField = 3;
Field[4].LcMax = 1;
Field[4].LcMin = 2 * lc;

Field[5] = Min;
Field[5].FieldsList = {1, 2, 4};
Background Field = 5;

Mesh.CharacteristicLengthExtendFromBoundary = 0;
Mesh.CharacteristicLengthFromPoints = 0;
Mesh.CharacteristicLengthFromCurvature = 0;
Mesh.Algorithm = 8;
Mesh.Format = 1;
Mesh.MshFileVersion = 2.2;
Mesh.RecombinationAlgorithm = 2;
Mesh.Smoothing = 10;
