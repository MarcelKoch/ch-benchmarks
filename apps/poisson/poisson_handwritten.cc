// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/diagonalmatrix.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/pdelab.hh>
#include "dune/ch-benchmarks/utility.hh"
#include "dune/ch-benchmarks/poisson/param.hh"
#include <dune/ch-benchmarks/timings.hh>
#include <dune/ch-benchmarks/mpihelper.hh>
#include <likwid.h>

constexpr int dim = DIMENSION;

template<typename GV, typename Param, typename RT>
class Driver {
public:
  using FEM = Dune::PDELab::QkLocalFiniteElementMap<GV, RT, RT, 1>;

  using CON = Dune::PDELab::ConformingDirichletConstraints;
  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, CON>;

  using CC = typename GFS::template ConstraintsContainer<RT>::Type;

  using LOP = Dune::PDELab::ConvectionDiffusionFEM<Param, FEM>;
  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT, CC, CC>;

  using LS = Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<GOP>;

  using V = Dune::PDELab::Backend::Vector<GFS, RT>;
  using SLP = Dune::PDELab::StationaryLinearProblemSolver<GOP, LS, V>;

  explicit Driver(const GV &gv, Param param_, int verbosity = 0)
      : fem(gv), gfs(gv, fem), cc(), param(std::move(param_)), lop(param),
        gop(gfs, cc, gfs, cc, lop, MB(Dune::power(3, static_cast<int>(GV::dimension)))),
        ls(5000, verbosity), slp(gop, ls, 1e-10, 1e-30, verbosity) {
    gfs.name("fesol");
    gfs.update();

    Dune::PDELab::constraints(Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&](const auto &is, const auto &x) {
                                return param.bctype(is, x) == Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
                              }),
                              gfs, cc);

    if (verbosity > 0) {
      std::cout << "gfs with " << gfs.size() << " dofs generated  "<< std::endl;
      std::cout << "cc with " << cc.size() << " dofs generated  "<< std::endl;
    }
  }

  FEM fem;
  GFS gfs;
  CC cc;
  Param param;
  LOP lop;
  GOP gop;
  LS ls;
  SLP slp;
};


int main(int argc, char **argv) {
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;

  using RF = double;
  const auto &helper = MPIHelper::instance(argc, argv);

  const auto type = std::string(argv[1]);
  const auto N = static_cast<unsigned>(std::stoul(argv[2]));

  const std::set<std::string> valid_types = {"solver", "verify", "operator", "get_iterations"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using Grid = Dune::YaspGrid<dim>;
  using GV = typename Grid::LeafGridView;
  auto grid_ptr = Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<RF, dim>(0.),
                                                                    Dune::FieldVector<RF, dim>(1.),
                                                                    filled_array<dim>(N));
  auto gv = grid_ptr->leafGridView();

  using Param = ParamRandom<Grid, RF>;
  Param param(N);

  using Driver = Driver<GV, Param, RF>;

  Driver driver(gv, param, 1);

  if (type == "operator" or type == "get_iterations"){
    typename Driver::GOP::Jacobian J(driver.gop);
    typename Driver::V x(driver.gfs, 1.), y(driver.gfs, 0.);
    driver.gop.jacobian(x, J);

    using Dune::PDELab::Backend::native;
    std::generate(begin(native(x)), end(native(x)), std::rand);

    likwid_timed(type, helper, [&]{ native(J).mv(native(x), native(y)); });
  }
  else if (type == "solver" || type == "verify") {
    typename Driver::V x(driver.gfs, 0.);

    int verbosity = 1;
    likwid_timed(type, helper,
                 [&]{
                   x = 0.;
                   Driver::LS ls(5000, verbosity);
                   Driver::SLP slp{driver.gop, ls, 1e-10, 1e-30, verbosity};
                   slp.apply(x);
                 },
                 [&]{ verbosity = 1; }, [&]{ verbosity = 0; });

    if (type == "verify") {
      Dune::VTKWriter<GV> vtkwriter(gv, Dune::VTK::conforming);
      Dune::PDELab::addSolutionToVTKWriter(vtkwriter, driver.gfs, x);
      vtkwriter.write("handwritten-verify-" + std::to_string(dim) + "d", Dune::VTK::appendedraw);
    }
  }
  LIKWID_MARKER_CLOSE;
}
