add_custom_target(poisson_handwritten)
add_custom_target(poisson_generated_2d)
add_custom_target(poisson_generated_3d)
add_custom_target(poisson_generated DEPENDS poisson_generated_2d poisson_generated_3d)
add_custom_target(poisson DEPENDS poisson_generated poisson_handwritten)

foreach(dim 2 3)
    set(target poisson_handwritten_${dim}d_ilvl-0)

    add_executable(${target} poisson_handwritten.cc)
    target_link_dune_default_libraries(${target})
    target_link_libraries(${target} likwid::likwid MPI::MPI_CXX)
    target_compile_definitions(${target} PRIVATE LIKWID_PERFMON=1 DIMENSION=${dim})

    add_dependencies(poisson_handwritten ${target})

    foreach(ilvl 0 3)
        foreach(block RANGE 8 64 8)
            set(target poisson_generated_${dim}d_ilvl-${ilvl}_block-${block})

            configure_file(poisson.ini ${target}.ini @ONLY)
            configure_file(poisson_generated.cc ${target}.cc @ONLY)

            dune_add_generated_executable(
                    UFLFILE poisson.ufl
                    INIFILE ${CMAKE_CURRENT_BINARY_DIR}/${target}.ini
                    SOURCE ${CMAKE_CURRENT_BINARY_DIR}/${target}.cc
                    TARGET ${target}
            )
            target_link_libraries(${target} likwid::likwid MPI::MPI_CXX)
            target_compile_definitions(${target} PRIVATE MAX_VECTOR_SIZE=512 DIMENSION=${dim})
            if (ilvl EQUAL 0)
                target_compile_definitions(${target} PRIVATE LIKWID_PERFMON=1)
            else()
                target_compile_definitions(${target} PRIVATE KERNEL_TIMINGS=1)
            endif ()

            add_dependencies(poisson_generated_${dim}d ${target})
        endforeach()
    endforeach()
endforeach()
