#include "config.h"
#include "dune/common/parallel/mpihelper.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/testtools/gridconstruction.hh"
#include "dune/common/parametertree.hh"
#include "dune/codegen/matrixfree.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include <dune/codegen/blockstructured/preconditioner/preconditioner.hh>
#include <dune/ch-benchmarks/timings.hh>
#include "dune/ch-benchmarks/utility.hh"
#include "dune/ch-benchmarks/poisson/param.hh"
#include "dune/ch-benchmarks/poisson/grid_transfers.h"
#include <dune/ch-benchmarks/nullstream.hh>
#include <dune/ch-benchmarks/mpihelper.hh>
#include <likwid.h>

#ifdef KERNEL_TIMINGS
#define ENABLE_HP_TIMERS 1
#endif

#include "poisson_generated_@dim@d_ilvl-@ilvl@_block-@block@_driverblock.hh"

constexpr int dim = @dim@;
constexpr int k = @block@;

template<typename V>
auto coefficient_map(const V& in, const unsigned N){
  using RF = double;
  using Grid = Dune::YaspGrid<dim>;
  auto grid_ptr = Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<RF, dim>(0.),
                                                                    Dune::FieldVector<RF, dim>(1.),
                                                                    filled_array<dim>(N * k));
  auto gv = grid_ptr->leafGridView();
  const auto& idxSet = gv.indexSet();

  using Key = Dune::FieldVector<RF, dim>;
  std::map<Key, double, less<Key>> values{};

  for (auto& e: Dune::elements(gv)){
    auto idx = idxSet.index(e);
    values[e.geometry().corner(0)] = in[idx];
  }

  return values;
}

template<typename Map, typename V, typename GV, typename GFS>
void fill_coefficient(const Map& map, V& coeff, const GV& gv, const GFS& gfs, const unsigned N){
  using LFS = Dune::PDELab::LocalFunctionSpace<GFS>;
  using LFSC = Dune::PDELab::LFSIndexCache<LFS>;
  LFS lfs(gfs);
  LFSC lfsc(lfs);

  typename V::template LocalView<LFSC> view{coeff};
  view.bind(lfsc);

  for (auto& e: Dune::elements(gv)){
    lfs.bind(e);
    lfsc.update();

    Dune::PDELab::LocalVector<double> local(lfs.size(), 0.);
    auto corner = e.geometry().corner(0);
    if constexpr (dim == 2) {
      for (int ej = 0; ej < k; ++ej)
        for (int ei = 0; ei < k; ++ei) {
          Dune::FieldVector<double, dim> offset{ei / double(k * N), ej / double(k * N)};
          local(lfs, ei + k * ej) = map.at(corner + offset);
        }
    } else {
      for (int el = 0; el < k; ++el)
        for (int ej = 0; ej < k; ++ej)
          for (int ei = 0; ei < k; ++ei) {
            Dune::FieldVector<double, dim> offset{ei / double(k * N), ej / double(k * N), el / double(k * N)};
            local(lfs, ei + k * ej + k * k * el) = map.at(corner + offset);
          }
    }
    view.write(local);
  }
}

int main(int argc, char** argv){
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;

  using RF = double;
  const auto& helper = MPIHelper::instance(argc, argv);

  const auto type = std::string(argv[1]);
  const auto N = static_cast<unsigned>(std::stoul(argv[2]));

  const std::set<std::string> valid_types = {"solver", "verify", "operator"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using Grid = Dune::YaspGrid<dim>;
  using GV = typename Grid::LeafGridView;
  auto grid_ptr = Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<RF, dim>(0.),
                                                                    Dune::FieldVector<RF, dim>(1.),
                                                                    filled_array<dim>(N));
  auto gv = grid_ptr->leafGridView();

  // Set up driver block...
  DriverBlock<GV> driverBlock(gv, Dune::ParameterTree{});

  using DB = DriverBlock<GV>;
  DB::BDG0_FEM coeff_fem{};
  using COEFF_GFS = DB::BDG0_GFS;
  auto coeff_gfs = std::make_shared<DB::BDG0_GFS>(gv, coeff_fem);
  auto coeff_cc = std::make_shared<Dune::PDELab::EmptyTransformation>();

  auto coeff = std::make_shared<DB::CoeffV_BDG0_1>(coeff_gfs);
  driverBlock.setCoefficientBDG0_1(coeff_gfs, coeff);

  ParamRandom<Grid, RF> param(N * k);
  const auto& rv = param.data();

  auto& c_native = Dune::PDELab::Backend::native(*coeff);
  const auto map = coefficient_map(rv, N);
  fill_coefficient(map, *coeff, gv, *coeff_gfs, N);

  auto go = driverBlock.getGridOperator_r();
  using GFS = DB::BCG1_dirichlet_GFS;
  auto gfs = driverBlock.getGridFunctionsSpace_BCG1();
  using V = DB::V_R;

  if(type == "operator") {
    V x(gfs, 0.), y(gfs, 0.);
    std::generate(begin(*x.storage()), end(*x.storage()), std::rand);

    likwid_timed(type, helper,
                 [&] { go->jacobian_apply(x, y); },
                 [&] {
                   NullStream nullStream{};
                   driverBlock.lop_r->dump_timers(nullStream, "KERNEL", true);
                 });
    driverBlock.lop_r->dump_timers(std::cout, "KERNEL", true);
  }
  else if (type == "solver" || type == "verify") {
    auto x = driverBlock.getCoefficient_r();
    auto pre = driverBlock.pre;

    const auto setup_start = std::chrono::system_clock::now();
    DB::DG0_FEM coeff_coarse_fem{};
    auto coeff_coarse_gfs = std::make_shared<DB::CoeffGFSDG0_1>(gv, coeff_coarse_fem);
    auto coeff_coarse = std::make_shared<DB::CoeffV_DG0_1>(*coeff_coarse_gfs, 0.);
    driverBlock.setCoefficientDG0_1(coeff_coarse_gfs, coeff_coarse);

    {
      SchurDecomposition coeff_decomp(*coeff_gfs, *coeff_cc);

      RestrictionDG0LocalOperator<k, dim> dg0_r_lop{};
      RestrictionOperator dg0_restriction(dg0_r_lop, *coeff_gfs, *coeff_cc, *coeff_coarse_gfs, *coeff_cc, coeff_decomp);

      dg0_restriction.apply(*coeff, *coeff_coarse);
    }

    auto point_diagonal_lop = driverBlock.point_diagonal_lop_r;
    point_diagonal_lop->setCoefficientBDG0_1(coeff_gfs, coeff);

    auto setup_time = std::chrono::duration<double>(std::chrono::system_clock::now() - setup_start).count();
    std::cout << "SETUP" << " " << setup_time << std::endl;

    int verbosity = 1;
    likwid_timed(type, helper, [&]{ *x = 0.; solveMatrixFree(*go, *x, *pre, verbosity); },
                 [&]{ verbosity = 1; }, [&]{ verbosity = 0; });

    if (type == "verify") {
      // Do visualization...
      using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
      VTKWriter vtkwriter(gv, Dune::RefinementIntervals{k});
      Dune::PDELab::addSolutionToVTKWriter(vtkwriter, *gfs, *x);
      vtkwriter.write("generated-verify", Dune::VTK::ascii);
    }
  }
  LIKWID_MARKER_CLOSE;
}

