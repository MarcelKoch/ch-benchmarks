// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/grid/yaspgrid.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>
#include <dune/pdelab/solver/newton.hh>

#include <dune/ch-benchmarks/hyperelasticity/param.hh>
#include <dune/ch-benchmarks/hyperelasticity/localoperator.hh>

#include <likwid.h>
#include <dune/ch-benchmarks/timings.hh>

constexpr int dim = 3;


struct NStepTerminate: public Dune::PDELab::TerminateInterface{
  explicit NStepTerminate(int n_): n(n_) {}

  bool terminate() override{
    return n <= it++;
  }

  void setParameters(const Dune::ParameterTree&) override {}

private:
  int n = 1;
  int it = 0;
};

int main(int argc, char** argv)
{
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;

  const auto type = std::string(argv[1]);
  const auto N_x = static_cast<unsigned>(std::stoul(argv[2]));
  const auto N_yz = N_x * 3 / 4;

  const std::set<std::string> valid_types = {"solver", "verify", "operator", "assemble", "get_iterations"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using RF = double;

  using Grid = Dune::YaspGrid<3>;
  using GV = typename Grid::LeafGridView;

  auto grid_p = Dune::StructuredGridFactory<Grid>::createCubeGrid({0., 0., 0.}, {1., 1., 1.},
                                                                  {N_x, N_yz, N_yz});
  auto gv = grid_p->leafGridView();

  constexpr RF young_modulus = 10.;
  constexpr RF nu = 0.3;
  constexpr RF lambda = young_modulus * nu / ((1 + nu) * (1 - 2 * nu));
  constexpr RF mu = young_modulus / (2 * (1 + nu));
  NeoHookMaterial<RF, dim> material(mu, lambda);
  //StVenantKirchoffMaterial<RF, dim> material(mu, lambda);
  HyperelasticityParameter param(material, RF(M_PI / 6.));

  using FEM = Dune::PDELab::QkLocalFiniteElementMap<GV, RF, RF, 1>;
  FEM fem(gv);

  using CON = Dune::PDELab::ConformingDirichletConstraints;
  using GFS_Q1 = Dune::PDELab::GridFunctionSpace<GV, FEM, CON>;
  GFS_Q1 gfs_q1(gv, fem);
  gfs_q1.name("Q1");

  using VB = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>;
  using GFS = Dune::PDELab::PowerGridFunctionSpace<GFS_Q1, dim, VB, Dune::PDELab::EntityBlockedOrderingTag>;
  GFS gfs(gfs_q1);
  gfs.name("displacement");
  gfs.update();

  using CC = GFS::template ConstraintsContainer<RF>::Type;
  CC cc{};
  Dune::PDELab::constraints(Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&](const auto& is, const auto& x){
                              return param.isDirichlet(is, x);
                            }),
                            gfs, cc);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  MB mb(dim * Dune::power(3, dim));

  using LOP = Hyperelasticity<decltype(param), FEM>;
  LOP lop(param);

  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RF, RF, RF, CC, CC>;
  GOP gop(gfs, cc, gfs, cc, lop, mb);

  std::cout << "DIM " << N_x << "x" << N_yz << "x" << N_yz << std::endl;
  std::cout << "gfs with " << gfs.size() << " dofs generated  "<< std::endl;
  std::cout << "cc with " << cc.size() << " dofs generated  "<< std::endl;

  using V = Dune::PDELab::Backend::Vector<GFS, RF>;
  if (type == "operator" or type == "assemble" or type == "get_iterations"){
    using Dune::PDELab::Backend::native;
    V x(gfs, 0.), y(gfs, 0.), z(gfs, 0.);
    std::generate(begin(*x.storage()), end(*x.storage()), std::rand);
    std::generate(begin(*z.storage()), end(*z.storage()), std::rand);

    using Jacobian = Dune::PDELab::Backend::Matrix<MB, V, V, RF>;
    Jacobian J(gop);

    auto assemble = [&](){ gop.jacobian(z, J); };
    auto apply = [&]() { native(J).mv(native(x), native(y)); };

    if (type == "get_iterations"){
      const auto it_assemble = guess_iterations(assemble);
      const auto it_apply = guess_iterations(apply);
      std::cout << it_apply << " " << it_assemble << std::endl;
    }
    else if (type == "operator"){
      assemble();

      const int it = std::stoi(argv[3]);

      LIKWID_MARKER_START(type.c_str());
      for (int i = 0; i < it; ++i) {
        apply();
      }
      LIKWID_MARKER_STOP(type.c_str());
      std::cout << "IT " << it << std::endl;
    } else if (type == "assemble"){
      const int it = std::stoi(argv[3]);

      LIKWID_MARKER_START(type.c_str());
      for (int i = 0; i < it; ++i) {
        assemble();
      }
      LIKWID_MARKER_STOP(type.c_str());
      std::cout << "IT " << it << std::endl;
    }
  } else if(type == "solver" or type == "verify") {
    using LS = Dune::PDELab::ISTLBackend_SEQ_BCGS_AMG_SSOR<GOP>;
    LS ls(5000, 1);

    using Solver = Dune::PDELab::NewtonMethod<GOP, LS>;
    Solver solver(gop, ls);

    V x(gfs, 0.);
    Dune::PDELab::interpolate(Dune::PDELab::makeGridFunctionFromCallable(gv, [&](const auto &x) { return param.g(x); }),
                              gfs, x);
    //Dune::PDELab::set_nonconstrained_dofs(cc, 0., x);

    solver.setVerbosityLevel(3);
    LIKWID_MARKER_START(type.c_str());
    solver.apply(x);
    LIKWID_MARKER_STOP(type.c_str());

    if (type == "verify") {
      using DGF = Dune::PDELab::VectorDiscreteGridFunction<GFS, V>;
      DGF dgf(gfs, x);
      Dune::VTKWriter<GV> vtkwriter(gv, Dune::VTK::conforming);
      typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;
      vtkwriter.addVertexData(std::make_shared<VTKF>(dgf, "fesol"));
      vtkwriter.write("handwritten-verify", Dune::VTK::appendedraw);
    }
  }
  LIKWID_MARKER_CLOSE;
}
