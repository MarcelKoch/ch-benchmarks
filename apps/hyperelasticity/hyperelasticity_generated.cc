// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/gridoperator/blockstructured.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>
#include <dune/pdelab/solver/newton.hh>
#include <dune/ch-benchmarks/timings.hh>
#include <dune/ch-benchmarks/linearsolver.hh>

#include "hyperelasticity_generated_driverblock.hh"

#include <likwid.h>

constexpr int k = 4;
constexpr int dim = 3;


int main(int argc, char** argv)
{
  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;

  const auto type = std::string(argv[1]);
  const auto N_x = static_cast<unsigned>(std::stoul(argv[2]));
  const auto N_yz = N_x * 3 / 4;

  const std::set<std::string> valid_types = {"solver", "verify", "operator", "assemble", "get_iterations"};
  if (valid_types.find(type) == end(valid_types))
    throw std::runtime_error("invalid type");

  using RF = double;

  using Grid = Dune::YaspGrid<3>;
  using GV = typename Grid::LeafGridView;

  auto grid_p = Dune::StructuredGridFactory<Grid>::createCubeGrid({0., 0., 0.}, {1., 1., 1.},
                                                                  {N_x, N_yz, N_yz});
  auto gv = grid_p->leafGridView();

  std::cout << "DIM " << N_x * k << "x" << N_yz * k << "x" << N_yz * k << std::endl;

  using DB = DriverBlock<GV>;
  DB driver(gv, {});

  using GOP = DB::GO_r;
  using GFS = DB::BCG1_dirichlet_GFS_POW3GFS;
  using V = DB::V_R;
  auto go = driver.getGridOperator_r();
  auto gfs = driver.getGridFunctionsSpace_BCG1_3();

  if (type == "operator" or type == "get_iterations"){
    V x(*gfs, 0.), y(*gfs, 0.), z(*gfs, 0.);
    std::generate(begin(*x.storage()), end(*x.storage()), std::rand);
    std::generate(begin(*z.storage()), end(*z.storage()), std::rand);

    auto f = [&](){ go->jacobian_apply(x, z, y);};

    if (type == "get_iterations"){
      std::cout << guess_iterations(f) << std::endl;
    } else {
      const int it = std::stoi(argv[3]);

      LIKWID_MARKER_START(type.c_str());
      for (int i = 0; i < it; ++i) {
        f();
      }
      LIKWID_MARKER_STOP(type.c_str());
      std::cout << "IT " << it << std::endl;
    }
  }
  else if(type == "solver" or type == "verify") {
//    using PreFine = DB::Jacobi;
//    using PreCoarse = DB::CGC;
//    auto preFine = driver.pre_fine;
//    auto preCoarse = driver.pre_coarse;
//
//    using LS = MatrixFreeTwoLevelBiCGStab<GOP, PreFine, PreCoarse>;
//    LS ls(*go, preFine, preCoarse, 5000, 1);
    using LS = Dune::PDELab::ISTLBackend_SEQ_MatrixFree_BCGS_Richardson<GOP>;
    LS ls(*go, 5000, 1);

    using Solver = Dune::PDELab::NewtonMethod<GOP, LS, true>;
    Solver solver(*go, ls);

    auto& x = *driver.getCoefficient_r();

    //solver.setTerminate(std::make_shared<NStepTerminate>(1));
    solver.setVerbosityLevel(3);
    LIKWID_MARKER_START(type.c_str());
    try {
      solver.apply(x);
    } catch (Dune::Exception& e) {
      std::cout << e.what() << std::endl;
    }
    LIKWID_MARKER_STOP(type.c_str());

    if (type == "verify") {
//      {
//        auto cgc = driver.pre_coarse;
//        cgc->setLinearizationPoint(x);
//
//        cgc->pre(x, x);
//      }
//      {
//        typename GOP::Jacobian J(*go);
//        go->jacobian(x, J);
//
//        V y(*gfs, 0.), x(*gfs, 1.);
//        using Dune::PDELab::Backend::native;
//        native(J).mv(native(x), native(y));
//
//        std::cout << x.two_norm() << std::endl;
//        std::cout << y.two_norm() << std::endl;
//
//        using C_GFS = DB::CG1_dirichlet_GFS_POW3GFS;
//        using C_V = Dune::PDELab::Backend::Vector<C_GFS, RF>;
//        auto c_gfs = driver.cg1_dirichlet_gfs_0_pow3gfs_;
//
//        auto restriction = driver.restriction;
//        C_V c_x(*c_gfs, 0.), c_y(*c_gfs, 0.);
//        restriction->apply(x, c_x);
//
//        auto cgc = driver.pre_coarse;
//        cgc->setLinearizationPoint(x);
//
//        using C_GOP = typename DB::GO_rCoarse;
//        auto c_go = driver.getGridOperator_rCoarse();
//        typename C_GOP::Jacobian c_J(*c_go);
//        c_go->jacobian(cgc->u, c_J);
//
//        native(c_J).mv(native(c_x), native(c_y));
//
//        std::cout << c_x.two_norm() << std::endl;
//        std::cout << c_y.two_norm() << std::endl;
//
//
//        using DGF = Dune::PDELab::VectorDiscreteGridFunction<C_GFS, C_V>;
//        DGF dgf(*c_gfs, c_x);
//        Dune::VTKWriter<GV> vtkwriter(gv);
//        typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;
//        vtkwriter.addVertexData(std::make_shared<VTKF>(dgf, "fesol"));
//        vtkwriter.write("generated-verify-coarse-2", Dune::VTK::appendedraw);
//      }
//      {
//        using C_GFS = DB::CG1_dirichlet_GFS_POW3GFS;
//        using C_V = Dune::PDELab::Backend::Vector<C_GFS, RF>;
//        auto c_gfs = driver.cg1_dirichlet_gfs_0_pow3gfs_;
//
//        auto cgc = driver.pre_coarse;
//        cgc->setLinearizationPoint(x);
//
//        using DGF = Dune::PDELab::VectorDiscreteGridFunction<C_GFS, C_V>;
//        DGF dgf(*c_gfs, cgc->u);
//        Dune::VTKWriter<GV> vtkwriter(gv);
//        typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;
//        vtkwriter.addVertexData(std::make_shared<VTKF>(dgf, "fesol"));
//        vtkwriter.write("generated-verify-coarse", Dune::VTK::appendedraw);
//      }

      using DGF = Dune::PDELab::VectorDiscreteGridFunction<GFS, V>;
      DGF dgf(*gfs, x);
      Dune::SubsamplingVTKWriter<GV> vtkwriter(gv, Dune::refinementIntervals(k));
      typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;
      vtkwriter.addVertexData(std::make_shared<VTKF>(dgf, "fesol"));
      vtkwriter.write("generated-verify", Dune::VTK::appendedraw);
    }
  }
  LIKWID_MARKER_CLOSE;
}
