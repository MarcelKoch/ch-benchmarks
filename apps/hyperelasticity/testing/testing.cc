// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#include "hyperelasticity_generated_interpolation_CG1_3_operator_file.hh"
#include "hyperelasticity_generated_rOperator_file.hh"
#include "hyperelasticity_generated_rCoarseOperator_file.hh"
#include "hyperelasticity_generated_restriction_BCG1_3_operator_file.hh"

#endif
#include <iostream>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/gridoperator/blockstructured.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/powergridfunctionspace.hh>
#include <dune/pdelab/solver/newton.hh>
#include <dune/ch-benchmarks/timings.hh>
#include <dune/ch-benchmarks/linearsolver.hh>

#include <dune/codegen/blockstructured/blockstructuredqkfem.hh>
#include <dune/codegen/blockstructured/preconditioner/schur_complement/decomposition.hh>
#include <dune/codegen/blockstructured/preconditioner/two_level/interpolation.hh>
#include <dune/codegen/blockstructured/preconditioner/two_level/restriction.hh>

#include <likwid.h>
#include <dune/codegen/blockstructured/blockstructuredqkfem.hh>

constexpr int k = 4;
constexpr int dim = 3;

using RF = double;

struct NStepTerminate: public Dune::PDELab::TerminateInterface{
  explicit NStepTerminate(int n_): n(n_) {}

  bool terminate() override{
    return n <= it++;
  }

  void setParameters(const Dune::ParameterTree&) override {}

private:
  int n = 1;
  int it = 0;
};

template<typename GV, typename FEM>
auto construct_gfs(const GV& gv, const FEM& fem){
  using GFS_ = Dune::PDELab::GridFunctionSpace<GV, FEM>;
  auto gfs_ = std::make_shared<GFS_>(gv, fem);

  std::array<decltype(gfs_), dim> storage{};
  std::fill_n(storage.begin(), dim, gfs_);

  using GFS = Dune::PDELab::PowerGridFunctionSpace<GFS_, dim, Dune::PDELab::ISTL::VectorBackend<>>;
  auto gfs = std::make_shared<GFS>(storage);
  gfs->update();
  return gfs;
}

template<typename GFS, typename LOP, typename V>
auto construct_jacobian(std::shared_ptr<GFS> gfs, LOP& lop, const V& x){
  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RF, RF, RF>;
  GOP gop(*gfs, *gfs, lop, MB(27 * 3));

  auto J = std::make_shared<typename GOP::Jacobian>(gop);
  gop.jacobian(x, *J);
  return J;
}

template<typename GFS, typename LOP>
auto construct_gridoperator(std::shared_ptr<GFS> gfs, LOP& lop){
  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RF, RF, RF>;
  return std::make_shared<GOP>(*gfs, *gfs, lop, MB(27 * 3));
}

int main(int argc, char** argv)
{
  const auto N_x = static_cast<unsigned>(std::stoul(argv[1]));
  const auto N_yz = N_x * 3 / 4;

  using Grid = Dune::YaspGrid<3>;
  using GV = typename Grid::LeafGridView;

  auto grid_p = Dune::StructuredGridFactory<Grid>::createCubeGrid({0., 0., 0.}, {1., 1., 1.},
                                                                  {N_x, N_yz, N_yz});
  auto gv = grid_p->leafGridView();

  std::cout << "DIM " << N_x * k << "x" << N_yz * k << "x" << N_yz * k << std::endl;

  using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, RF, RF, k>;
  FEM fem(gv);
  auto gfs = construct_gfs(gv, fem);

  using C_FEM = Dune::PDELab::QkLocalFiniteElementMap<GV, RF, RF, 1>;
  C_FEM c_fem(gv);
  auto c_gfs = construct_gfs(gv, c_fem);

  using CC = Dune::PDELab::EmptyTransformation;

  SchurDecomposition decomp(*gfs, CC{});

  InterpolationCG1_3LocalOperator i_lop(*c_gfs, *gfs, {});
  InterpolationOperator interpolation(i_lop, *gfs, CC{}, *c_gfs, CC{}, decomp);

  RestrictionBCG1_3LocalOperator r_lop(*gfs, *c_gfs, {});
  RestrictionOperator restriction(r_lop, *gfs, CC{}, *c_gfs, CC{}, decomp);

  using V = Dune::PDELab::Backend::Vector<decltype(gfs)::element_type, RF>;
  using C_V = Dune::PDELab::Backend::Vector<decltype(c_gfs)::element_type, RF>;

  using Dune::PDELab::Backend::native;

  {
    V x(*gfs, 0.);
    C_V c_x(*c_gfs, 1.);

    interpolation.apply(c_x, x);

    using DGF = Dune::PDELab::VectorDiscreteGridFunction<decltype(gfs)::element_type, V>;
    DGF dgf(*gfs, x);
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv, Dune::refinementIntervals(k));
    typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;
    vtkwriter.addVertexData(std::make_shared<VTKF>(dgf, "fesol"));
    vtkwriter.write("generated-verify", Dune::VTK::appendedraw);
  }
  {
    V z(*gfs, 0.);
    rOperator lop(*gfs, *gfs, {});   ;
    auto J = construct_jacobian(gfs, lop, z);
    auto go = construct_gridoperator(gfs, lop);

    rCoarseOperator c_lop(*c_gfs, *c_gfs, {});
    auto c_J = construct_jacobian(c_gfs, c_lop, C_V(*c_gfs, 0.));
    auto c_go = construct_gridoperator(c_gfs, c_lop);

    C_V c_x(*c_gfs, 0.), c_z(*c_gfs, 0.);
    C_V c_y1(*c_gfs, 0.), c_y2(*c_gfs, 0.);

    std::generate(begin(native(c_x)), end(native(c_x)), []{ return 1. + (std::rand() / (2. * RAND_MAX));});

    native(*c_J).mv(native(c_x), native(c_y1));

    {
      V x(*gfs, 0.), y1(*gfs, 0.), y2(*gfs, 0.);
      std::generate(begin(native(x)), end(native(x)), []{ return 1. + (std::rand() / (2. * RAND_MAX));});

      go->jacobian_apply(z, x, y1);
      native(*J).mv(native(x), native(y2));
      std::cout << y1.two_norm() << std::endl;
      std::cout << y2.two_norm() << std::endl;
      y2 -= y1;
      std::cout << y2.two_norm() / y1.two_norm() << std::endl;
    }

    V x(*gfs, 0.), y(*gfs, 0.);
    interpolation.apply(c_x, x);
    std::cout << x.two_norm() << std::endl;
    native(*J).mv(native(x), native(y));
    std::cout << y.two_norm() << std::endl;
    restriction.apply(y, c_y2);

    C_V r(c_y1);
    r -= c_y2;
    std::cout << c_y1.two_norm() << std::endl;
    std::cout << c_y2.two_norm() << std::endl;
    std::cout << r.two_norm() / (c_y1.two_norm() + c_y2.two_norm()) << std::endl;
  }
}
