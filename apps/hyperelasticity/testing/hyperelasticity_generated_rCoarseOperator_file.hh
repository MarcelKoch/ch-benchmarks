#ifndef HYPERELASTICITY_GENERATED_RCOARSEOPERATOR_FILE_HH
#define HYPERELASTICITY_GENERATED_RCOARSEOPERATOR_FILE_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/codegen/localbasiscache.hh"
#include "dune/geometry/referenceelements.hh"
#include "dune/typetree/childextraction.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
using std::abs;


template<typename GFSU, typename GFSV>
class rCoarseOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::FullBoundaryPattern
{  

  public:
  rCoarseOperator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams) :
      _iniParams(iniParams)
  {
    fillQuadraturePointsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 2, qp_dim2_order2);
    fillQuadratureWeightsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 2, qw_dim2_order2);
    fillQuadraturePointsCache(gfsu.gridView().template begin<0>()->geometry(), 2, qp_dim3_order2);
    jit = gfsu.gridView().template begin<0>()->geometry().jacobianInverseTransposed(Dune::FieldVector<double, 3>());
    fillQuadratureWeightsCache(gfsu.gridView().template begin<0>()->geometry(), 2, qw_dim3_order2);
    detjac = gfsu.gridView().template begin<0>()->geometry().integrationElement(Dune::FieldVector<double, 3>());
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = false };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Vector> qp_dim2_order2;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Field> qw_dim2_order2;
  

  public:
  using GFSV_0 = Dune::TypeTree::Child<GFSV,0>;
  

  public:
  using Q1_LocalBasis = typename GFSV_0::Traits::FiniteElementMap::Traits::FiniteElementType::Traits::LocalBasisType;
  

  public:
  LocalBasisCacheWithoutReferences<Q1_LocalBasis> cache_CG1;
  

  public:
  using GFSV_1 = Dune::TypeTree::Child<GFSV,1>;
  

  public:
  using GFSV_2 = Dune::TypeTree::Child<GFSV,2>;
  

  public:
  enum { doPatternBoundary = true };
  

  public:
  enum { doAlphaBoundary = true };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 3>::Vector> qp_dim3_order2;
  

  public:
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::JacobianInverseTransposed jit;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 3>::Field> qw_dim3_order2;
  

  public:
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::ctype detjac;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  template<typename LFSV, typename LFSU, typename IG, typename J, typename X>
  void jacobian_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, J& jac_s_s) const
  {
  }
  

  public:
  template<typename R, typename LFSV, typename LFSU, typename IG, typename X>
  void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const
  {
    auto is_geo = ig.geometry();
    const auto quadrature_rule = quadratureRule(is_geo, 2);
    auto quadrature_size = quadrature_rule.size();
    auto refElement = referenceElement(is_geo);
    auto localcenter = refElement.position(0,0);
    auto fdetjac = is_geo.integrationElement(localcenter);
    using namespace Dune::Indices;
    auto lfsv_s_0 = child(lfsv_s, _0);
    auto lfsv_s_0_size = lfsv_s_0.size();
    auto geo_in_inside = ig.geometryInInside();
    auto lfsv_s_1 = child(lfsv_s, _1);
    auto lfsv_s_1_size = lfsv_s_1.size();
    auto lfsv_s_2 = child(lfsv_s, _2);
    auto lfsv_s_2_size = lfsv_s_2.size();
    typename LocalBasisCacheWithoutReferences<Q1_LocalBasis>::FunctionReturnType phi_CG1_s;
    Dune::FieldVector<double, 3> qp_dim2_order2_global(0.0);
    Dune::FieldVector<double, 3> qp_dim2_order2_in_inside(0.0);
    double t;
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      qp_dim2_order2_in_inside = geo_in_inside.global(qp_dim2_order2[q]);
      phi_CG1_s = cache_CG1.evaluateFunction(qp_dim2_order2_in_inside, lfsv_s_0.finiteElement().localBasis());
      qp_dim2_order2_global = is_geo.global(qp_dim2_order2[q]);
      if ((abs(qp_dim2_order2_global[0]) < 1e-10 || abs(-1.0 + qp_dim2_order2_global[0]) < 1e-10 ? 1.0 : 0.0) == 0.0)
      {
        t = -0.1 * fdetjac * qw_dim2_order2[q];
        for (int lfsv_s_0_0_index = 0; lfsv_s_0_0_index <= -1 + lfsv_s_0_size; ++lfsv_s_0_0_index)
          r_s.accumulate(lfsv_s_0, lfsv_s_0_0_index, t * (phi_CG1_s[lfsv_s_0_0_index])[0]);
      }
    }
  }
  

  public:
  template<typename R, typename LFSV, typename EG, typename LFSU, typename X>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    using namespace Dune::Indices;
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_size = lfsv_0.size();
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 2);
    auto quadrature_size = quadrature_rule.size();
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    auto lfsv_2 = child(lfsv, _2);
    auto lfsv_2_size = lfsv_2.size();
    double acc_lfsv_0_0_trialgrad_index;
    double acc_lfsv_0_0_trialgrad_index_0;
    double acc_lfsv_0_0_trialgrad_index_1;
    double gradu_0[3];
    double gradu_1[3];
    double gradu_2[3];
    typename LocalBasisCacheWithoutReferences<Q1_LocalBasis>::JacobianReturnType js_CG1;
    typename LocalBasisCacheWithoutReferences<Q1_LocalBasis>::FunctionReturnType phi_CG1;
    double t;
    double t_0;
    double t_1;
    double t_10;
    double t_11;
    double t_12;
    double t_13;
    double t_14;
    double t_15;
    double t_16;
    double t_17;
    double t_18;
    double t_19;
    double t_2;
    double t_20;
    double t_21;
    double t_22;
    double t_23;
    double t_24;
    double t_25;
    double t_26;
    double t_3;
    double t_4;
    double t_5;
    double t_6;
    double t_7;
    double t_8;
    double t_9;
  
    t_0 = -1.0 * (jit[2])[2];
    t_9 = (jit[2])[2] * (jit[0])[0];
    t_10 = -1.0 * (jit[2])[2] * (jit[1])[1];
    t_11 = -1.0 * (jit[0])[0];
    t_12 = (jit[0])[0] * (jit[1])[1];
    t_4 = 3.846153846153846 * (jit[0])[0];
    t_1 = (jit[1])[1] * (jit[2])[2];
    t_5 = -1.0 * (jit[1])[1];
    t_6 = (jit[2])[2] * (jit[1])[1];
    t_2 = (jit[1])[1] * (jit[0])[0];
    t_7 = 3.846153846153846 * (jit[2])[2];
    t_8 = 3.846153846153846 * (jit[1])[1];
    t = -1.0 * (jit[1])[1] * (jit[0])[0];
    t_3 = -1.0 * (jit[0])[0] * (jit[2])[2];
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      js_CG1 = cache_CG1.evaluateJacobian(qp_dim3_order2[q], lfsv_0.finiteElement().localBasis());
      phi_CG1 = cache_CG1.evaluateFunction(qp_dim3_order2[q], lfsv_0.finiteElement().localBasis());
      for (int idim0 = 0; idim0 <= 2; ++idim0)
      {
        acc_lfsv_0_0_trialgrad_index_1 = 0.0;
        acc_lfsv_0_0_trialgrad_index_0 = 0.0;
        acc_lfsv_0_0_trialgrad_index = 0.0;
        for (int lfsv_0_0_trialgrad_index = 0; lfsv_0_0_trialgrad_index <= -1 + lfsv_0_size; ++lfsv_0_0_trialgrad_index)
        {
          acc_lfsv_0_0_trialgrad_index_1 = acc_lfsv_0_0_trialgrad_index_1 + x(lfsv_2, lfsv_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_trialgrad_index])[0])[idim0];
          acc_lfsv_0_0_trialgrad_index_0 = acc_lfsv_0_0_trialgrad_index_0 + x(lfsv_1, lfsv_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_trialgrad_index])[0])[idim0];
          acc_lfsv_0_0_trialgrad_index = acc_lfsv_0_0_trialgrad_index + x(lfsv_0, lfsv_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_trialgrad_index])[0])[idim0];
        }
        gradu_2[idim0] = acc_lfsv_0_0_trialgrad_index_1;
        gradu_1[idim0] = acc_lfsv_0_0_trialgrad_index_0;
        gradu_0[idim0] = acc_lfsv_0_0_trialgrad_index;
      }
      t_19 = (1.0 + (jit[1])[1] * gradu_1[1]) * (1.0 + (jit[2])[2] * gradu_2[2]) + t_10 * gradu_1[2] * gradu_2[1];
      t_20 = t_12 * gradu_1[0] * gradu_2[1] + t_11 * (1.0 + (jit[1])[1] * gradu_1[1]) * gradu_2[0];
      t_22 = (jit[2])[2] * t_20 * gradu_0[2] + (1.0 + (jit[0])[0] * gradu_0[0]) * t_19 + (jit[1])[1] * (t_9 * gradu_1[2] * gradu_2[0] + t_11 * (1.0 + (jit[2])[2] * gradu_2[2]) * gradu_1[0]) * gradu_0[1];
      t_25 = 1.0 / t_22;
      t_26 = 3.846153846153846 + -5.769230769230769 * log(t_22);
      t_13 = -1.0 * t_26 * (t_1 * gradu_0[1] * gradu_1[2] + t_0 * (1.0 + (jit[1])[1] * gradu_1[1]) * gradu_0[2]) * t_25 + t_4 * gradu_2[0];
      t_14 = -1.0 * t_26 * (t_6 * gradu_0[2] * gradu_2[1] + t_5 * (1.0 + (jit[2])[2] * gradu_2[2]) * gradu_0[1]) * t_25 + t_4 * gradu_1[0];
      t_15 = -1.0 * t_26 * (t_2 * gradu_0[1] * gradu_2[0] + t_5 * (1.0 + (jit[0])[0] * gradu_0[0]) * gradu_2[1]) * t_25 + t_7 * gradu_1[2];
      t_16 = -1.0 * t_26 * t_20 * t_25 + t_7 * gradu_0[2];
      t_17 = -1.0 * t_26 * (t_9 * gradu_1[2] * gradu_2[0] + t_11 * gradu_1[0] * (1.0 + (jit[2])[2] * gradu_2[2])) * t_25 + t_8 * gradu_0[1];
      t_18 = -1.0 * t_26 * (t_9 * gradu_0[2] * gradu_1[0] + t_0 * (1.0 + (jit[0])[0] * gradu_0[0]) * gradu_1[2]) * t_25 + t_8 * gradu_2[1];
      t_21 = -1.0 * t_26 * ((1.0 + (jit[0])[0] * gradu_0[0]) * (1.0 + (jit[1])[1] * gradu_1[1]) + t * gradu_0[1] * gradu_1[0]) * t_25 + 3.846153846153846 * (1.0 + (jit[2])[2] * gradu_2[2]);
      t_23 = -1.0 * t_26 * t_19 * t_25 + 3.846153846153846 * (1.0 + (jit[0])[0] * gradu_0[0]);
      t_24 = -1.0 * t_26 * ((1.0 + (jit[0])[0] * gradu_0[0]) * (1.0 + (jit[2])[2] * gradu_2[2]) + t_3 * gradu_2[0] * gradu_0[2]) * t_25 + 3.846153846153846 * (1.0 + (jit[1])[1] * gradu_1[1]);
      for (int lfsv_0_0_index = 0; lfsv_0_0_index <= -1 + lfsv_0_size; ++lfsv_0_0_index)
      {
        r.accumulate(lfsv_2, lfsv_0_0_index, (t_13 * (jit[0])[0] * ((js_CG1[lfsv_0_0_index])[0])[0] + t_18 * (jit[1])[1] * ((js_CG1[lfsv_0_0_index])[0])[1] + t_21 * (jit[2])[2] * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
        r.accumulate(lfsv_1, lfsv_0_0_index, (0.5 * (phi_CG1[lfsv_0_0_index])[0] + t_14 * (jit[0])[0] * ((js_CG1[lfsv_0_0_index])[0])[0] + t_24 * (jit[1])[1] * ((js_CG1[lfsv_0_0_index])[0])[1] + t_15 * (jit[2])[2] * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
        r.accumulate(lfsv_0, lfsv_0_0_index, (t_23 * (jit[0])[0] * ((js_CG1[lfsv_0_0_index])[0])[0] + t_17 * (jit[1])[1] * ((js_CG1[lfsv_0_0_index])[0])[1] + t_16 * (jit[2])[2] * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
      }
    }
  }
  

  public:
  template<typename LFSV, typename EG, typename LFSU, typename J, typename X>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 2);
    auto quadrature_size = quadrature_rule.size();
    using namespace Dune::Indices;
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_size = lfsv_0.size();
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    auto lfsv_2 = child(lfsv, _2);
    auto lfsv_2_size = lfsv_2.size();
    double acc_lfsv_0_0_trialgrad_index;
    double acc_lfsv_0_0_trialgrad_index_0;
    double acc_lfsv_0_0_trialgrad_index_1;
    double gradu_0[3];
    double gradu_1[3];
    double gradu_2[3];
    typename LocalBasisCacheWithoutReferences<Q1_LocalBasis>::JacobianReturnType js_CG1;
    double t;
    double t_0;
    double t_1;
    double t_10;
    double t_100;
    double t_101;
    double t_102;
    double t_103;
    double t_104;
    double t_105;
    double t_106;
    double t_107;
    double t_108;
    double t_109;
    double t_11;
    double t_110;
    double t_111;
    double t_112;
    double t_113;
    double t_114;
    double t_115;
    double t_116;
    double t_117;
    double t_118;
    double t_119;
    double t_12;
    double t_120;
    double t_121;
    double t_13;
    double t_14;
    double t_15;
    double t_16;
    double t_17;
    double t_18;
    double t_19;
    double t_2;
    double t_20;
    double t_21;
    double t_22;
    double t_23;
    double t_24;
    double t_25;
    double t_26;
    double t_27;
    double t_28;
    double t_29;
    double t_3;
    double t_30;
    double t_31;
    double t_32;
    double t_33;
    double t_34;
    double t_35;
    double t_36;
    double t_37;
    double t_38;
    double t_39;
    double t_4;
    double t_40;
    double t_41;
    double t_42;
    double t_43;
    double t_44;
    double t_45;
    double t_46;
    double t_47;
    double t_48;
    double t_49;
    double t_5;
    double t_50;
    double t_51;
    double t_52;
    double t_53;
    double t_54;
    double t_55;
    double t_56;
    double t_57;
    double t_58;
    double t_59;
    double t_6;
    double t_60;
    double t_61;
    double t_62;
    double t_63;
    double t_64;
    double t_65;
    double t_66;
    double t_67;
    double t_68;
    double t_69;
    double t_7;
    double t_70;
    double t_71;
    double t_72;
    double t_73;
    double t_74;
    double t_75;
    double t_76;
    double t_77;
    double t_78;
    double t_79;
    double t_8;
    double t_80;
    double t_81;
    double t_82;
    double t_83;
    double t_84;
    double t_85;
    double t_86;
    double t_87;
    double t_88;
    double t_89;
    double t_9;
    double t_90;
    double t_91;
    double t_92;
    double t_93;
    double t_94;
    double t_95;
    double t_96;
    double t_97;
    double t_98;
    double t_99;
  
    t_24 = -1.0 * (jit[2])[2];
    t_51 = -1.0 * (jit[2])[2] * (jit[1])[1];
    t_48 = (jit[2])[2] * (jit[0])[0];
    t_57 = -1.0 * (jit[0])[0];
    t_66 = (jit[0])[0] * (jit[1])[1];
    t_42 = -1.0 * (jit[1])[1] * (jit[0])[0];
    t_2 = -1.0 * (jit[0])[0] * (jit[1])[1];
    t_33 = -1.0 * (jit[1])[1];
    t_21 = (jit[1])[1] * (jit[0])[0];
    t_18 = -1.0 * (jit[1])[1] * (jit[2])[2];
    t_15 = (jit[0])[0] * (jit[2])[2];
    t_13 = -1.0 * (jit[2])[2] * (jit[0])[0];
    t_8 = -1.0 * (jit[0])[0] * (jit[2])[2];
    t = (jit[1])[1] * (jit[2])[2];
    t_5 = (jit[2])[2] * (jit[1])[1];
    t_45 = 3.846153846153846 * (jit[2])[2];
    t_46 = 3.846153846153846 * (jit[1])[1];
    t_47 = 3.846153846153846 * (jit[0])[0];
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      js_CG1 = cache_CG1.evaluateJacobian(qp_dim3_order2[q], lfsv_0.finiteElement().localBasis());
      for (int idim0 = 0; idim0 <= 2; ++idim0)
      {
        acc_lfsv_0_0_trialgrad_index_1 = 0.0;
        acc_lfsv_0_0_trialgrad_index_0 = 0.0;
        acc_lfsv_0_0_trialgrad_index = 0.0;
        for (int lfsv_0_0_trialgrad_index = 0; lfsv_0_0_trialgrad_index <= -1 + lfsv_0_size; ++lfsv_0_0_trialgrad_index)
        {
          acc_lfsv_0_0_trialgrad_index_1 = acc_lfsv_0_0_trialgrad_index_1 + x(lfsv_2, lfsv_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_trialgrad_index])[0])[idim0];
          acc_lfsv_0_0_trialgrad_index_0 = acc_lfsv_0_0_trialgrad_index_0 + x(lfsv_1, lfsv_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_trialgrad_index])[0])[idim0];
          acc_lfsv_0_0_trialgrad_index = acc_lfsv_0_0_trialgrad_index + x(lfsv_0, lfsv_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_trialgrad_index])[0])[idim0];
        }
        gradu_2[idim0] = acc_lfsv_0_0_trialgrad_index_1;
        gradu_1[idim0] = acc_lfsv_0_0_trialgrad_index_0;
        gradu_0[idim0] = acc_lfsv_0_0_trialgrad_index;
      }
      t_55 = 1.0 + (jit[0])[0] * gradu_0[0];
      t_56 = 1.0 + (jit[1])[1] * gradu_1[1];
      t_54 = 1.0 + (jit[2])[2] * gradu_2[2];
      t_86 = t_56 * t_54 + t_51 * gradu_1[2] * gradu_2[1];
      t_84 = t_48 * gradu_1[2] * gradu_2[0] + t_57 * t_54 * gradu_1[0];
      t_87 = t_66 * gradu_1[0] * gradu_2[1] + t_57 * t_56 * gradu_2[0];
      t_88 = (jit[2])[2] * t_87 * gradu_0[2] + t_55 * t_86 + (jit[1])[1] * t_84 * gradu_0[1];
      t_114 = 1.0 / t_88;
      t_117 = -5.769230769230769 * t_87 * t_114 * t_114;
      t_50 = t_48 * gradu_2[0];
      t_61 = t_57 * t_54;
      t_68 = t_66 * gradu_2[1];
      t_52 = t_51 * gradu_2[1];
      t_44 = t_42 * gradu_2[0];
      t_121 = 3.846153846153846 + -5.769230769230769 * log(t_88);
      t_120 = t_121 * t_114;
      t_116 = -1.0 * t_87 * t_114;
      t_4 = t_2 * gradu_2[1];
      t_72 = t_21 * gradu_0[1] * gradu_2[0] + t_33 * t_55 * gradu_2[1];
      t_99 = -5.769230769230769 * t_72 * t_114 * t_114;
      t_23 = t_21 * gradu_2[0];
      t_98 = -1.0 * t_72 * t_114;
      t_58 = t_57 * t_56;
      t_20 = t_18 * gradu_1[2];
      t_22 = t_21 * gradu_1[0];
      t_17 = t_15 * gradu_1[2];
      t_14 = t_13 * gradu_1[0];
      t_41 = t_33 * t_55;
      t_67 = t_66 * gradu_0[1];
      t_69 = t_55 * t_56 + t_42 * gradu_0[1] * gradu_1[0];
      t_89 = -5.769230769230769 * t_69 * t_114 * t_114;
      t_43 = t_42 * gradu_1[0];
      t_91 = -1.0 * t_69 * t_114;
      t_3 = t_2 * gradu_0[1];
      t_9 = t_8 * gradu_1[2];
      t_70 = t_48 * gradu_0[2] * gradu_1[0] + t_24 * t_55 * gradu_1[2];
      t_92 = -1.0 * t_70 * t_114;
      t_49 = t_48 * gradu_1[0];
      t_93 = -5.769230769230769 * t_70 * t_114 * t_114;
      t_10 = t_8 * gradu_2[0];
      t_73 = t_55 * t_54 + t_8 * gradu_2[0] * gradu_0[2];
      t_103 = -1.0 * t_73 * t_114;
      t_101 = -5.769230769230769 * t_73 * t_114 * t_114;
      t_85 = t_48 * gradu_1[2] * gradu_2[0] + t_57 * gradu_1[0] * t_54;
      t_108 = -5.769230769230769 * t_85 * t_114 * t_114;
      t_110 = -1.0 * t_85 * t_114;
      t_11 = t_8 * gradu_0[2];
      t_16 = t_15 * gradu_0[2];
      t_25 = t_24 * t_55;
      t_12 = t_8 * gradu_1[0];
      t_0 = t * gradu_0[2];
      t_74 = t_5 * gradu_0[2] * gradu_2[1] + t_33 * t_54 * gradu_0[1];
      t_106 = -1.0 * t_74 * t_114;
      t_104 = -5.769230769230769 * t_74 * t_114 * t_114;
      t_53 = t_51 * gradu_0[1];
      t_111 = -5.769230769230769 * t_86 * t_114 * t_114;
      t_113 = -1.0 * t_86 * t_114;
      t_71 = t * gradu_0[1] * gradu_1[2] + t_24 * t_56 * gradu_0[2];
      t_95 = -5.769230769230769 * t_71 * t_114 * t_114;
      t_1 = t * gradu_1[2];
      t_32 = t_24 * t_56;
      t_97 = -1.0 * t_71 * t_114;
      t_6 = t_5 * gradu_2[1];
      t_34 = t_33 * t_54;
      t_19 = t_18 * gradu_0[2];
      t_7 = t_5 * gradu_0[1];
      t_90 = -1.0 * t_121 * t_114 * t_69 * t_114;
      t_100 = -1.0 * t_121 * t_114 * t_72 * t_114;
      t_115 = -1.0 * t_121 * t_114 * t_87 * t_114;
      t_94 = -1.0 * t_121 * t_114 * t_70 * t_114;
      t_102 = -1.0 * t_121 * t_114 * t_73 * t_114;
      t_109 = -1.0 * t_121 * t_114 * t_85 * t_114;
      t_96 = -1.0 * t_121 * t_114 * t_71 * t_114;
      t_112 = -1.0 * t_121 * t_114 * t_86 * t_114;
      t_105 = -1.0 * t_121 * t_114 * t_74 * t_114;
      for (int lfsv_0_1_index = 0; lfsv_0_1_index <= -1 + lfsv_0_size; ++lfsv_0_1_index)
      {
        t_119 = (t_68 * ((js_CG1[lfsv_0_1_index])[0])[0] + t_44 * ((js_CG1[lfsv_0_1_index])[0])[1]) * (jit[2])[2] * gradu_0[2] + t_55 * (t_54 * (jit[1])[1] * ((js_CG1[lfsv_0_1_index])[0])[1] + t_52 * ((js_CG1[lfsv_0_1_index])[0])[2]) + (t_50 * ((js_CG1[lfsv_0_1_index])[0])[2] + t_61 * ((js_CG1[lfsv_0_1_index])[0])[0]) * (jit[1])[1] * gradu_0[1];
        t_26 = t_24 * (t_120 * (t_116 * t_119 + t_68 * ((js_CG1[lfsv_0_1_index])[0])[0] + t_44 * ((js_CG1[lfsv_0_1_index])[0])[1]) + t_117 * t_119);
        t_107 = t_87 * (jit[2])[2] * ((js_CG1[lfsv_0_1_index])[0])[2] + t_86 * (jit[0])[0] * ((js_CG1[lfsv_0_1_index])[0])[0] + t_84 * (jit[1])[1] * ((js_CG1[lfsv_0_1_index])[0])[1];
        t_27 = t_24 * (t_120 * (t_98 * t_107 + t_23 * ((js_CG1[lfsv_0_1_index])[0])[1] + t_4 * ((js_CG1[lfsv_0_1_index])[0])[0]) + t_99 * t_107);
        t_118 = (t_22 * ((js_CG1[lfsv_0_1_index])[0])[1] + t_58 * ((js_CG1[lfsv_0_1_index])[0])[0]) * (jit[2])[2] * gradu_0[2] + t_55 * (t_56 * (jit[2])[2] * ((js_CG1[lfsv_0_1_index])[0])[2] + t_20 * ((js_CG1[lfsv_0_1_index])[0])[1]) + (t_17 * ((js_CG1[lfsv_0_1_index])[0])[0] + t_14 * ((js_CG1[lfsv_0_1_index])[0])[2]) * (jit[1])[1] * gradu_0[1];
        t_28 = t_24 * (t_120 * (t_98 * t_118 + t_67 * ((js_CG1[lfsv_0_1_index])[0])[0] + t_41 * ((js_CG1[lfsv_0_1_index])[0])[1]) + t_99 * t_118);
        t_29 = t_24 * (t_120 * (t_91 * t_107 + t_56 * (jit[0])[0] * ((js_CG1[lfsv_0_1_index])[0])[0] + t_43 * ((js_CG1[lfsv_0_1_index])[0])[1]) + t_89 * t_107);
        t_30 = t_24 * (t_120 * (t_116 * t_118 + t_22 * ((js_CG1[lfsv_0_1_index])[0])[1] + t_58 * ((js_CG1[lfsv_0_1_index])[0])[0]) + t_117 * t_118);
        t_31 = t_24 * (t_120 * (t_91 * t_119 + t_55 * (jit[1])[1] * ((js_CG1[lfsv_0_1_index])[0])[1] + t_3 * ((js_CG1[lfsv_0_1_index])[0])[0]) + t_89 * t_119);
        t_35 = t_33 * (t_120 * (t_92 * t_107 + t_49 * ((js_CG1[lfsv_0_1_index])[0])[2] + t_9 * ((js_CG1[lfsv_0_1_index])[0])[0]) + t_93 * t_107);
        t_36 = t_33 * (t_120 * (t_103 * t_107 + t_54 * (jit[0])[0] * ((js_CG1[lfsv_0_1_index])[0])[0] + t_10 * ((js_CG1[lfsv_0_1_index])[0])[2]) + t_101 * t_107);
        t_37 = t_33 * (t_120 * (t_110 * t_119 + t_50 * ((js_CG1[lfsv_0_1_index])[0])[2] + t_61 * ((js_CG1[lfsv_0_1_index])[0])[0]) + t_108 * t_119);
        t_38 = t_33 * (t_120 * (t_103 * t_118 + t_55 * (jit[2])[2] * ((js_CG1[lfsv_0_1_index])[0])[2] + t_11 * ((js_CG1[lfsv_0_1_index])[0])[0]) + t_101 * t_118);
        t_39 = t_33 * (t_120 * (t_92 * t_119 + t_16 * ((js_CG1[lfsv_0_1_index])[0])[0] + t_25 * ((js_CG1[lfsv_0_1_index])[0])[2]) + t_93 * t_119);
        t_40 = t_33 * (t_120 * (t_110 * t_118 + t_17 * ((js_CG1[lfsv_0_1_index])[0])[0] + t_12 * ((js_CG1[lfsv_0_1_index])[0])[2]) + t_108 * t_118);
        t_59 = t_57 * (t_120 * (t_106 * t_118 + t_0 * ((js_CG1[lfsv_0_1_index])[0])[1] + t_53 * ((js_CG1[lfsv_0_1_index])[0])[2]) + t_104 * t_118);
        t_60 = t_57 * (t_120 * (t_113 * t_118 + t_56 * (jit[2])[2] * ((js_CG1[lfsv_0_1_index])[0])[2] + t_20 * ((js_CG1[lfsv_0_1_index])[0])[1]) + t_111 * t_118);
        t_62 = t_57 * (t_120 * (t_97 * t_107 + t_1 * ((js_CG1[lfsv_0_1_index])[0])[1] + t_32 * ((js_CG1[lfsv_0_1_index])[0])[2]) + t_95 * t_107);
        t_63 = t_57 * (t_120 * (t_113 * t_119 + t_54 * (jit[1])[1] * ((js_CG1[lfsv_0_1_index])[0])[1] + t_52 * ((js_CG1[lfsv_0_1_index])[0])[2]) + t_111 * t_119);
        t_64 = t_57 * (t_120 * (t_106 * t_107 + t_6 * ((js_CG1[lfsv_0_1_index])[0])[2] + t_34 * ((js_CG1[lfsv_0_1_index])[0])[1]) + t_104 * t_107);
        t_65 = t_57 * (t_120 * (t_97 * t_119 + t_7 * ((js_CG1[lfsv_0_1_index])[0])[2] + t_19 * ((js_CG1[lfsv_0_1_index])[0])[1]) + t_95 * t_119);
        t_75 = -1.0 * (t_90 * t_118 + t_89 * t_118) + t_45 * ((js_CG1[lfsv_0_1_index])[0])[2];
        t_76 = -1.0 * (t_100 * t_119 + t_99 * t_119) + t_45 * ((js_CG1[lfsv_0_1_index])[0])[2];
        t_77 = -1.0 * (t_115 * t_107 + t_117 * t_107) + t_45 * ((js_CG1[lfsv_0_1_index])[0])[2];
        t_78 = -1.0 * (t_94 * t_118 + t_93 * t_118) + t_46 * ((js_CG1[lfsv_0_1_index])[0])[1];
        t_79 = -1.0 * (t_102 * t_119 + t_101 * t_119) + t_46 * ((js_CG1[lfsv_0_1_index])[0])[1];
        t_80 = -1.0 * (t_109 * t_107 + t_108 * t_107) + t_46 * ((js_CG1[lfsv_0_1_index])[0])[1];
        t_81 = -1.0 * (t_96 * t_118 + t_95 * t_118) + t_47 * ((js_CG1[lfsv_0_1_index])[0])[0];
        t_82 = -1.0 * (t_112 * t_107 + t_111 * t_107) + t_47 * ((js_CG1[lfsv_0_1_index])[0])[0];
        t_83 = -1.0 * (t_105 * t_119 + t_104 * t_119) + t_47 * ((js_CG1[lfsv_0_1_index])[0])[0];
        for (int lfsv_0_0_index = 0; lfsv_0_0_index <= -1 + lfsv_0_size; ++lfsv_0_0_index)
        {
          jac.accumulate(lfsv_2, lfsv_0_0_index, lfsv_2, lfsv_0_1_index, (t_81 * (jit[0])[0] * ((js_CG1[lfsv_0_0_index])[0])[0] + t_78 * (jit[1])[1] * ((js_CG1[lfsv_0_0_index])[0])[1] + t_75 * (jit[2])[2] * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_2, lfsv_0_0_index, lfsv_1, lfsv_0_1_index, (t_65 * ((js_CG1[lfsv_0_0_index])[0])[0] + t_39 * ((js_CG1[lfsv_0_0_index])[0])[1] + t_31 * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_2, lfsv_0_0_index, lfsv_0, lfsv_0_1_index, (t_62 * ((js_CG1[lfsv_0_0_index])[0])[0] + t_35 * ((js_CG1[lfsv_0_0_index])[0])[1] + t_29 * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_1, lfsv_0_0_index, lfsv_2, lfsv_0_1_index, (t_59 * ((js_CG1[lfsv_0_0_index])[0])[0] + t_38 * ((js_CG1[lfsv_0_0_index])[0])[1] + t_28 * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_1, lfsv_0_0_index, lfsv_1, lfsv_0_1_index, (t_83 * (jit[0])[0] * ((js_CG1[lfsv_0_0_index])[0])[0] + t_79 * (jit[1])[1] * ((js_CG1[lfsv_0_0_index])[0])[1] + t_76 * (jit[2])[2] * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_1, lfsv_0_0_index, lfsv_0, lfsv_0_1_index, (t_64 * ((js_CG1[lfsv_0_0_index])[0])[0] + t_36 * ((js_CG1[lfsv_0_0_index])[0])[1] + t_27 * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_0, lfsv_0_0_index, lfsv_2, lfsv_0_1_index, (t_60 * ((js_CG1[lfsv_0_0_index])[0])[0] + t_40 * ((js_CG1[lfsv_0_0_index])[0])[1] + t_30 * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_0, lfsv_0_0_index, lfsv_1, lfsv_0_1_index, (t_63 * ((js_CG1[lfsv_0_0_index])[0])[0] + t_37 * ((js_CG1[lfsv_0_0_index])[0])[1] + t_26 * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
          jac.accumulate(lfsv_0, lfsv_0_0_index, lfsv_0, lfsv_0_1_index, (t_82 * (jit[0])[0] * ((js_CG1[lfsv_0_0_index])[0])[0] + t_80 * (jit[1])[1] * ((js_CG1[lfsv_0_0_index])[0])[1] + t_77 * (jit[2])[2] * ((js_CG1[lfsv_0_0_index])[0])[2]) * qw_dim3_order2[q] * detjac);
        }
      }
    }
  }
};


#pragma GCC diagnostic pop

#endif //HYPERELASTICITY_GENERATED_RCOARSEOPERATOR_FILE_HH
