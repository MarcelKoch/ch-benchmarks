#ifndef HYPERELASTICITY_GENERATED_ROPERATOR_FILE_HH
#define HYPERELASTICITY_GENERATED_ROPERATOR_FILE_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/typetree/childextraction.hh"
#include "dune/codegen/localbasiscache.hh"
#include "dune/localfunctions/lagrange/lagrangecube.hh"
#include "dune/codegen/common/vectorclass.hh"
#include "dune/geometry/referenceelements.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
using std::abs;


template<typename GFSU, typename GFSV>
class rOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags
{  

  public:
  rOperator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams) :
      _iniParams(iniParams),
      BCG1_microElementBasis()
  {
    jit = gfsu.gridView().template begin<0>()->geometry().jacobianInverseTransposed(Dune::FieldVector<double, 3>());
    fillQuadraturePointsCache(gfsu.gridView().template begin<0>()->geometry(), 2, qp_dim3_order2);
    for (int q = 0; q < 8; ++q)
    {
      std::vector<typename Q1_LocalBasis::Traits::JacobianType> evaluated;
      BCG1_microElementBasis.evaluateJacobian(qp_dim3_order2[q], evaluated);
      for (int j = 0; j < 8; ++j)
        for (int k = 0; k < 3; ++k)
        js_BCG1[k + j * 3 + q * 24] = evaluated[j][0][k];
    }
    for (int q = 0; q < 8; ++q)
    {
      std::vector<typename Q1_LocalBasis::Traits::RangeType> evaluated;
      BCG1_microElementBasis.evaluateFunction(qp_dim3_order2[q], evaluated);
      for (int j = 0; j < 8; ++j)
        phi_BCG1[j + q * 8] = evaluated[j][0];
    }
    fillQuadratureWeightsCache(gfsu.gridView().template begin<0>()->geometry(), 2, qw_dim3_order2);
    detjac = gfsu.gridView().template begin<0>()->geometry().integrationElement(Dune::FieldVector<double, 3>());
    fillQuadraturePointsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 2, qp_dim2_order2);
    fillQuadratureWeightsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 2, qw_dim2_order2);
    for (int f_id = 0; f_id < 6; ++f_id)
    {
      auto ref_el = Dune::referenceElement<double, 3>(Dune::GeometryTypes::cube(3));
      auto is_geo = ref_el.geometry<1>(f_id);
      for (int q = 0; q < 4; ++q)
      {
        std::vector<typename Q1_LocalBasis::Traits::RangeType> evaluated;
        auto qp_in_inside = is_geo.global(qp_dim2_order2[q]);
        BCG1_microElementBasis.evaluateFunction(qp_in_inside, evaluated);
        for (int j = 0; j < 8; ++j)
          phi_BCG1_s[j + q * 8 + f_id * 32] = evaluated[j][0];
      }
    }
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = false };
  

  public:
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::JacobianInverseTransposed jit;
  

  public:
  using GFSU_0 = Dune::TypeTree::Child<GFSU,0>;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 3>::Vector> qp_dim3_order2;
  

  public:
  double js_BCG1[192];
  

  public:
  using Q1_LocalBasis = Dune::Impl::LagrangeCubeLocalBasis<typename GFSU_0::Traits::GridView::ctype, double, 3, 1>;
  

  public:
  const Q1_LocalBasis BCG1_microElementBasis;
  

  public:
  using GFSU_1 = Dune::TypeTree::Child<GFSU,1>;
  

  public:
  using GFSU_2 = Dune::TypeTree::Child<GFSU,2>;
  

  public:
  double phi_BCG1[64];
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 3>::Field> qw_dim3_order2;
  

  public:
  typename GFSU::Traits::GridView::template Codim<0>::Geometry::ctype detjac;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Vector> qp_dim2_order2;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Field> qw_dim2_order2;
  

  public:
  double phi_BCG1_s[192];
  

  public:
  enum { doAlphaBoundary = true };


  // define sparsity pattern of operator representation
  template<typename LFSU, typename LFSV, typename LocalPattern>
  void pattern_volume (const LFSU& lfsu, const LFSV& lfsv,
                       LocalPattern& pattern) const
  {
    using namespace Dune::Indices;
    auto lfsu_0 = child(lfsu, _0);
    auto lfsu_1 = child(lfsu, _1);
    auto lfsu_2 = child(lfsu, _2);
    for (int ez = 0; ez <= 3; ++ez)
      for (int ey = 0; ey <= 3; ++ey)
        for (int ex = 0; ex <= 3; ++ex)
          for (int iz = 0; iz <= 1; ++iz)
            for (int iy = 0; iy <= 1; ++iy)
              for (int ix = 0; ix <= 1; ++ix)
                for (int jz = 0; jz <= 1; ++jz)
                  for (int jy = 0; jy <= 1; ++jy)
                    for (int jx = 0; jx <= 1; ++jx)
                    {
                      pattern.addLink(lfsu_2, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_2, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_2, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_1, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_2, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_0, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_1, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_2, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_1, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_1, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_1, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_0, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_0, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_2, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_0, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_1, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                      pattern.addLink(lfsu_0, 5 * (ey + iy) + 25 * (ez + iz) + ex + ix, lfsu_0, 5 * (ey + jy) + 25 * (ez + jz) + ex + jx);
                    }
  }

  public:
  template<typename LFSV, typename Z, typename IG, typename X, typename R, typename LFSU>
  void jacobian_apply_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const Z& z_s, const LFSV& lfsv_s, R& r_s) const
  {
  }
  

  public:
  template<typename LFSV, typename IG, typename J, typename X, typename LFSU>
  void jacobian_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, J& jac_s_s) const
  {
  }
  

  public:
  template<typename LFSV, typename IG, typename X, typename R, typename LFSU>
  void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const
  {
    auto geo_in_inside = ig.geometryInInside();
    const auto& inside_cell = ig.inside();
    auto cell_geo_s = inside_cell.geometry();
    auto is_geo = ig.geometry();
    auto refElement = referenceElement(is_geo);
    auto localcenter = refElement.position(0,0);
    auto fdetjac = is_geo.integrationElement(localcenter);
    using namespace Dune::Indices;
    auto lfsv_s_0 = child(lfsv_s, _0);
    auto lfsv_s_0_size = lfsv_s_0.size();
    auto lfsv_s_1 = child(lfsv_s, _1);
    auto lfsv_s_1_size = lfsv_s_1.size();
    auto lfsv_s_2 = child(lfsv_s, _2);
    auto lfsv_s_2_size = lfsv_s_2.size();
    auto r_s_lfsv_s_0_alias = &r_s.container()(lfsv_s_0,0);
    auto r_s_lfsv_s_0_alias_tail = &r_s.container()(lfsv_s_0,0);
    Dune::FieldVector<Dune::FieldVector<double, 3>, 8> corners(Dune::FieldVector<double, 3>(0.0));
    int face_id;
    Dune::FieldVector<double, 3> qp_dim2_order2_in_inside(0.0);
    double qp_dim2_order2_in_inside_macro[3];
    double qp_dim2_order2_in_inside_macro_global[3];
    double r_lfsv_s_0_loc[2 * 2 * 2];
    double t;
    double t_0;
  
    t = r_s.weight();
    face_id = ig.indexInInside();
    for (int i_corner = 0; i_corner <= 7; ++i_corner)
      corners[i_corner] = cell_geo_s.corner(i_corner);
    for (int subel_z = 0; subel_z <= 3; ++subel_z)
      for (int subel_y = 0; subel_y <= 3; ++subel_y)
        for (int subel_x = 0; subel_x <= 3; ++subel_x)
        {
          for (int micro_12_z = 0; micro_12_z <= 1; ++micro_12_z)
            for (int micro_12_y = 0; micro_12_y <= 1; ++micro_12_y)
              for (int micro_12_x = 0; micro_12_x <= 1; ++micro_12_x)
                r_lfsv_s_0_loc[4 * micro_12_x + 2 * micro_12_y + micro_12_z] = 0.0;
          for (int q = 0; q <= 3; ++q)
          {
            qp_dim2_order2_in_inside = geo_in_inside.global(qp_dim2_order2[q]);
            qp_dim2_order2_in_inside_macro[2] = (qp_dim2_order2_in_inside[2] + subel_z) / 4.0;
            qp_dim2_order2_in_inside_macro[1] = (qp_dim2_order2_in_inside[1] + subel_y) / 4.0;
            qp_dim2_order2_in_inside_macro[0] = (qp_dim2_order2_in_inside[0] + subel_x) / 4.0;
            for (int idim_to_global0 = 0; idim_to_global0 <= 2; ++idim_to_global0)
              qp_dim2_order2_in_inside_macro_global[idim_to_global0] = ((corners[7])[idim_to_global0] + -1.0 * (corners[0])[idim_to_global0]) * qp_dim2_order2_in_inside_macro[idim_to_global0] + (corners[0])[idim_to_global0];
            if ((face_id == 4 ? subel_z == 0 : 1) && (face_id == 2 ? subel_y == 0 : 1) && (face_id == 1 ? subel_x == 3 : 1) && (face_id == 3 ? subel_y == 3 : 1) && (face_id == 0 ? subel_x == 0 : 1) && (abs(qp_dim2_order2_in_inside_macro_global[0]) < 1e-10 || abs(-1.0 + qp_dim2_order2_in_inside_macro_global[0]) < 1e-10 ? 1.0 : 0.0) == 0.0 && (face_id == 5 ? subel_z == 3 : 1))
            {
              t_0 = -0.00625 * qw_dim2_order2[q] * fdetjac;
              for (int micro_11_z = 0; micro_11_z <= 1; ++micro_11_z)
                for (int micro_11_y = 0; micro_11_y <= 1; ++micro_11_y)
                  for (int micro_11_x = 0; micro_11_x <= 1; ++micro_11_x)
                    r_lfsv_s_0_loc[4 * micro_11_x + 2 * micro_11_y + micro_11_z] = t_0 * phi_BCG1_s[8 * (q + 4 * face_id) + micro_11_x + 2 * micro_11_y + 4 * micro_11_z] + r_lfsv_s_0_loc[4 * micro_11_x + 2 * micro_11_y + micro_11_z];
            }
          }
          for (int micro_13_z = 0; micro_13_z <= 1; ++micro_13_z)
            for (int micro_13_y = 0; micro_13_y <= 1; ++micro_13_y)
              for (int micro_13_x = 0; micro_13_x <= 1; ++micro_13_x)
                r_s_lfsv_s_0_alias[subel_x + 5 * subel_y + 25 * subel_z + micro_13_x + 5 * micro_13_y + 25 * micro_13_z] = r_s_lfsv_s_0_alias[subel_x + 5 * subel_y + 25 * subel_z + micro_13_x + 5 * micro_13_y + 25 * micro_13_z] + r_lfsv_s_0_loc[4 * micro_13_x + 2 * micro_13_y + micro_13_z] * t;
        }
  }
  

  public:
  template<typename LFSV, typename EG, typename X, typename R, typename LFSU>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    using namespace Dune::Indices;
    auto lfsu_0 = child(lfsu, _0);
    auto lfsu_0_size = lfsu_0.size();
    const auto x_lfsu_0_alias = &x(lfsu_0,0);
    auto lfsu_1 = child(lfsu, _1);
    auto lfsu_1_size = lfsu_1.size();
    const auto x_lfsu_1_alias = &x(lfsu_1,0);
    auto lfsu_2 = child(lfsu, _2);
    auto lfsu_2_size = lfsu_2.size();
    const auto x_lfsu_2_alias = &x(lfsu_2,0);
    auto r_lfsu_0_alias = &r.container()(lfsu_0,0);
    auto r_lfsu_0_alias_tail = &r.container()(lfsu_0,0);
    auto r_lfsu_1_alias = &r.container()(lfsu_1,0);
    auto r_lfsu_1_alias_tail = &r.container()(lfsu_1,0);
    auto r_lfsu_2_alias = &r.container()(lfsu_2,0);
    auto r_lfsu_2_alias_tail = &r.container()(lfsu_2,0);
    Vec4d acc_micro_4_x_micro_4_y_micro_4_z;
    Vec4d acc_micro_4_x_micro_4_y_micro_4_z_0;
    Vec4d acc_micro_4_x_micro_4_y_micro_4_z_1;
    Vec4d gradu_0[3];
    Vec4d gradu_1[3];
    Vec4d gradu_2[3];
    Vec4d r_lfsu_0_loc[2 * 2 * 2];
    Vec4d r_lfsu_0_vec4;
    Vec4d r_lfsu_1_loc[2 * 2 * 2];
    Vec4d r_lfsu_1_vec4;
    Vec4d r_lfsu_2_loc[2 * 2 * 2];
    Vec4d r_lfsu_2_vec4;
    double t;
    Vec4d t_0;
    double t_1;
    double t_10;
    Vec4d t_11;
    double t_12;
    double t_13;
    Vec4d t_14;
    Vec4d t_15;
    double t_16;
    Vec4d t_17;
    Vec4d t_18;
    double t_19;
    Vec4d t_2;
    Vec4d t_20;
    double t_21;
    Vec4d t_22;
    double t_23;
    Vec4d t_24;
    Vec4d t_25;
    double t_26;
    Vec4d t_27;
    Vec4d t_28;
    Vec4d t_29;
    double t_3;
    Vec4d t_30;
    double t_31;
    Vec4d t_32;
    Vec4d t_33;
    double t_34;
    Vec4d t_35;
    double t_36;
    Vec4d t_37;
    Vec4d t_38;
    Vec4d t_39;
    Vec4d t_4;
    Vec4d t_40;
    double t_41;
    Vec4d t_42;
    Vec4d t_43;
    Vec4d t_44;
    Vec4d t_45;
    Vec4d t_46;
    Vec4d t_47;
    Vec4d t_48;
    Vec4d t_49;
    double t_5;
    Vec4d t_50;
    Vec4d t_51;
    Vec4d t_52;
    Vec4d t_53;
    Vec4d t_54;
    Vec4d t_55;
    Vec4d t_56;
    Vec4d t_57;
    Vec4d t_58;
    Vec4d t_59;
    Vec4d t_6;
    Vec4d t_60;
    Vec4d t_61;
    double t_7;
    Vec4d t_8;
    Vec4d t_9;
    Vec4d x_lfsu_0_loc[2 * 2 * 2];
    Vec4d x_lfsu_0_vec4;
    Vec4d x_lfsu_1_loc[2 * 2 * 2];
    Vec4d x_lfsu_1_vec4;
    Vec4d x_lfsu_2_loc[2 * 2 * 2];
    Vec4d x_lfsu_2_vec4;
  
    t_12 = r.weight();
    t_41 = 0.015625 * detjac;
    t_7 = 16.0 * (jit[0])[0] * (jit[0])[0];
    t_13 = 16.0 * (jit[2])[2] * (jit[2])[2];
    t_16 = 16.0 * (jit[1])[1] * (jit[1])[1];
    t_23 = 4.0 * (jit[0])[0];
    t_26 = 4.0 * (jit[1])[1];
    t_36 = 4.0 * (jit[2])[2];
    t_31 = -4.0 * (jit[0])[0];
    t_34 = 16.0 * (jit[0])[0] * (jit[1])[1];
    t_21 = -16.0 * (jit[2])[2] * (jit[1])[1];
    t_19 = 16.0 * (jit[2])[2] * (jit[0])[0];
    t_10 = -16.0 * (jit[1])[1] * (jit[0])[0];
    t_5 = 16.0 * (jit[1])[1] * (jit[0])[0];
    t_3 = -16.0 * (jit[1])[1] * (jit[2])[2];
    t_1 = 16.0 * (jit[0])[0] * (jit[2])[2];
    t = -16.0 * (jit[2])[2] * (jit[0])[0];
    for (int subel_z = 0; subel_z <= 3; ++subel_z)
      for (int subel_y = 0; subel_y <= 3; ++subel_y)
      {
        int const subel_x_outer = 0;
  
        for (int micro_5_z = 0; micro_5_z <= 1; ++micro_5_z)
          for (int micro_5_y = 0; micro_5_y <= 1; ++micro_5_y)
            for (int micro_5_x = 0; micro_5_x <= 1; ++micro_5_x)
            {
              x_lfsu_2_vec4.load(x_lfsu_2_alias + 5 * subel_y + 25 * subel_z + micro_5_x + 5 * micro_5_y + 25 * micro_5_z);
              x_lfsu_1_vec4.load(x_lfsu_1_alias + 5 * subel_y + 25 * subel_z + micro_5_x + 5 * micro_5_y + 25 * micro_5_z);
              x_lfsu_0_vec4.load(x_lfsu_0_alias + 5 * subel_y + 25 * subel_z + micro_5_x + 5 * micro_5_y + 25 * micro_5_z);
              r_lfsu_2_loc[4 * micro_5_x + 2 * micro_5_y + micro_5_z] = 0.0;
              r_lfsu_1_loc[4 * micro_5_x + 2 * micro_5_y + micro_5_z] = 0.0;
              r_lfsu_0_loc[4 * micro_5_x + 2 * micro_5_y + micro_5_z] = 0.0;
              x_lfsu_2_loc[4 * micro_5_x + 2 * micro_5_y + micro_5_z] = x_lfsu_2_vec4;
              x_lfsu_1_loc[4 * micro_5_x + 2 * micro_5_y + micro_5_z] = x_lfsu_1_vec4;
              x_lfsu_0_loc[4 * micro_5_x + 2 * micro_5_y + micro_5_z] = x_lfsu_0_vec4;
            }
        for (int q = 0; q <= 7; ++q)
        {
          t_42 = t_41 * qw_dim3_order2[q];
          for (int idim0 = 0; idim0 <= 2; ++idim0)
          {
            acc_micro_4_x_micro_4_y_micro_4_z_1 = 0.0;
            acc_micro_4_x_micro_4_y_micro_4_z_0 = 0.0;
            acc_micro_4_x_micro_4_y_micro_4_z = 0.0;
            for (int micro_4_z = 0; micro_4_z <= 1; ++micro_4_z)
              for (int micro_4_y = 0; micro_4_y <= 1; ++micro_4_y)
                for (int micro_4_x = 0; micro_4_x <= 1; ++micro_4_x)
                {
                  acc_micro_4_x_micro_4_y_micro_4_z_1 = acc_micro_4_x_micro_4_y_micro_4_z_1 + x_lfsu_2_loc[4 * micro_4_x + 2 * micro_4_y + micro_4_z] * js_BCG1[idim0 + 24 * q + 3 * micro_4_x + 6 * micro_4_y + 12 * micro_4_z];
                  acc_micro_4_x_micro_4_y_micro_4_z_0 = acc_micro_4_x_micro_4_y_micro_4_z_0 + x_lfsu_1_loc[4 * micro_4_x + 2 * micro_4_y + micro_4_z] * js_BCG1[idim0 + 24 * q + 3 * micro_4_x + 6 * micro_4_y + 12 * micro_4_z];
                  acc_micro_4_x_micro_4_y_micro_4_z = acc_micro_4_x_micro_4_y_micro_4_z + x_lfsu_0_loc[4 * micro_4_x + 2 * micro_4_y + micro_4_z] * js_BCG1[idim0 + 24 * q + 3 * micro_4_x + 6 * micro_4_y + 12 * micro_4_z];
                }
            gradu_2[idim0] = acc_micro_4_x_micro_4_y_micro_4_z_1;
            gradu_1[idim0] = acc_micro_4_x_micro_4_y_micro_4_z_0;
            gradu_0[idim0] = acc_micro_4_x_micro_4_y_micro_4_z;
          }
          t_8 = t_7 * gradu_1[0];
          t_9 = t_7 * gradu_2[0];
          t_14 = t_13 * gradu_0[2];
          t_15 = t_13 * gradu_1[2];
          t_17 = t_16 * gradu_2[1];
          t_18 = t_16 * gradu_0[1];
          t_51 = 1.0 + t_23 * gradu_0[0];
          t_24 = t_23 * t_51;
          t_28 = t_26 * (1.0 + t_26 * gradu_1[1]);
          t_37 = t_36 * (1.0 + t_36 * gradu_2[2]);
          t_54 = t_34 * gradu_1[0] * gradu_2[1] + t_31 * (1.0 + t_26 * gradu_1[1]) * gradu_2[0];
          t_40 = t_36 * t_54;
          t_50 = (1.0 + t_26 * gradu_1[1]) * (1.0 + t_36 * gradu_2[2]) + t_21 * gradu_1[2] * gradu_2[1];
          t_25 = t_23 * t_50;
          t_49 = t_19 * gradu_1[2] * gradu_2[0] + t_31 * (1.0 + t_36 * gradu_2[2]) * gradu_1[0];
          t_27 = t_26 * t_49;
          t_30 = t_26 * (1.0 + t_36 * gradu_2[2]);
          t_20 = t_19 * gradu_2[0];
          t_35 = t_34 * gradu_2[1];
          t_29 = t_26 * gradu_0[1];
          t_22 = t_21 * gradu_2[1];
          t_32 = t_31 * (1.0 + t_36 * gradu_2[2]);
          t_38 = t_36 * gradu_0[2];
          t_11 = t_10 * gradu_2[0];
          t_33 = t_31 * (1.0 + t_26 * gradu_1[1]);
          t_6 = t_5 * gradu_1[0];
          t_39 = t_36 * (1.0 + t_26 * gradu_1[1]);
          t_4 = t_3 * gradu_1[2];
          t_2 = t_1 * gradu_1[2];
          t_0 = t * gradu_1[0];
          t_60 = t_36 * t_54 * gradu_0[2] + t_51 * t_50 + t_26 * t_49 * gradu_0[1];
          t_57 = 1.0 / t_60;
          t_61 = 5.769230769230769 * log(t_60) * t_57;
          for (int micro_6_z = 0; micro_6_z <= 1; ++micro_6_z)
            for (int micro_6_y = 0; micro_6_y <= 1; ++micro_6_y)
              for (int micro_6_x = 0; micro_6_x <= 1; ++micro_6_x)
              {
                t_43 = t_8 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_44 = t_9 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_45 = t_14 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_46 = t_15 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_47 = t_17 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_48 = t_18 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_52 = t_24 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_53 = t_28 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_55 = t_37 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_56 = t_40 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_25 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_27 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z];
                t_58 = t_38 * (t_35 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_11 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z]) + t_51 * (t_30 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_22 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z]) + t_29 * (t_20 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_32 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z]);
                t_59 = t_38 * (t_6 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_33 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z]) + t_51 * (t_39 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_4 * js_BCG1[1 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z]) + t_29 * (t_2 * js_BCG1[24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z] + t_0 * js_BCG1[2 + 24 * q + 3 * micro_6_x + 6 * micro_6_y + 12 * micro_6_z]);
                r_lfsu_2_loc[4 * micro_6_x + 2 * micro_6_y + micro_6_z] = t_42 * (t_61 * t_59 + -3.846153846153846 * t_59 * t_57 + 1.923076923076923 * (t_44 + t_44 + t_47 + t_47 + t_55 + t_55)) + r_lfsu_2_loc[4 * micro_6_x + 2 * micro_6_y + micro_6_z];
                r_lfsu_1_loc[4 * micro_6_x + 2 * micro_6_y + micro_6_z] = t_42 * (0.5 * phi_BCG1[8 * q + micro_6_x + 2 * micro_6_y + 4 * micro_6_z] + t_61 * t_58 + -3.846153846153846 * t_58 * t_57 + 1.923076923076923 * (t_43 + t_43 + t_53 + t_53 + t_46 + t_46)) + r_lfsu_1_loc[4 * micro_6_x + 2 * micro_6_y + micro_6_z];
                r_lfsu_0_loc[4 * micro_6_x + 2 * micro_6_y + micro_6_z] = t_42 * (t_61 * t_56 + -3.846153846153846 * t_56 * t_57 + 1.923076923076923 * (t_52 + t_52 + t_48 + t_48 + t_45 + t_45)) + r_lfsu_0_loc[4 * micro_6_x + 2 * micro_6_y + micro_6_z];
              }
        }
        for (int micro_7_x = 0; micro_7_x <= 1; ++micro_7_x)
          for (int micro_7_z = 0; micro_7_z <= 1; ++micro_7_z)
            for (int micro_7_y = 0; micro_7_y <= 1; ++micro_7_y)
            {
              r_lfsu_2_vec4.load(r_lfsu_2_alias + 5 * subel_y + 25 * subel_z + micro_7_x + 5 * micro_7_y + 25 * micro_7_z);
              r_lfsu_1_vec4.load(r_lfsu_1_alias + 5 * subel_y + 25 * subel_z + micro_7_x + 5 * micro_7_y + 25 * micro_7_z);
              r_lfsu_0_vec4.load(r_lfsu_0_alias + 5 * subel_y + 25 * subel_z + micro_7_x + 5 * micro_7_y + 25 * micro_7_z);
              r_lfsu_2_vec4 = r_lfsu_2_vec4 + r_lfsu_2_loc[4 * micro_7_x + 2 * micro_7_y + micro_7_z] * t_12;
              r_lfsu_2_vec4.store(r_lfsu_2_alias + 5 * subel_y + 25 * subel_z + micro_7_x + 5 * micro_7_y + 25 * micro_7_z);
              r_lfsu_1_vec4 = r_lfsu_1_vec4 + r_lfsu_1_loc[4 * micro_7_x + 2 * micro_7_y + micro_7_z] * t_12;
              r_lfsu_1_vec4.store(r_lfsu_1_alias + 5 * subel_y + 25 * subel_z + micro_7_x + 5 * micro_7_y + 25 * micro_7_z);
              r_lfsu_0_vec4 = r_lfsu_0_vec4 + r_lfsu_0_loc[4 * micro_7_x + 2 * micro_7_y + micro_7_z] * t_12;
              r_lfsu_0_vec4.store(r_lfsu_0_alias + 5 * subel_y + 25 * subel_z + micro_7_x + 5 * micro_7_y + 25 * micro_7_z);
            }
      }
  }
  

  public:
  template<typename LFSV, typename Z, typename EG, typename X, typename R, typename LFSU>
  void jacobian_apply_volume(const EG& eg, const LFSU& lfsu, const X& x, const Z& z, const LFSV& lfsv, R& r) const
  {
    using namespace Dune::Indices;
    auto lfsu_0 = child(lfsu, _0);
    auto lfsu_0_size = lfsu_0.size();
    const auto x_lfsu_0_alias = &x(lfsu_0,0);
    auto lfsu_1 = child(lfsu, _1);
    auto lfsu_1_size = lfsu_1.size();
    const auto x_lfsu_1_alias = &x(lfsu_1,0);
    auto lfsu_2 = child(lfsu, _2);
    auto lfsu_2_size = lfsu_2.size();
    const auto x_lfsu_2_alias = &x(lfsu_2,0);
    const auto z_lfsu_1_alias = &z(lfsu_1,0);
    const auto z_lfsu_2_alias = &z(lfsu_2,0);
    const auto z_lfsu_0_alias = &z(lfsu_0,0);
    auto r_lfsu_0_alias = &r.container()(lfsu_0,0);
    auto r_lfsu_0_alias_tail = &r.container()(lfsu_0,0);
    auto r_lfsu_1_alias = &r.container()(lfsu_1,0);
    auto r_lfsu_1_alias_tail = &r.container()(lfsu_1,0);
    auto r_lfsu_2_alias = &r.container()(lfsu_2,0);
    auto r_lfsu_2_alias_tail = &r.container()(lfsu_2,0);
    Vec4d acc_micro_18_x_micro_18_y_micro_18_z;
    Vec4d acc_micro_18_x_micro_18_y_micro_18_z_0;
    Vec4d acc_micro_18_x_micro_18_y_micro_18_z_1;
    Vec4d acc_micro_18_x_micro_18_y_micro_18_z_2;
    Vec4d acc_micro_18_x_micro_18_y_micro_18_z_3;
    Vec4d acc_micro_18_x_micro_18_y_micro_18_z_4;
    Vec4d gradu_0[3];
    Vec4d gradu_1[3];
    Vec4d gradu_2[3];
    Vec4d gradz_func_0[3];
    Vec4d gradz_func_1[3];
    Vec4d gradz_func_2[3];
    Vec4d r_lfsu_0_loc[2 * 2 * 2];
    Vec4d r_lfsu_0_vec4;
    Vec4d r_lfsu_1_loc[2 * 2 * 2];
    Vec4d r_lfsu_1_vec4;
    Vec4d r_lfsu_2_loc[2 * 2 * 2];
    Vec4d r_lfsu_2_vec4;
    double t;
    Vec4d t_0;
    double t_1;
    double t_10;
    double t_11;
    Vec4d t_12;
    Vec4d t_13;
    Vec4d t_14;
    double t_15;
    Vec4d t_16;
    Vec4d t_17;
    Vec4d t_18;
    double t_19;
    Vec4d t_2;
    Vec4d t_20;
    Vec4d t_21;
    Vec4d t_22;
    double t_23;
    Vec4d t_24;
    double t_25;
    Vec4d t_26;
    double t_27;
    double t_28;
    Vec4d t_29;
    double t_3;
    Vec4d t_30;
    double t_31;
    Vec4d t_32;
    Vec4d t_33;
    Vec4d t_34;
    double t_35;
    Vec4d t_36;
    Vec4d t_37;
    Vec4d t_38;
    double t_39;
    Vec4d t_4;
    Vec4d t_40;
    Vec4d t_41;
    Vec4d t_42;
    double t_43;
    Vec4d t_44;
    Vec4d t_45;
    Vec4d t_46;
    Vec4d t_47;
    Vec4d t_48;
    double t_49;
    Vec4d t_5;
    Vec4d t_50;
    Vec4d t_51;
    double t_52;
    Vec4d t_53;
    Vec4d t_54;
    Vec4d t_55;
    double t_56;
    Vec4d t_57;
    Vec4d t_58;
    Vec4d t_59;
    double t_6;
    Vec4d t_60;
    Vec4d t_61;
    double t_62;
    Vec4d t_63;
    Vec4d t_64;
    Vec4d t_65;
    Vec4d t_66;
    Vec4d t_67;
    Vec4d t_68;
    Vec4d t_69;
    Vec4d t_7;
    Vec4d t_70;
    Vec4d t_71;
    Vec4d t_72;
    Vec4d t_73;
    Vec4d t_74;
    Vec4d t_75;
    Vec4d t_76;
    Vec4d t_77;
    Vec4d t_78;
    Vec4d t_79;
    Vec4d t_8;
    Vec4d t_80;
    Vec4d t_81;
    Vec4d t_82;
    Vec4d t_83;
    Vec4d t_84;
    Vec4d t_85;
    Vec4d t_86;
    Vec4d t_87;
    Vec4d t_88;
    Vec4d t_89;
    Vec4d t_9;
    Vec4d t_90;
    Vec4d t_91;
    Vec4d x_lfsu_0_loc[2 * 2 * 2];
    Vec4d x_lfsu_0_vec4;
    Vec4d x_lfsu_1_loc[2 * 2 * 2];
    Vec4d x_lfsu_1_vec4;
    Vec4d x_lfsu_2_loc[2 * 2 * 2];
    Vec4d x_lfsu_2_vec4;
    Vec4d z_lfsu_0_loc[2 * 2 * 2];
    Vec4d z_lfsu_0_vec4;
    Vec4d z_lfsu_1_loc[2 * 2 * 2];
    Vec4d z_lfsu_1_vec4;
    Vec4d z_lfsu_2_loc[2 * 2 * 2];
    Vec4d z_lfsu_2_vec4;
  
    t_10 = r.weight();
    t_62 = 0.015625 * detjac;
    t_11 = 16.0 * (jit[2])[2] * (jit[2])[2];
    t_15 = 16.0 * (jit[1])[1] * (jit[1])[1];
    t_19 = 16.0 * (jit[0])[0] * (jit[0])[0];
    t_31 = 16.0 * (jit[2])[2] * (jit[0])[0];
    t_25 = 16.0 * (jit[0])[0] * (jit[2])[2];
    t_39 = 4.0 * (jit[0])[0];
    t_56 = 4.0 * (jit[2])[2];
    t_43 = 4.0 * (jit[1])[1];
    t_27 = 16.0 * (jit[2])[2] * (jit[1])[1];
    t_28 = 16.0 * (jit[1])[1] * (jit[2])[2];
    t_49 = -4.0 * (jit[0])[0];
    t_52 = 16.0 * (jit[0])[0] * (jit[1])[1];
    t_35 = -16.0 * (jit[2])[2] * (jit[1])[1];
    t_23 = 16.0 * (jit[1])[1] * (jit[0])[0];
    t_3 = -16.0 * (jit[0])[0] * (jit[2])[2];
    t_6 = -16.0 * (jit[1])[1] * (jit[0])[0];
    t_1 = -16.0 * (jit[1])[1] * (jit[2])[2];
    t = -16.0 * (jit[2])[2] * (jit[0])[0];
    for (int subel_z = 0; subel_z <= 3; ++subel_z)
      for (int subel_y = 0; subel_y <= 3; ++subel_y)
      {
        int const subel_x_outer = 0;
  
        for (int micro_19_z = 0; micro_19_z <= 1; ++micro_19_z)
          for (int micro_19_y = 0; micro_19_y <= 1; ++micro_19_y)
            for (int micro_19_x = 0; micro_19_x <= 1; ++micro_19_x)
            {
              z_lfsu_2_vec4.load(z_lfsu_2_alias + 5 * subel_y + 25 * subel_z + micro_19_x + 5 * micro_19_y + 25 * micro_19_z);
              z_lfsu_1_vec4.load(z_lfsu_1_alias + 5 * subel_y + 25 * subel_z + micro_19_x + 5 * micro_19_y + 25 * micro_19_z);
              z_lfsu_0_vec4.load(z_lfsu_0_alias + 5 * subel_y + 25 * subel_z + micro_19_x + 5 * micro_19_y + 25 * micro_19_z);
              x_lfsu_2_vec4.load(x_lfsu_2_alias + 5 * subel_y + 25 * subel_z + micro_19_x + 5 * micro_19_y + 25 * micro_19_z);
              x_lfsu_1_vec4.load(x_lfsu_1_alias + 5 * subel_y + 25 * subel_z + micro_19_x + 5 * micro_19_y + 25 * micro_19_z);
              x_lfsu_0_vec4.load(x_lfsu_0_alias + 5 * subel_y + 25 * subel_z + micro_19_x + 5 * micro_19_y + 25 * micro_19_z);
              r_lfsu_2_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = 0.0;
              r_lfsu_1_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = 0.0;
              r_lfsu_0_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = 0.0;
              z_lfsu_0_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = z_lfsu_0_vec4;
              z_lfsu_2_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = z_lfsu_2_vec4;
              z_lfsu_1_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = z_lfsu_1_vec4;
              x_lfsu_2_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = x_lfsu_2_vec4;
              x_lfsu_1_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = x_lfsu_1_vec4;
              x_lfsu_0_loc[4 * micro_19_x + 2 * micro_19_y + micro_19_z] = x_lfsu_0_vec4;
            }
        for (int q = 0; q <= 7; ++q)
        {
          t_63 = t_62 * qw_dim3_order2[q];
          for (int idim0 = 0; idim0 <= 2; ++idim0)
          {
            acc_micro_18_x_micro_18_y_micro_18_z_4 = 0.0;
            acc_micro_18_x_micro_18_y_micro_18_z_3 = 0.0;
            acc_micro_18_x_micro_18_y_micro_18_z_2 = 0.0;
            acc_micro_18_x_micro_18_y_micro_18_z_1 = 0.0;
            acc_micro_18_x_micro_18_y_micro_18_z_0 = 0.0;
            acc_micro_18_x_micro_18_y_micro_18_z = 0.0;
            for (int micro_18_z = 0; micro_18_z <= 1; ++micro_18_z)
              for (int micro_18_y = 0; micro_18_y <= 1; ++micro_18_y)
                for (int micro_18_x = 0; micro_18_x <= 1; ++micro_18_x)
                {
                  acc_micro_18_x_micro_18_y_micro_18_z_4 = acc_micro_18_x_micro_18_y_micro_18_z_4 + z_lfsu_0_loc[4 * micro_18_x + 2 * micro_18_y + micro_18_z] * js_BCG1[idim0 + 24 * q + 3 * micro_18_x + 6 * micro_18_y + 12 * micro_18_z];
                  acc_micro_18_x_micro_18_y_micro_18_z_3 = acc_micro_18_x_micro_18_y_micro_18_z_3 + z_lfsu_2_loc[4 * micro_18_x + 2 * micro_18_y + micro_18_z] * js_BCG1[idim0 + 24 * q + 3 * micro_18_x + 6 * micro_18_y + 12 * micro_18_z];
                  acc_micro_18_x_micro_18_y_micro_18_z_2 = acc_micro_18_x_micro_18_y_micro_18_z_2 + z_lfsu_1_loc[4 * micro_18_x + 2 * micro_18_y + micro_18_z] * js_BCG1[idim0 + 24 * q + 3 * micro_18_x + 6 * micro_18_y + 12 * micro_18_z];
                  acc_micro_18_x_micro_18_y_micro_18_z_1 = acc_micro_18_x_micro_18_y_micro_18_z_1 + x_lfsu_2_loc[4 * micro_18_x + 2 * micro_18_y + micro_18_z] * js_BCG1[idim0 + 24 * q + 3 * micro_18_x + 6 * micro_18_y + 12 * micro_18_z];
                  acc_micro_18_x_micro_18_y_micro_18_z_0 = acc_micro_18_x_micro_18_y_micro_18_z_0 + x_lfsu_1_loc[4 * micro_18_x + 2 * micro_18_y + micro_18_z] * js_BCG1[idim0 + 24 * q + 3 * micro_18_x + 6 * micro_18_y + 12 * micro_18_z];
                  acc_micro_18_x_micro_18_y_micro_18_z = acc_micro_18_x_micro_18_y_micro_18_z + x_lfsu_0_loc[4 * micro_18_x + 2 * micro_18_y + micro_18_z] * js_BCG1[idim0 + 24 * q + 3 * micro_18_x + 6 * micro_18_y + 12 * micro_18_z];
                }
            gradz_func_0[idim0] = acc_micro_18_x_micro_18_y_micro_18_z_4;
            gradz_func_2[idim0] = acc_micro_18_x_micro_18_y_micro_18_z_3;
            gradz_func_1[idim0] = acc_micro_18_x_micro_18_y_micro_18_z_2;
            gradu_2[idim0] = acc_micro_18_x_micro_18_y_micro_18_z_1;
            gradu_1[idim0] = acc_micro_18_x_micro_18_y_micro_18_z_0;
            gradu_0[idim0] = acc_micro_18_x_micro_18_y_micro_18_z;
          }
          t_12 = t_11 * gradz_func_1[2];
          t_13 = t_11 * gradz_func_0[2];
          t_14 = t_11 * gradz_func_2[2];
          t_16 = t_15 * gradz_func_1[1];
          t_17 = t_15 * gradz_func_2[1];
          t_18 = t_15 * gradz_func_0[1];
          t_20 = t_19 * gradz_func_2[0];
          t_21 = t_19 * gradz_func_1[0];
          t_22 = t_19 * gradz_func_0[0];
          t_74 = -1.0 * (t_31 * gradz_func_2[2] * gradu_1[0] + t_39 * (1.0 + t_56 * gradu_2[2]) * gradz_func_1[0]) + t_25 * gradz_func_2[0] * gradu_1[2] + t_31 * gradz_func_1[2] * gradu_2[0];
          t_48 = t_43 * t_74;
          t_87 = -1.0 * (t_28 * gradz_func_2[1] * gradu_1[2] + t_27 * gradz_func_1[2] * gradu_2[1]) + t_56 * (1.0 + t_43 * gradu_1[1]) * gradz_func_2[2] + t_43 * (1.0 + t_56 * gradu_2[2]) * gradz_func_1[1];
          t_40 = t_39 * t_87;
          t_79 = t_52 * gradu_1[0] * gradu_2[1] + t_49 * (1.0 + t_43 * gradu_1[1]) * gradu_2[0];
          t_77 = 1.0 + t_39 * gradu_0[0];
          t_76 = (1.0 + t_43 * gradu_1[1]) * (1.0 + t_56 * gradu_2[2]) + t_35 * gradu_1[2] * gradu_2[1];
          t_75 = t_31 * gradu_1[2] * gradu_2[0] + t_49 * (1.0 + t_56 * gradu_2[2]) * gradu_1[0];
          t_88 = t_56 * t_79 * gradu_0[2] + t_77 * t_76 + t_43 * t_75 * gradu_0[1];
          t_84 = 1.0 / t_88;
          t_73 = -1.0 * (t_39 * (1.0 + t_43 * gradu_1[1]) * gradz_func_2[0] + t_23 * gradz_func_1[1] * gradu_2[0]) + t_23 * gradz_func_2[1] * gradu_1[0] + t_52 * gradz_func_1[0] * gradu_2[1];
          t_78 = t_77 * t_87 + t_39 * t_76 * gradz_func_0[0] + t_43 * t_74 * gradu_0[1] + t_43 * t_75 * gradz_func_0[1] + t_56 * t_73 * gradu_0[2] + t_56 * t_79 * gradz_func_0[2];
          t_90 = -1.0 * t_78 * t_84;
          t_57 = t_56 * t_73;
          t_58 = t_56 * t_79;
          t_42 = t_39 * t_76;
          t_46 = t_43 * t_75;
          t_59 = t_56 * gradz_func_0[2];
          t_4 = t_3 * gradz_func_2[2];
          t_29 = t_28 * gradz_func_2[2];
          t_36 = t_35 * gradz_func_2[1];
          t_44 = t_43 * gradz_func_0[1];
          t_41 = t_39 * gradz_func_0[0];
          t_54 = t_52 * gradz_func_2[1];
          t_9 = t_6 * gradz_func_2[0];
          t_7 = t_6 * gradu_2[0];
          t_32 = t_31 * gradu_2[0];
          t_50 = t_49 * (1.0 + t_56 * gradu_2[2]);
          t_47 = t_43 * (1.0 + t_56 * gradu_2[2]);
          t_60 = t_56 * gradu_0[2];
          t_37 = t_35 * gradu_2[1];
          t_45 = t_43 * gradu_0[1];
          t_55 = t_52 * gradu_2[1];
          t_34 = t_31 * gradz_func_2[0];
          t_33 = t_31 * gradz_func_1[2];
          t_38 = t_35 * gradz_func_1[2];
          t_30 = t_28 * gradz_func_1[1];
          t_53 = t_52 * gradz_func_1[0];
          t_5 = t_3 * gradz_func_1[0];
          t_8 = t_6 * gradz_func_1[1];
          t_26 = t_25 * gradu_1[2];
          t_51 = t_49 * (1.0 + t_43 * gradu_1[1]);
          t_24 = t_23 * gradu_1[0];
          t_61 = t_56 * (1.0 + t_43 * gradu_1[1]);
          t_2 = t_1 * gradu_1[2];
          t_0 = t * gradu_1[0];
          t_89 = 2.0 * t_84 * t_78 * t_84;
          t_91 = 2.0 * log(t_88) * t_84;
          for (int micro_20_z = 0; micro_20_z <= 1; ++micro_20_z)
            for (int micro_20_y = 0; micro_20_y <= 1; ++micro_20_y)
              for (int micro_20_x = 0; micro_20_x <= 1; ++micro_20_x)
              {
                t_64 = t_12 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_65 = t_13 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_66 = t_14 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_67 = t_16 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_68 = t_17 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_69 = t_18 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_70 = t_20 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_71 = t_21 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_72 = t_22 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_83 = t_58 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_42 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_46 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_80 = t_90 * t_83 + t_57 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_40 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_48 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z];
                t_85 = t_60 * (t_55 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_7 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_77 * (t_47 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_37 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_45 * (t_32 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_50 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]);
                t_81 = t_90 * t_85 + t_77 * (t_29 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_36 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_41 * (t_47 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_37 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_45 * (t_34 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_4 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_44 * (t_32 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_50 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_60 * (t_54 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_9 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_59 * (t_55 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_7 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]);
                t_86 = t_60 * (t_24 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_51 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_77 * (t_61 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_2 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_45 * (t_26 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_0 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]);
                t_82 = t_90 * t_86 + t_77 * (t_30 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_38 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_41 * (t_61 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_2 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_45 * (t_33 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_5 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_44 * (t_26 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_0 * js_BCG1[2 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_60 * (t_53 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_8 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]) + t_59 * (t_24 * js_BCG1[1 + 24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z] + t_51 * js_BCG1[24 * q + 3 * micro_20_x + 6 * micro_20_y + 12 * micro_20_z]);
                r_lfsu_2_loc[4 * micro_20_x + 2 * micro_20_y + micro_20_z] = t_63 * (2.8846153846153846 * (t_89 * t_86 + t_91 * t_82) + -3.846153846153846 * t_82 * t_84 + 1.923076923076923 * (t_70 + t_70 + t_68 + t_68 + t_66 + t_66)) + r_lfsu_2_loc[4 * micro_20_x + 2 * micro_20_y + micro_20_z];
                r_lfsu_1_loc[4 * micro_20_x + 2 * micro_20_y + micro_20_z] = t_63 * (2.8846153846153846 * (t_89 * t_85 + t_91 * t_81) + -3.846153846153846 * t_81 * t_84 + 1.923076923076923 * (t_71 + t_71 + t_67 + t_67 + t_64 + t_64)) + r_lfsu_1_loc[4 * micro_20_x + 2 * micro_20_y + micro_20_z];
                r_lfsu_0_loc[4 * micro_20_x + 2 * micro_20_y + micro_20_z] = t_63 * (2.8846153846153846 * (t_89 * t_83 + t_91 * t_80) + -3.846153846153846 * t_80 * t_84 + 1.923076923076923 * (t_72 + t_72 + t_69 + t_69 + t_65 + t_65)) + r_lfsu_0_loc[4 * micro_20_x + 2 * micro_20_y + micro_20_z];
              }
        }
        for (int micro_21_x = 0; micro_21_x <= 1; ++micro_21_x)
          for (int micro_21_y = 0; micro_21_y <= 1; ++micro_21_y)
            for (int micro_21_z = 0; micro_21_z <= 1; ++micro_21_z)
            {
              r_lfsu_2_vec4.load(r_lfsu_2_alias + 5 * subel_y + 25 * subel_z + micro_21_x + 5 * micro_21_y + 25 * micro_21_z);
              r_lfsu_1_vec4.load(r_lfsu_1_alias + 5 * subel_y + 25 * subel_z + micro_21_x + 5 * micro_21_y + 25 * micro_21_z);
              r_lfsu_0_vec4.load(r_lfsu_0_alias + 5 * subel_y + 25 * subel_z + micro_21_x + 5 * micro_21_y + 25 * micro_21_z);
              r_lfsu_2_vec4 = r_lfsu_2_vec4 + r_lfsu_2_loc[4 * micro_21_x + 2 * micro_21_y + micro_21_z] * t_10;
              r_lfsu_2_vec4.store(r_lfsu_2_alias + 5 * subel_y + 25 * subel_z + micro_21_x + 5 * micro_21_y + 25 * micro_21_z);
              r_lfsu_1_vec4 = r_lfsu_1_vec4 + r_lfsu_1_loc[4 * micro_21_x + 2 * micro_21_y + micro_21_z] * t_10;
              r_lfsu_1_vec4.store(r_lfsu_1_alias + 5 * subel_y + 25 * subel_z + micro_21_x + 5 * micro_21_y + 25 * micro_21_z);
              r_lfsu_0_vec4 = r_lfsu_0_vec4 + r_lfsu_0_loc[4 * micro_21_x + 2 * micro_21_y + micro_21_z] * t_10;
              r_lfsu_0_vec4.store(r_lfsu_0_alias + 5 * subel_y + 25 * subel_z + micro_21_x + 5 * micro_21_y + 25 * micro_21_z);
            }
      }
  }
  

  public:
  template<typename LFSV, typename EG, typename J, typename X, typename LFSU>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    using namespace Dune::Indices;
    auto lfsu_0 = child(lfsu, _0);
    auto lfsu_0_size = lfsu_0.size();
    const auto x_lfsu_0_alias = &x(lfsu_0,0);
    auto lfsu_1 = child(lfsu, _1);
    auto lfsu_1_size = lfsu_1.size();
    const auto x_lfsu_1_alias = &x(lfsu_1,0);
    auto lfsu_2 = child(lfsu, _2);
    auto lfsu_2_size = lfsu_2.size();
    const auto x_lfsu_2_alias = &x(lfsu_2,0);
    double acc_micro_26_x_micro_26_y_micro_26_z;
    double acc_micro_26_x_micro_26_y_micro_26_z_0;
    double acc_micro_26_x_micro_26_y_micro_26_z_1;
    double gradu_0[3];
    double gradu_1[3];
    double gradu_2[3];
    double t;
    double t_0;
    double t_1;
    double t_10;
    double t_11;
    double t_12;
    double t_13;
    double t_14;
    double t_15;
    double t_16;
    double t_17;
    double t_18;
    double t_19;
    double t_2;
    double t_20;
    double t_21;
    double t_22;
    double t_23;
    double t_24;
    double t_25;
    double t_26;
    double t_27;
    double t_28;
    double t_29;
    double t_3;
    double t_30;
    double t_31;
    double t_32;
    double t_33;
    double t_34;
    double t_35;
    double t_36;
    double t_37;
    double t_38;
    double t_39;
    double t_4;
    double t_40;
    double t_41;
    double t_42;
    double t_43;
    double t_44;
    double t_45;
    double t_46;
    double t_47;
    double t_48;
    double t_49;
    double t_5;
    double t_50;
    double t_51;
    double t_52;
    double t_53;
    double t_54;
    double t_55;
    double t_56;
    double t_57;
    double t_58;
    double t_59;
    double t_6;
    double t_60;
    double t_61;
    double t_62;
    double t_63;
    double t_64;
    double t_65;
    double t_66;
    double t_67;
    double t_68;
    double t_69;
    double t_7;
    double t_70;
    double t_71;
    double t_72;
    double t_73;
    double t_74;
    double t_75;
    double t_76;
    double t_77;
    double t_78;
    double t_79;
    double t_8;
    double t_80;
    double t_81;
    double t_82;
    double t_83;
    double t_84;
    double t_85;
    double t_86;
    double t_87;
    double t_88;
    double t_89;
    double t_9;
    double t_90;
    double t_91;
    double x_lfsu_0_loc[2 * 2 * 2];
    double x_lfsu_1_loc[2 * 2 * 2];
    double x_lfsu_2_loc[2 * 2 * 2];
  
    t_57 = 0.015625 * detjac;
    t_27 = -16.0 * (jit[2])[2] * (jit[1])[1];
    t_23 = 16.0 * (jit[2])[2] * (jit[0])[0];
    t_50 = 4.0 * (jit[2])[2];
    t_46 = 16.0 * (jit[0])[0] * (jit[1])[1];
    t_36 = 4.0 * (jit[1])[1];
    t_43 = -4.0 * (jit[0])[0];
    t_31 = 4.0 * (jit[0])[0];
    t_13 = -16.0 * (jit[1])[1] * (jit[0])[0];
    t_7 = 16.0 * (jit[0])[0] * (jit[2])[2];
    t_5 = -16.0 * (jit[2])[2] * (jit[0])[0];
    t_9 = -16.0 * (jit[1])[1] * (jit[2])[2];
    t_11 = 16.0 * (jit[1])[1] * (jit[0])[0];
    t_2 = 16.0 * (jit[1])[1] * (jit[2])[2];
    t = -16.0 * (jit[0])[0] * (jit[2])[2];
    t_17 = 16.0 * (jit[2])[2] * (jit[2])[2];
    t_19 = 16.0 * (jit[1])[1] * (jit[1])[1];
    t_21 = 16.0 * (jit[0])[0] * (jit[0])[0];
    for (int subel_z = 0; subel_z <= 3; ++subel_z)
      for (int subel_y = 0; subel_y <= 3; ++subel_y)
        for (int subel_x = 0; subel_x <= 3; ++subel_x)
        {
          for (int micro_27_z = 0; micro_27_z <= 1; ++micro_27_z)
            for (int micro_27_y = 0; micro_27_y <= 1; ++micro_27_y)
              for (int micro_27_x = 0; micro_27_x <= 1; ++micro_27_x)
              {
                x_lfsu_2_loc[4 * micro_27_x + 2 * micro_27_y + micro_27_z] = x_lfsu_2_alias[subel_x + 5 * subel_y + 25 * subel_z + micro_27_x + 5 * micro_27_y + 25 * micro_27_z];
                x_lfsu_1_loc[4 * micro_27_x + 2 * micro_27_y + micro_27_z] = x_lfsu_1_alias[subel_x + 5 * subel_y + 25 * subel_z + micro_27_x + 5 * micro_27_y + 25 * micro_27_z];
                x_lfsu_0_loc[4 * micro_27_x + 2 * micro_27_y + micro_27_z] = x_lfsu_0_alias[subel_x + 5 * subel_y + 25 * subel_z + micro_27_x + 5 * micro_27_y + 25 * micro_27_z];
              }
          for (int q = 0; q <= 7; ++q)
          {
            t_58 = t_57 * qw_dim3_order2[q];
            for (int idim0 = 0; idim0 <= 2; ++idim0)
            {
              acc_micro_26_x_micro_26_y_micro_26_z_1 = 0.0;
              acc_micro_26_x_micro_26_y_micro_26_z_0 = 0.0;
              acc_micro_26_x_micro_26_y_micro_26_z = 0.0;
              for (int micro_26_z = 0; micro_26_z <= 1; ++micro_26_z)
                for (int micro_26_y = 0; micro_26_y <= 1; ++micro_26_y)
                  for (int micro_26_x = 0; micro_26_x <= 1; ++micro_26_x)
                  {
                    acc_micro_26_x_micro_26_y_micro_26_z_1 = acc_micro_26_x_micro_26_y_micro_26_z_1 + x_lfsu_2_loc[4 * micro_26_x + 2 * micro_26_y + micro_26_z] * js_BCG1[idim0 + 24 * q + 3 * micro_26_x + 6 * micro_26_y + 12 * micro_26_z];
                    acc_micro_26_x_micro_26_y_micro_26_z_0 = acc_micro_26_x_micro_26_y_micro_26_z_0 + x_lfsu_1_loc[4 * micro_26_x + 2 * micro_26_y + micro_26_z] * js_BCG1[idim0 + 24 * q + 3 * micro_26_x + 6 * micro_26_y + 12 * micro_26_z];
                    acc_micro_26_x_micro_26_y_micro_26_z = acc_micro_26_x_micro_26_y_micro_26_z + x_lfsu_0_loc[4 * micro_26_x + 2 * micro_26_y + micro_26_z] * js_BCG1[idim0 + 24 * q + 3 * micro_26_x + 6 * micro_26_y + 12 * micro_26_z];
                  }
              gradu_2[idim0] = acc_micro_26_x_micro_26_y_micro_26_z_1;
              gradu_1[idim0] = acc_micro_26_x_micro_26_y_micro_26_z_0;
              gradu_0[idim0] = acc_micro_26_x_micro_26_y_micro_26_z;
            }
            t_28 = t_27 * gradu_2[1];
            t_24 = t_23 * gradu_2[0];
            t_54 = t_50 * gradu_0[2];
            t_47 = t_46 * gradu_2[1];
            t_37 = t_36 * (1.0 + t_50 * gradu_2[2]);
            t_45 = t_43 * (1.0 + t_50 * gradu_2[2]);
            t_38 = t_36 * gradu_0[1];
            t_61 = 1.0 + t_31 * gradu_0[0];
            t_15 = t_13 * gradu_2[0];
            t_59 = t_23 * gradu_1[2] * gradu_2[0] + t_43 * (1.0 + t_50 * gradu_2[2]) * gradu_1[0];
            t_39 = t_36 * t_59;
            t_62 = t_46 * gradu_1[0] * gradu_2[1] + t_43 * (1.0 + t_36 * gradu_1[1]) * gradu_2[0];
            t_51 = t_50 * t_62;
            t_60 = (1.0 + t_36 * gradu_1[1]) * (1.0 + t_50 * gradu_2[2]) + t_27 * gradu_1[2] * gradu_2[1];
            t_32 = t_31 * t_60;
            t_78 = t_50 * t_62 * gradu_0[2] + t_61 * t_60 + t_36 * t_59 * gradu_0[1];
            t_69 = 1.0 / t_78;
            t_8 = t_7 * gradu_1[2];
            t_6 = t_5 * gradu_1[0];
            t_10 = t_9 * gradu_1[2];
            t_52 = t_50 * (1.0 + t_36 * gradu_1[1]);
            t_44 = t_43 * (1.0 + t_36 * gradu_1[1]);
            t_12 = t_11 * gradu_1[0];
            t_90 = 3.846153846153846 * t_69 * t_69;
            t_91 = 2.0 * t_69 * t_69;
            t_70 = log(t_78);
            t_89 = -2.0 * t_70 * t_69 * t_69;
            t_88 = 2.0 * t_70 * t_69;
            for (int micro_29_z = 0; micro_29_z <= 1; ++micro_29_z)
              for (int micro_29_y = 0; micro_29_y <= 1; ++micro_29_y)
                for (int micro_29_x = 0; micro_29_x <= 1; ++micro_29_x)
                {
                  t_73 = t_54 * (t_47 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_15 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]) + t_61 * (t_37 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_28 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]) + t_38 * (t_24 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_45 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_33 = t_31 * (t_37 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_28 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_40 = t_36 * (t_24 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_45 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_55 = t_50 * (t_47 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_15 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_41 = t_36 * (t_8 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_6 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_76 = t_54 * (t_12 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_44 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]) + t_61 * (t_52 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_10 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]) + t_38 * (t_8 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_6 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_56 = t_50 * (t_12 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_44 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_34 = t_31 * (t_52 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_10 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z]);
                  t_35 = t_31 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_53 = t_50 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_64 = t_51 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_32 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z] + t_39 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_42 = t_36 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_26 = t_23 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_16 = t_13 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_29 = t_27 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_49 = t_46 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_4 = t_2 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_0 = t * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_1 = t * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_30 = t_27 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_25 = t_23 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_14 = t_13 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_3 = t_2 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_48 = t_46 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_18 = t_17 * js_BCG1[2 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_20 = t_19 * js_BCG1[1 + 24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_22 = t_21 * js_BCG1[24 * q + 3 * micro_29_x + 6 * micro_29_y + 12 * micro_29_z];
                  t_79 = t_90 * t_64;
                  t_80 = t_91 * t_64;
                  t_81 = t_89 * t_64;
                  t_82 = t_90 * t_73;
                  t_83 = t_91 * t_73;
                  t_84 = t_89 * t_73;
                  t_85 = t_89 * t_76;
                  t_86 = t_90 * t_76;
                  t_87 = t_91 * t_76;
                  for (int micro_28_z = 0; micro_28_z <= 1; ++micro_28_z)
                    for (int micro_28_y = 0; micro_28_y <= 1; ++micro_28_y)
                      for (int micro_28_x = 0; micro_28_x <= 1; ++micro_28_x)
                      {
                        t_63 = t_51 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_32 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_39 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z];
                        t_65 = -1.0 * t_63 * t_73 * t_69 + t_55 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_33 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_40 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z];
                        t_66 = -1.0 * t_63 * t_76 * t_69 + t_56 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_34 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_41 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z];
                        t_74 = t_54 * (t_12 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_44 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_61 * (t_52 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_10 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_38 * (t_8 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_6 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]);
                        t_67 = -1.0 * t_74 * t_64 * t_69 + t_53 * (t_12 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_44 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_35 * (t_52 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_10 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_42 * (t_8 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_6 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]);
                        t_75 = t_54 * (t_47 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_15 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_61 * (t_37 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_28 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_38 * (t_24 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_45 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]);
                        t_68 = -1.0 * t_75 * t_64 * t_69 + t_53 * (t_47 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_15 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_35 * (t_37 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_28 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_42 * (t_24 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_45 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]);
                        t_71 = -1.0 * t_74 * t_73 * t_69 + t_54 * (t_49 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_16 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_61 * (t_4 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_29 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_38 * (t_26 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_0 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]);
                        t_72 = -1.0 * t_75 * t_76 * t_69 + t_54 * (t_48 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_14 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_61 * (t_3 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_30 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]) + t_38 * (t_25 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_1 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]);
                        t_77 = 1.923076923076923 * (t_22 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_22 * js_BCG1[24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_20 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_20 * js_BCG1[1 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_18 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z] + t_18 * js_BCG1[2 + 24 * q + 3 * micro_28_x + 6 * micro_28_y + 12 * micro_28_z]);
                        jac.accumulate(lfsu_2, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_2, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (2.8846153846153846 * (t_87 * t_74 + t_85 * t_74) + t_86 * t_74 + t_77));
                        jac.accumulate(lfsu_2, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_1, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (-3.846153846153846 * t_72 * t_69 + 2.8846153846153846 * (t_87 * t_75 + t_88 * t_72)));
                        jac.accumulate(lfsu_2, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_0, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (-3.846153846153846 * t_66 * t_69 + 2.8846153846153846 * (t_87 * t_63 + t_88 * t_66)));
                        jac.accumulate(lfsu_1, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_2, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (-3.846153846153846 * t_71 * t_69 + 2.8846153846153846 * (t_83 * t_74 + t_88 * t_71)));
                        jac.accumulate(lfsu_1, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_1, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (2.8846153846153846 * (t_83 * t_75 + t_84 * t_75) + t_82 * t_75 + t_77));
                        jac.accumulate(lfsu_1, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_0, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (-3.846153846153846 * t_65 * t_69 + 2.8846153846153846 * (t_83 * t_63 + t_88 * t_65)));
                        jac.accumulate(lfsu_0, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_2, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (-3.846153846153846 * t_67 * t_69 + 2.8846153846153846 * (t_80 * t_74 + t_88 * t_67)));
                        jac.accumulate(lfsu_0, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_1, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (-3.846153846153846 * t_68 * t_69 + 2.8846153846153846 * (t_80 * t_75 + t_88 * t_68)));
                        jac.accumulate(lfsu_0, 5 * (subel_y + micro_29_y) + 25 * (subel_z + micro_29_z) + subel_x + micro_29_x, lfsu_0, 5 * (subel_y + micro_28_y) + 25 * (subel_z + micro_28_z) + subel_x + micro_28_x, t_58 * (2.8846153846153846 * (t_80 * t_63 + t_81 * t_63) + t_79 * t_63 + t_77));
                      }
                }
          }
        }
  }
};


#pragma GCC diagnostic pop

#endif //HYPERELASTICITY_GENERATED_ROPERATOR_FILE_HH
