add_executable(testing testing.cc)
target_link_dune_default_libraries(testing)

dune_add_generated_executable(
        UFLFILE nonlinear.ufl
        INIFILE nonlinear.ini
        SOURCE nonlinear.cc
        TARGET nonlinear_poisson
)
