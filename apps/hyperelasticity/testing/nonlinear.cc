
#include "config.h"
#include "dune/common/parallel/mpihelper.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/testtools/gridconstruction.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/pdelab/gridoperator/blockstructured.hh"
#include <dune/pdelab/solver/newton.hh>
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/codegen/vtkpredicate.hh"
#include "nonlinear_poisson_driverblock.hh"
#include "dune/ch-benchmarks/linearsolver.hh"



int main(int argc, char** argv){
  if (argc != 2){
    std::cerr << "This program needs to be called with an ini file" << std::endl;
    return 1;
  }

  // Initialize basic stuff...
  Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
  using RangeType = double;
  Dune::ParameterTree initree;
  Dune::ParameterTreeParser::readINITree(argv[1], initree);

  // Setup grid (view)...
  using Grid = Dune::YaspGrid<2, Dune::EquidistantCoordinates<RangeType, 2>>;
  using GV = Grid::LeafGridView;
  IniGridFactory<Grid> factory(initree);
  std::shared_ptr<Grid> grid = factory.getGrid();
  GV gv = grid->leafGridView();

  // Set up driver block...
  using DB = DriverBlock<GV>;
  DB driver(gv, initree);
  using GOP = typename DB::GO_r;
  auto gridOperator = driver.getGridOperator_r();
  auto coefficient = driver.getCoefficient_r();
  using Coefficient = DriverBlock<GV>::V_R;
  auto gridFunctionSpace = driver.getGridFunctionsSpace_BCG1();

  auto prefine = driver.pre_fine;
  auto precoarse = driver.pre_coarse;

  //using LS = Dune::PDELab::ISTLBackend_SEQ_MatrixFree_BCGS_Richardson<GOP>;
  //LS ls(*gridOperator, 5000, 1);
  using LS = MatrixFreeTwoLevelBiCGStab<GOP, decltype(prefine)::element_type, decltype(precoarse)::element_type>;
  LS ls(*gridOperator, prefine, precoarse, 5000, 1);

  using Solver = Dune::PDELab::NewtonMethod<GOP, LS, true>;
  Solver solver(*gridOperator, ls);

  auto& x = *driver.getCoefficient_r();
  solver.apply(x);

  // Do visualization...
  using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
  Dune::RefinementIntervals subint(initree.get<int>("vtk.subsamplinglevel", 4));
  VTKWriter vtkwriter(gv, subint);
  std::string vtkfile = initree.get<std::string>("wrapper.vtkcompare.name", "output");
  CuttingPredicate predicate;
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, *gridFunctionSpace, *coefficient, Dune::PDELab::vtk::defaultNameScheme(), predicate);
  vtkwriter.write(vtkfile, Dune::VTK::ascii);
}

