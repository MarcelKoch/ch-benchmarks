#ifndef HYPERELASTICITY_GENERATED_RESTRICTION_BCG1_3_OPERATOR_FILE_HH
#define HYPERELASTICITY_GENERATED_RESTRICTION_BCG1_3_OPERATOR_FILE_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/localfunctions/lagrange/lagrangecube.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


template<typename GFSU, typename GFSV>
class RestrictionBCG1_3LocalOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern
{  

  public:
  RestrictionBCG1_3LocalOperator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams) :
      _iniParams(iniParams)
  {
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = false };
  

  public:
  mutable std::vector<Dune::FieldVector<double, 1>> phiCG1 = std::vector<Dune::FieldVector<double, 1>>(8);
  

  public:
  Dune::Impl::LagrangeCubeLocalBasis<double, double, 3, 1> coarseBasisCG1;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  template<typename R, typename LFSV, typename EG, typename LFSU, typename X>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    Dune::FieldVector<double, 3> dofCG1(0.0);
    double t;
    double t_0;
  
    auto* x_base = Dune::PDELab::accessBaseContainer(x).data();
    auto* x2 = x_base + 250;
    auto* x1 = x_base + 125;
    auto* x0 = x_base + 0;
    auto* r_base = Dune::PDELab::accessBaseContainer(r).data();
    auto* r2 = r_base + 16;
    auto* r1 = r_base + 8;
    auto* r0 = r_base + 0;
    for (int i_26 = 0; i_26 <= 4; ++i_26)
    {
      t = (double) (i_26) / (double) (4.0);
      for (int i_25 = 0; i_25 <= 4; ++i_25)
      {
        t_0 = (double) (i_25) / (double) (4.0);
        for (int i_24 = 0; i_24 <= 4; ++i_24)
        {
          dofCG1[2] = t;
          dofCG1[1] = t_0;
          dofCG1[0] = (double) (i_24) / (double) (4.0);
          coarseBasisCG1.evaluateFunction(dofCG1, phiCG1);
          for (int i_35 = 0; i_35 <= 1; ++i_35)
            for (int i_34 = 0; i_34 <= 1; ++i_34)
              for (int i_33 = 0; i_33 <= 1; ++i_33)
              {
                r2[i_33 + 2 * i_34 + 4 * i_35] = (phiCG1[i_33 + 2 * i_34 + 4 * i_35])[0] * x2[i_24 + 5 * i_25 + 25 * i_26] + r2[i_33 + 2 * i_34 + 4 * i_35];
                r1[i_33 + 2 * i_34 + 4 * i_35] = (phiCG1[i_33 + 2 * i_34 + 4 * i_35])[0] * x1[i_24 + 5 * i_25 + 25 * i_26] + r1[i_33 + 2 * i_34 + 4 * i_35];
                r0[i_33 + 2 * i_34 + 4 * i_35] = (phiCG1[i_33 + 2 * i_34 + 4 * i_35])[0] * x0[i_24 + 5 * i_25 + 25 * i_26] + r0[i_33 + 2 * i_34 + 4 * i_35];
              }
        }
      }
    }
  }
};


#pragma GCC diagnostic pop

#endif //HYPERELASTICITY_GENERATED_RESTRICTION_BCG1_3_OPERATOR_FILE_HH
