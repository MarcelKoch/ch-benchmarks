#ifndef HYPERELASTICITY_GENERATED_INTERPOLATION_CG1_3_OPERATOR_FILE_HH
#define HYPERELASTICITY_GENERATED_INTERPOLATION_CG1_3_OPERATOR_FILE_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/localfunctions/lagrange/lagrangecube.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


template<typename GFSU, typename GFSV>
class InterpolationCG1_3LocalOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern
{  

  public:
  InterpolationCG1_3LocalOperator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams) :
      _iniParams(iniParams)
  {
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = false };
  

  public:
  mutable std::vector<Dune::FieldVector<double, 1>> phiCG1 = std::vector<Dune::FieldVector<double, 1>>(8);
  

  public:
  Dune::Impl::LagrangeCubeLocalBasis<double, double, 3, 1> coarseBasisCG1;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  template<typename R, typename LFSV, typename EG, typename LFSU, typename X>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    Dune::FieldVector<double, 3> dofCG1(0.0);
    double t;
    double t_0;
  
    auto* x_base = Dune::PDELab::accessBaseContainer(x).data();
    auto* x2 = x_base + 16;
    auto* x1 = x_base + 8;
    auto* x0 = x_base + 0;
    auto* r_base = Dune::PDELab::accessBaseContainer(r).data();
    auto* r2 = r_base + 250;
    auto* r1 = r_base + 125;
    auto* r0 = r_base + 0;
    for (int i_17 = 0; i_17 <= 4; ++i_17)
    {
      t = (double) (i_17) / (double) (4.0);
      for (int i_16 = 0; i_16 <= 4; ++i_16)
      {
        t_0 = (double) (i_16) / (double) (4.0);
        for (int i_15 = 0; i_15 <= 4; ++i_15)
        {
          dofCG1[2] = t;
          dofCG1[1] = t_0;
          dofCG1[0] = (double) (i_15) / (double) (4.0);
          coarseBasisCG1.evaluateFunction(dofCG1, phiCG1);
          for (int i_8 = 0; i_8 <= 1; ++i_8)
            for (int i_7 = 0; i_7 <= 1; ++i_7)
              for (int i_6 = 0; i_6 <= 1; ++i_6)
              {
                r2[i_15 + 5 * i_16 + 25 * i_17] = (phiCG1[i_6 + 2 * i_7 + 4 * i_8])[0] * x2[i_6 + 2 * i_7 + 4 * i_8] + r2[i_15 + 5 * i_16 + 25 * i_17];
                r1[i_15 + 5 * i_16 + 25 * i_17] = (phiCG1[i_6 + 2 * i_7 + 4 * i_8])[0] * x1[i_6 + 2 * i_7 + 4 * i_8] + r1[i_15 + 5 * i_16 + 25 * i_17];
                r0[i_15 + 5 * i_16 + 25 * i_17] = (phiCG1[i_6 + 2 * i_7 + 4 * i_8])[0] * x0[i_6 + 2 * i_7 + 4 * i_8] + r0[i_15 + 5 * i_16 + 25 * i_17];
              }
        }
      }
    }
  }
};


#pragma GCC diagnostic pop

#endif //HYPERELASTICITY_GENERATED_INTERPOLATION_CG1_3_OPERATOR_FILE_HH
